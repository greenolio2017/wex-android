
package com.greenolio.app.wex;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.PreferencesUtil;
import com.greenolio.app.wex.pincode.PincodeActivity;

import java.lang.ref.WeakReference;


public class Application extends android.app.Application implements android.app.Application.ActivityLifecycleCallbacks {

    private static Application application;
    private boolean mIsShowPincode = true;
    private WeakReference<Activity> mOldActivity;


    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        mIsShowPincode = false;
        registerActivityLifecycleCallbacks(this);

    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        application = null;
    }


    public static Application getApplication() {
        return application;
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(final Activity activity) {

        if (mIsShowPincode && mOldActivity != null && mOldActivity.get() == activity) {
            if (activity.getComponentName().getClassName().equals(PincodeActivity.class.getName())) {
                return;
            }
            showPincode();
            mIsShowPincode = false;
            mOldActivity = null;
        }
    }

    private void showPincode() {
        boolean isShowPin = PreferencesUtil.getInstance(this).getBoolean(Constant.KEY_PREF_KEY_ACTIVE_PIN, false);
        if (isShowPin) {
            Intent intent = new Intent(this, PincodeActivity.class);
            intent.putExtra(Constant.INTENT_EXTRAS_TYPE_DISPLAY_PINCODE, Constant.INTENT_EXTRAS_PINCODE_DEFAULT);
            this.startActivity(intent);
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        mIsShowPincode = true;
        mOldActivity = new WeakReference<>(activity);


    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        mIsShowPincode = false;

    }

}

