package com.greenolio.app.wex.aboutus;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.common.Utils;

public class AboutUsPresenter {
    public static final int TYPE_FACEBOOK = 1;
    public static final int TYPE_TWITTER = 2;
    public static final int TYPE_WEXRUS = 3;
    public static final int TYPE_SUPPORT = 4;
    private Context mContext;

    public AboutUsPresenter(Context mContext) {
        this.mContext = mContext;
    }

    public void openLinkSocial(int type) {
        switch (type) {
            case TYPE_FACEBOOK:
                String linkFace = mContext.getResources().getString(R.string.txt_link_facebook);
                openUrlFacebook(linkFace);
                break;
            case TYPE_TWITTER:
                String linkTwitter = mContext.getResources().getString(R.string.txt_link_twitter);
                //openLink(linkTwitter);
                openUrlTwitter(linkTwitter);
                break;

            case TYPE_WEXRUS:
                String linkWexRus = mContext.getResources().getString(R.string.txt_link_wexrus);
               // openLink(linkWexRus);
                openUrlTelegram(linkWexRus);
                break;
            case TYPE_SUPPORT:
                String linkSupport = mContext.getResources().getString(R.string.txt_link_support);
                openLink(linkSupport);
                break;
        }
    }

    private void openUrlTwitter(String linkTwitter){
        String packageName = "com.twitter.android";
        if (Utils.isAppInstalledOrNot(packageName, mContext)) {
            Intent intent = getTwitterIntent(linkTwitter);
            mContext.startActivity(intent);
        } else {
            openLink(linkTwitter);
        }

    }

    private void openUrlFacebook(String linkFacebook){
        String packageName = "com.facebook.katana";
        if (Utils.isAppInstalledOrNot(packageName, mContext)) {
            Intent intent = getFacebookIntent(linkFacebook);
            mContext.startActivity(intent);
        } else {
            openLink(linkFacebook);
        }

    }
    private void openUrlTelegram(String linkTelegram){
        String packageName = "org.telegram.messenger";
        if (Utils.isAppInstalledOrNot(packageName, mContext)) {
            Intent intent = getTelegramIntent(linkTelegram);
            mContext.startActivity(intent);
        } else {
            openLink(linkTelegram);
        }

    }


    private Intent getFacebookIntent(String url) {
        PackageManager pm = mContext.getPackageManager();
        Uri uri = Uri.parse(url);

        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        }

        catch (PackageManager.NameNotFoundException ignored) {
        }

        return new Intent(Intent.ACTION_VIEW, uri);
    }

    private Intent getTwitterIntent(String url) {
        PackageManager pm = mContext.getPackageManager();
        Uri uri = Uri.parse(url);

        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.twitter.android", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("twitter://user?screen_name=wexnz");
            }
        }

        catch (PackageManager.NameNotFoundException ignored) {
        }

        return new Intent(Intent.ACTION_VIEW, uri);
    }

    private Intent getTelegramIntent(String url) {
        PackageManager pm = mContext.getPackageManager();
        Uri uri = Uri.parse(url);

        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("org.telegram.messenger", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("tg://share?url="+url);
            }
        }

        catch (PackageManager.NameNotFoundException ignored) {
        }

        return new Intent(Intent.ACTION_VIEW, uri);
    }

    private void openLink(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        if (url == null || url.isEmpty()) {
            return;
        }
        intent.setData(Uri.parse(url));
        mContext.startActivity(intent);
    }
}
