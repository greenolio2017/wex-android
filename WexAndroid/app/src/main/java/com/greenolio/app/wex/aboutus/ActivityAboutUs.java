package com.greenolio.app.wex.aboutus;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.databinding.ActivityAboutUsBinding;

public class ActivityAboutUs extends BaseActivity implements View.OnClickListener {
    private ActivityAboutUsBinding mAboutUsBinding;
    private AboutUsPresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mAboutUsBinding = DataBindingUtil.setContentView(this, R.layout.activity_about_us);
            initView();
            initData();
        }
    }


    private void initView() {
        mAboutUsBinding.imgBack.setOnClickListener(this);
        mAboutUsBinding.imgFacebook.setOnClickListener(this);
        mAboutUsBinding.imgTwitter.setOnClickListener(this);
        mAboutUsBinding.imgWexrus.setOnClickListener(this);
        mAboutUsBinding.txtSupport.setOnClickListener(this);


    }

    private void initData() {
        if (mPresenter == null) {
            mPresenter = new AboutUsPresenter(this);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.img_twitter:
                mPresenter.openLinkSocial(AboutUsPresenter.TYPE_TWITTER);
                break;
            case R.id.img_facebook:
                mPresenter.openLinkSocial(AboutUsPresenter.TYPE_FACEBOOK);
                break;
            case R.id.img_wexrus:
                mPresenter.openLinkSocial(AboutUsPresenter.TYPE_WEXRUS);
                break;
            case R.id.txt_support:
                mPresenter.openLinkSocial(AboutUsPresenter.TYPE_SUPPORT);
                break;
        }

    }


}

