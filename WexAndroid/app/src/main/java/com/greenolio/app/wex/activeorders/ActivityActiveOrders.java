package com.greenolio.app.wex.activeorders;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.adapter.ActiveOrdersAdapter;
import com.greenolio.app.wex.adapter.InfoOrdersAdapter;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityActiveOrdersBinding;

import java.util.List;

public class ActivityActiveOrders extends BaseActivity implements View.OnClickListener, ActivityActiveOrdersView, ActiveOrdersAdapter.ItemClickListener {
    private ActivityActiveOrdersPresenter mPresenter;
    private ActivityActiveOrdersBinding mBindingOrders;
    private ActiveOrdersAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBindingOrders = DataBindingUtil.setContentView(this, R.layout.activity_active_orders);
        initView();
        initData();

    }

    private void initView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mBindingOrders.recyclerView.setLayoutManager(layoutManager);
        mBindingOrders.imgBack.setOnClickListener(this);

    }

    private void initData() {
        if (mPresenter == null) {
            ActivityActiveOrdersModel mModel = new ActivityActiveOrdersModel(this);
            mPresenter = new ActivityActiveOrdersPresenter(this, this, mModel);
        }
        mPresenter.loadDataOrders();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_back:
                finish();
                break;

        }

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void loadDataSuccess(final List<ActiveOrdersResponse> responseList) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mAdapter == null) {
                    mAdapter = new ActiveOrdersAdapter(responseList, ActivityActiveOrders.this);
                    mAdapter.setItemClickListener(ActivityActiveOrders.this);
                    mBindingOrders.recyclerView.setAdapter(mAdapter);
                } else {
                    mAdapter.dataSetChangeList(responseList);
                }
            }
        });
    }

    @Override
    public void showTextNoData(boolean isNodata) {
        if (isNodata) {
            mBindingOrders.txtNoData.setVisibility(View.VISIBLE);
            mBindingOrders.recyclerView.setVisibility(View.INVISIBLE);
        } else {
            mBindingOrders.txtNoData.setVisibility(View.GONE);
            mBindingOrders.recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClickItem(int position, ActiveOrdersResponse responses) {
        if (responses != null) {
            String orderId = responses.getOrderID();
            mPresenter.doCancelOrder(orderId);
        }
    }

    @Override
    public void showDialogDisconnect() {
        super.showDialogDisconnect();
        mPresenter.dialogDisconnectInternet();
    }

    @Override
    public void dismissDialogDisconnect() {
        super.dismissDialogDisconnect();
        mPresenter.dismissDialogDisconnect();
    }
}
