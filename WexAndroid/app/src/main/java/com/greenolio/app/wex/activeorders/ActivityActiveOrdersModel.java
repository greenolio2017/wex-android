package com.greenolio.app.wex.activeorders;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.ActiveOrdersAPI;
import com.greenolio.app.wex.api.CancelOrderAPI;
import com.greenolio.app.wex.api.GetInfo;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

class ActivityActiveOrdersModel {
    private final Context mContext;
    private ActivityActiveOrdersResult mResult;

    public ActivityActiveOrdersModel(Context mContext) {
        this.mContext = mContext;
    }

    public void loadDataActiveOrders(final String pair) {
        final List<ActiveOrdersResponse> responseList = new ArrayList<>();
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, List<ActiveOrdersResponse>> task = new AsyncTask<Void, Void, List<ActiveOrdersResponse>>() {
            @Override
            protected List<ActiveOrdersResponse> doInBackground(Void... voids) {
                JsonObject jsonObject = ActiveOrdersAPI.getActiveOrdersObj(pair, mContext);
                if (jsonObject != null) {
                    boolean isSuccess = ActiveOrdersAPI.getSuccess(jsonObject);
                    if (isSuccess) {
                        JsonObject ordersReturn = ActiveOrdersAPI.getReturn(jsonObject);
                        try {
                            JSONObject jo = new JSONObject(ordersReturn.toString());
                            final int size = jo.length();
                            for (int i = 0; i < size; i++) {
                                String key = jo.names().get(i).toString();
                                JsonElement jelement = new JsonParser().parse(jo.get(key).toString());
                                JsonObject getKeyElement = jelement.getAsJsonObject();

                                ActiveOrdersResponse response = new ActiveOrdersResponse();
                                response.setPair(ActiveOrdersAPI.getPair(getKeyElement));
                                response.setType(ActiveOrdersAPI.getType(getKeyElement));
                                response.setAmount(ActiveOrdersAPI.getAmount(getKeyElement));
                                response.setRate(ActiveOrdersAPI.getRate(getKeyElement));
                                response.setTimeStamp(ActiveOrdersAPI.getTimestamp(getKeyElement));
                                response.setStatus(ActiveOrdersAPI.getStatus(getKeyElement));
                                response.setOrderID(key);
                                responseList.add(response);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        String error = jsonObject.getAsJsonObject().get("error").toString();
                        mResult.loadOrdersFail(error);
                    }

                }
                return responseList;

            }

            @Override
            protected void onPostExecute(List<ActiveOrdersResponse> response) {
                super.onPostExecute(response);
                if (response.size() != 0) {
                    mResult.loadOrdersSuccess(response);
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void loadCancelOrder(final String orderId) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                JsonObject jsonObject = CancelOrderAPI.doCancelOrder(orderId, mContext);
                if (jsonObject != null) {
                    boolean isSuccess = CancelOrderAPI.getSuccess(jsonObject);
                    return isSuccess;
                }
                return false;

            }

            @Override
            protected void onPostExecute(Boolean isSuccess) {
                super.onPostExecute(isSuccess);
                if (isSuccess) {
                    mResult.cancelOrderSuccess(orderId);
                } else {
                    mResult.cancelOrderFail(orderId);
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    public void setResult(ActivityActiveOrdersResult result) {
        this.mResult = result;
    }
}
