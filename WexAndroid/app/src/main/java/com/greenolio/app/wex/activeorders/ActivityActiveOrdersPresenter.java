package com.greenolio.app.wex.activeorders;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.component.ProgressDialog;

import java.util.List;

public class ActivityActiveOrdersPresenter implements ActivityActiveOrdersResult {
    private final AppCompatActivity mContext;
    private final ActivityActiveOrdersView mView;
    private final ActivityActiveOrdersModel mModel;
    private ProgressDialog mDialogLoading;
    private List<ActiveOrdersResponse> mResponseList;
    private DialogHelper mDialogHelper;

    public ActivityActiveOrdersPresenter(AppCompatActivity mContext, ActivityActiveOrdersView mView, ActivityActiveOrdersModel model) {
        this.mContext = mContext;
        this.mView = mView;
        this.mModel = model;
        this.mModel.setResult(this);


    }

    public void loadDataOrders() {
        if (mDialogLoading == null) {
            mDialogLoading = new ProgressDialog(mContext);
        }
        mDialogLoading.show();
        mModel.loadDataActiveOrders("");
    }

    public void doCancelOrder(String order_ID) {
        if (!Utils.isCheckConnection(mContext)) {
            dialogDisconnectInternet();

            return;

        }
        if (mDialogLoading == null) {
            mDialogLoading = new ProgressDialog(mContext);
        }
        mDialogLoading.show();
        mModel.loadCancelOrder(order_ID);
    }

    @Override
    public void loadOrdersSuccess(List<ActiveOrdersResponse> responseList) {
        mResponseList = responseList;
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        if (responseList.size() != 0) {
            mView.loadDataSuccess(responseList);
            mView.showTextNoData(false);
        } else {
            mView.showTextNoData(true);
        }

    }

    @Override
    public void loadOrdersFail(final String error) {
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        if (error == null || error.isEmpty()) {
            return;
        }
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null && !error.isEmpty()) {
                    if (mDialogHelper == null)
                        mDialogHelper = new DialogHelper(mContext);
                    mDialogHelper.showAlert("Notify", error, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDialogHelper.dismiss();
                        }
                    });
                }
                mView.showTextNoData(true);
            }
        });

    }

    @Override
    public void cancelOrderSuccess(String order_ID) {
        if (mDialogLoading != null)
            mDialogLoading.dissmissDialog();
        if (mResponseList != null && mResponseList.size() != 0) {
            for (ActiveOrdersResponse response : mResponseList) {
                if (response.getOrderID().equals(order_ID)) {
                    mResponseList.remove(response);
                    mView.loadDataSuccess(mResponseList);
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.order) + " " + order_ID + " "
                            + mContext.getResources().getString(R.string.closed), Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }


    }

    @Override
    public void cancelOrderFail(String order_Id) {
        if (mDialogLoading != null)
            mDialogLoading.dissmissDialog();
        Toast.makeText(mContext, mContext.getResources().getString(R.string.order_error) +
                " " + order_Id + " " + mContext.getResources().getString(R.string.order_not_closed), Toast.LENGTH_SHORT).show();

    }

    public void dialogDisconnectInternet() {
        if (mDialogHelper == null)
            mDialogHelper = new DialogHelper(mContext);
        mDialogHelper.showAlert("INFO",
                mContext.getResources().getString(R.string.txt_content_disconnect_internet), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialogHelper.dismiss();
                        checkDismissDialog();
                    }
                });
    }

    public void checkDismissDialog() {
        if (!Utils.isCheckConnection(mContext)) {
            dialogDisconnectInternet();
        }
    }

    public void dismissDialogDisconnect() {
        if (mDialogHelper != null)
            mDialogHelper.dismiss();
    }

}
