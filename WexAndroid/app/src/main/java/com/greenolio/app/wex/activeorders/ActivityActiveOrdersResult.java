package com.greenolio.app.wex.activeorders;

import com.greenolio.app.wex.api.response.ActiveOrdersResponse;

import java.util.List;

interface ActivityActiveOrdersResult {
     void loadOrdersSuccess(List<ActiveOrdersResponse> responseList);
     void loadOrdersFail(String error);
     void cancelOrderSuccess(String order_ID);
     void cancelOrderFail(String order_Id);
}
