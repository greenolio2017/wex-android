package com.greenolio.app.wex.activeorders;


import com.greenolio.app.wex.api.response.ActiveOrdersResponse;

import java.util.List;

interface ActivityActiveOrdersView {
        void loadDataSuccess(List<ActiveOrdersResponse> responseList);
        void showTextNoData(boolean isNodata);
}
