package com.greenolio.app.wex.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.aboutus.ActivityAboutUs;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.Utils;


public class BaseActivity extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    private final Gson gson = new Gson();
    private ConnectivityReceiver mReceiverInternet;
    private boolean mIsDisconnectInternet;
    private boolean mIsRegisterBroadcast;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBroadcast();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.unregisterOnSharedPreferenceChangeListener(this);
        unRegisterBroadcast();
    }




    private void registerBroadcast() {
        if (!mIsRegisterBroadcast) {
            if (mReceiverInternet == null) {
                mReceiverInternet = new ConnectivityReceiver();
                mIsRegisterBroadcast = true;
            }
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(mReceiverInternet, intentFilter);
        }
    }

    private void unRegisterBroadcast() {
        if (mReceiverInternet != null) {
            unregisterReceiver(mReceiverInternet);
            dismissDialogDisconnect();
        }
    }

    public class ConnectivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                //NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                ConnectivityManager connectivityManager = (ConnectivityManager) BaseActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo == null) {
                    mIsDisconnectInternet = true;
                    showDialogDisconnect();
                    return;
                }
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                    if (mIsDisconnectInternet) {
                        mIsDisconnectInternet = false;
                        dismissDialogDisconnect();
                    }

                } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                    mIsDisconnectInternet = true;
                    showDialogDisconnect();

                }
            }
        }

    }

    public void showDialogDisconnect(){

    }

    public void dismissDialogDisconnect() {

    }


}
