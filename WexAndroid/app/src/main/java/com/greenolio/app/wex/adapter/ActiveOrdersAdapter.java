package com.greenolio.app.wex.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.databinding.ItemBaseOrdersBinding;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import static com.greenolio.app.wex.common.Constant.ORDER_TYPE_BUY;
import static com.greenolio.app.wex.common.Constant.ORDER_TYPE_SELL;


public class ActiveOrdersAdapter extends RecyclerView.Adapter<ActiveOrdersAdapter.MyViewHolder> {
    private List<ActiveOrdersResponse> responses;
    private MyViewHolder mHolder;
    private ItemClickListener mItemClickListener;
    private Context mContext;

    public ActiveOrdersAdapter(List<ActiveOrdersResponse> responses, Context context) {
        this.responses = responses;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemBaseOrdersBinding binding = ItemBaseOrdersBinding.inflate(inflater, parent, false);
        mHolder = new MyViewHolder(binding);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ActiveOrdersResponse response = this.responses.get(position);
        NumberFormat formatter = new DecimalFormat("#.######");
        String pair = getPair(response.getPair().replace("\"", ""));
        String sell = response.getType().toUpperCase().replace("\"", "");
        Double amount = Double.parseDouble(response.getAmount());
        Double price = Double.parseDouble(response.getRate());

        int color = ContextCompat.getColor(mContext, R.color.red);
        if (sell.equalsIgnoreCase("BUY")) {
            color = ContextCompat.getColor(mContext, R.color.green);
        }
        holder.getDataBinding().txtPair.setText(pair);
        holder.getDataBinding().txtAmount.setText(formatter.format(amount));
        holder.getDataBinding().txtType.setText(sell);
        holder.getDataBinding().txtType.setTextColor(color);
        holder.getDataBinding().txtPrice.setText(formatter.format(price));
        holder.getDataBinding().txtDate.setVisibility(View.GONE);
        holder.getDataBinding().imgRemove.setVisibility(View.VISIBLE);
        holder.getDataBinding().imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String content = mContext.getResources().getString(R.string.dialog_content_cancel_order) + "ID-" + response.getOrderID();
                final DialogHelper dialogHelper = new DialogHelper(mContext);
                dialogHelper.showConfirmCancelOrder(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogHelper.dismiss();
                        mItemClickListener.onClickItem(position, response);
                    }
                }, content);
            }
        });


    }

    private String getPair(String pair) {
        String result = "";
        String[] arrPair = pair.split("_");
        if (arrPair.length != 0) {
            StringBuilder builder = new StringBuilder();
            builder.append(arrPair[0]).append("/").append(arrPair[1]);
            result = builder.toString().toUpperCase();
        }
        return result;
    }

    @Override
    public int getItemCount() {
        return this.responses.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemBaseOrdersBinding binding;

        MyViewHolder(ItemBaseOrdersBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(this);

        }

        ItemBaseOrdersBinding getDataBinding() {
            return this.binding;
        }


        @Override
        public void onClick(View view) {

        }
    }


    public void dataSetChangeList(List<ActiveOrdersResponse> list) {
        this.responses = list;
        notifyDataSetChanged();
    }


    public void setItemClickListener(ItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface ItemClickListener {
        void onClickItem(int position, ActiveOrdersResponse responses);
    }


}
