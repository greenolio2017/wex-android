package com.greenolio.app.wex.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.greenolio.app.wex.balances.BalancesResponse;
import com.greenolio.app.wex.databinding.ItemBalancesBinding;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;


public class BalancesAdapter extends RecyclerView.Adapter<BalancesAdapter.MyViewHolder> {
    private List<BalancesResponse> responses;
    private MyViewHolder mHolder;
    private ItemClickListener mItemClickListener;
    private Context mContext;

    public BalancesAdapter(List<BalancesResponse> responses, Context context) {
        this.responses = responses;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemBalancesBinding binding = ItemBalancesBinding.inflate(inflater, parent, false);
        mHolder = new MyViewHolder(binding);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final BalancesResponse response = this.responses.get(position);
        String pair = response.getPair();
        String amout = response.getValue();

        holder.getDataBinding().txtAmount.setText(amout);
        holder.getDataBinding().txtPair.setText(pair);


    }


    @Override
    public int getItemCount() {
        return this.responses.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemBalancesBinding binding;

        MyViewHolder(ItemBalancesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(this);

        }

        ItemBalancesBinding getDataBinding() {
            return this.binding;
        }


        @Override
        public void onClick(View view) {

        }
    }


    public void dataSetChangeList(List<BalancesResponse> list) {
        this.responses = list;
        notifyDataSetChanged();
    }


    public void setItemClickListener(ItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface ItemClickListener {
        void onClickItem(int position, BalancesResponse responses);
    }


}
