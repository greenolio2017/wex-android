package com.greenolio.app.wex.adapter;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.databinding.ItemBaseListInfoBinding;

import java.security.PublicKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BaseTabHomeAdapter extends
        RecyclerView.Adapter<BaseTabHomeAdapter.MyViewHolder> {
    private List<InfoResponse> mListDataInfo;
    private final Activity mContext;
    private MyViewHolder mHolder;
    private int mCurType = Constant.TAG_999;
    private ItemClickListener mItemClickListener;

    public BaseTabHomeAdapter(List<InfoResponse> mListDataDataCoin, Activity mContext, int curType) {
        this.mListDataInfo = mListDataDataCoin;
        this.mContext = mContext;
        this.mCurType = curType;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemBaseListInfoBinding binding = ItemBaseListInfoBinding.inflate(inflater, parent, false);
        mHolder = new MyViewHolder(binding);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final InfoResponse infoResponse = this.mListDataInfo.get(position);
        if (infoResponse == null) {
            return;
        }
        holder.bind(infoResponse);
        final String originTitle = infoResponse.getInfo().getOriginTitle();
        String title = infoResponse.getInfo().getTitleConvert();
        String vol = "Vol " + infoResponse.getTicker().getVol();
        String minMax = infoResponse.getTicker().getLow()
                + " - " + infoResponse.getTicker().getHigh();
        String lastPrice = infoResponse.getTicker().getLast();
        boolean isFavourite = infoResponse.getInfo().isFavourite();
        final int curType = infoResponse.getInfo().getSortType();

        holder.getDataBinding().txtCoinName.setText(title);
        holder.getDataBinding().txtVolCoin.setText(vol);
        holder.getDataBinding().txtCoinValue.setText(lastPrice);
        holder.getDataBinding().txtVolValue.setText(minMax);
        if (isFavourite || mCurType == Constant.TAG_995) {
            holder.getDataBinding().imgFavorite.setBackgroundResource(R.drawable.ico_favorite_on);
        } else {
            holder.getDataBinding().imgFavorite.setBackgroundResource(R.drawable.ico_favorite_off);
        }
        holder.getDataBinding().layoutFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> mapFavourite = DataInfoController.getInstance().loadDataFavourite(mContext);
                String favourite = mapFavourite.get(originTitle);
                if (favourite == null || favourite.isEmpty()) {
                    holder.getDataBinding().imgFavorite.setBackgroundResource(R.drawable.ico_favorite_on);
                    mapFavourite.put(originTitle, originTitle);
                    DataInfoController.getInstance().saveDataFavourite(mContext, mapFavourite);
                    infoResponse.getInfo().setFavourite(true);
                    HashMap<String, InfoResponse> mapInfo = DataInfoController.getInstance().getMapInfo();
                    mapInfo.put(originTitle, infoResponse);
                    DataInfoController.getInstance().setMapInfo(mapInfo);
                } else {
                    holder.getDataBinding().imgFavorite.setBackgroundResource(R.drawable.ico_favorite_off);
                    mapFavourite.remove(originTitle);
                    DataInfoController.getInstance().saveDataFavourite(mContext, mapFavourite);
                    infoResponse.getInfo().setFavourite(false);
                    HashMap<String, InfoResponse> mapInfo = DataInfoController.getInstance().getMapInfo();
                    mapInfo.put(originTitle, infoResponse);
                    DataInfoController.getInstance().setMapInfo(mapInfo);

                }
                if (curType != Constant.TAG_995) {
                    if (mItemClickListener != null) {
                        if (curType == Constant.TAG_999) {
                            mItemClickListener.onReloadDataPage(curType);
                        } else {
                            mItemClickListener.onReloadDataPage(curType);
                            mItemClickListener.onReloadDataPage(Constant.TAG_999);
                        }
                        mItemClickListener.onReloadDataPage(Constant.TAG_995);

                    }
                } else {
                    if (mItemClickListener != null) {
                        mItemClickListener.onReloadDataPage(Constant.TAG_995);
                        mItemClickListener.onReloadDataPage(Constant.TAG_999);
                    }
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return this.mListDataInfo.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemBaseListInfoBinding binding;

        MyViewHolder(ItemBaseListInfoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(this);
        }

        void bind(InfoResponse item) {
            binding.setModel(item);
        }

        ItemBaseListInfoBinding getDataBinding() {
            return this.binding;
        }


        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                int pos = getAdapterPosition();
                InfoResponse infoResponse = mListDataInfo.get(pos);
                if (mItemClickListener != null) {
                    mItemClickListener.onClickItem(pos, infoResponse);
                }
            }
        }
    }


    public void dataSetChangeList(List<InfoResponse> list) {
        this.mListDataInfo = list;
        notifyDataSetChanged();
    }

    public void setItemClickListener(ItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface ItemClickListener {
        void onClickItem(int position, InfoResponse infoResponse);
        void onReloadDataPage(int curTab);
    }

}
