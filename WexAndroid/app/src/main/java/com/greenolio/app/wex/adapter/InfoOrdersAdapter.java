package com.greenolio.app.wex.adapter;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.databinding.ItemOrdersBinding;

import java.util.List;

import static com.greenolio.app.wex.common.Constant.ORDER_TYPE_BUY;
import static com.greenolio.app.wex.common.Constant.ORDER_TYPE_SELL;


public class InfoOrdersAdapter extends
        RecyclerView.Adapter<InfoOrdersAdapter.MyViewHolder> {
    private List<DepthResponse> mDepthResponses;
    private MyViewHolder mHolder;
    private ItemClickListener mItemClickListener;

    public InfoOrdersAdapter(List<DepthResponse> depthResponses) {
        this.mDepthResponses = depthResponses;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemOrdersBinding binding = ItemOrdersBinding.inflate(inflater, parent, false);
        mHolder = new MyViewHolder(binding);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DepthResponse depthResponse = this.mDepthResponses.get(position);
        DepthResponse.Ask ask = depthResponse.getAsk();
        DepthResponse.Bids bids = depthResponse.getBids();
        holder.getDataBinding().txtOrderPrice.setText(ask == null ? "" : ask.getAskPrice());
        holder.getDataBinding().txtOrderAmount.setText(ask == null ? "" : ask.getAskAmount());

        holder.getDataBinding().txtBidPrice.setText(bids == null ? "" : bids.getBidPrice());
        holder.getDataBinding().txtBidAmount.setText(bids == null ? "" : bids.getBidAmount());


    }

    @Override
    public int getItemCount() {
        return this.mDepthResponses.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemOrdersBinding binding;

        MyViewHolder(ItemOrdersBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(this);
            binding.layoutBuy.setOnClickListener(this);
            binding.layoutSell.setOnClickListener(this);
        }

        ItemOrdersBinding getDataBinding() {
            return this.binding;
        }


        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.layout_buy) {
                int pos = getAdapterPosition();
                DepthResponse depthResponse = mDepthResponses.get(pos);
                depthResponse.setPosItemClick(pos);
                if (mItemClickListener != null) {
                    mItemClickListener.onClickItem(pos, depthResponse, ORDER_TYPE_BUY);
                }
            } else if (view.getId() == R.id.layout_sell) {
                int pos = getAdapterPosition();
                DepthResponse depthResponse = mDepthResponses.get(pos);
                depthResponse.setPosItemClick(pos);
                if (mItemClickListener != null) {
                    mItemClickListener.onClickItem(pos, depthResponse, ORDER_TYPE_SELL);
                }
            }
        }
    }


    public void dataSetChangeList(List<DepthResponse> list) {
        this.mDepthResponses = list;
        notifyDataSetChanged();
    }


    public void setItemClickListener(ItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface ItemClickListener {
        void onClickItem(int position, DepthResponse depthResponse, int orderType);
    }


}
