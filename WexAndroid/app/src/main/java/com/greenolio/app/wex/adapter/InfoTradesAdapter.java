package com.greenolio.app.wex.adapter;


import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.response.TradesResponse;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ItemTradesBinding;

import java.util.List;


public class InfoTradesAdapter extends
        RecyclerView.Adapter<InfoTradesAdapter.MyViewHolder> {
    private List<TradesResponse> mTradesResponses;
    private final Activity mContext;
    private MyViewHolder mHolder;
    private ItemClickListener mItemClickListener;

    public InfoTradesAdapter(List<TradesResponse> TradesResponses, Activity mContext) {
        this.mTradesResponses = TradesResponses;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemTradesBinding binding = ItemTradesBinding.inflate(inflater, parent, false);
        mHolder = new MyViewHolder(binding);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final TradesResponse tradesResponse = this.mTradesResponses.get(position);
        long time = Long.parseLong(tradesResponse.getDateTime());
        String timer = Utils.convertTimeStampToTime(time);
        String type = tradesResponse.getType();
        int color = ContextCompat.getColor(mContext, R.color.red);
        if (type.equalsIgnoreCase("ask")) {
            type = "SELL";
        } else {
            type = "BUY";
            color = ContextCompat.getColor(mContext, R.color.green);
        }
        holder.getDataBinding().txtType.setTextColor(color);
        holder.getDataBinding().txtAmount.setTextColor(color);
        holder.getDataBinding().txtPrice.setTextColor(color);
        holder.getDataBinding().txtDate.setTextColor(color);

        holder.getDataBinding().txtType.setText(type == null ? "" : type.toUpperCase());
        holder.getDataBinding().txtAmount.setText(tradesResponse.getAmount());

        holder.getDataBinding().txtPrice.setText(tradesResponse.getPrice());
        holder.getDataBinding().txtDate.setText(timer);


    }

    @Override
    public int getItemCount() {
        return this.mTradesResponses.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemTradesBinding binding;

        MyViewHolder(ItemTradesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        ItemTradesBinding getDataBinding() {
            return this.binding;
        }


        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                int pos = getAdapterPosition();
                TradesResponse TradesResponse = mTradesResponses.get(pos);
                if (mItemClickListener != null) {
                    mItemClickListener.onClickItem(pos, TradesResponse);
                }
            }
        }
    }


    public void dataSetChangeList(List<TradesResponse> list) {
        this.mTradesResponses = list;
        notifyDataSetChanged();
    }


    public void setItemClickListener(ItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface ItemClickListener {
        void onClickItem(int position, TradesResponse TradesResponse);
    }


}
