package com.greenolio.app.wex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.greenolio.app.wex.R;

public class IntroduceTabsPagerAdapter extends PagerAdapter {
    private final Context mContext;

    public IntroduceTabsPagerAdapter(Context context) {
        mContext = context;
    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = null;
        int resId = 0;

        switch (position) {
            case 0:
                resId = R.layout.fragment_introduce01;
                break;
            case 1:
                resId = R.layout.fragment_introduce02;
                break;
            case 2:
                resId = R.layout.fragment_introduce03;
                break;
            default:
                break;
        }

        if (resId != 0) v = inflater.inflate(resId, container, false);
        if (v != null) container.addView(v);
        if (v == null) {
            v = inflater.inflate(resId, container, false);
        }

        return v;
    }
}
