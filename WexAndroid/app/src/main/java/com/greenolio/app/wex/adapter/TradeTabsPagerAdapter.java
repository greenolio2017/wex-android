package com.greenolio.app.wex.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.fragment.TabBuyFragment;
import com.greenolio.app.wex.fragment.TabSellFragment;
import java.util.List;

public class TradeTabsPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> listFragment;


    public TradeTabsPagerAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        listFragment = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Fragment  fragmentBuy = listFragment.get(position);
                if (fragmentBuy == null) {
                    fragmentBuy = TabBuyFragment.getInstance(Constant.TAG_104);
                }
                return fragmentBuy;
            case 1:
                Fragment  fragmentSell = listFragment.get(position);
                if (fragmentSell == null) {
                    fragmentSell = TabSellFragment.getInstance(Constant.TAG_105);
                }
                return fragmentSell;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        if (listFragment != null && listFragment.size() != 0) {
            return listFragment.size();
        }
        return 0;
    }


}
