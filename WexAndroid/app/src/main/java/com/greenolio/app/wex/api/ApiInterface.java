package com.greenolio.app.wex.api;



import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface ApiInterface {

    @GET("api/3/info")
    Call<ResponseBody> getDataInfo();

    @GET("api/3/ticker/{title}")
    Call<ResponseBody> getDataTicker(@Path("title") final String groupTitle);

    @GET("api/3/depth/{title}")
    Call<ResponseBody> getDataDepth(@Path("title") final String title);

    @GET("api/3/trades/{title}")
    Call<ResponseBody> getDataTrades(@Path("title") final String title);


}
