package com.greenolio.app.wex.api;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomCallBack<T> implements Callback<T> {

    private ProgressDialog mProgressDialog;
    private TextView mTvMessage;
    private boolean mIsDismissDialog = true;


    public CustomCallBack() {

    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        dismissDialog(mIsDismissDialog);
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        if (autoDismissDialog) {
            dismissDialog(mIsDismissDialog);
        }
    }

    private final boolean autoDismissDialog = true;

    private void dismissDialog(boolean isDismiss) {
        mIsDismissDialog = isDismiss;
        if (mIsDismissDialog && mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
