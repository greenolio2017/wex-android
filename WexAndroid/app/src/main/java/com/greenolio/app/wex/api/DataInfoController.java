package com.greenolio.app.wex.api;


import android.content.Context;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.greenolio.app.wex.Application;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.PreferencesUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DataInfoController {
    private static DataInfoController mInstance;
    private final List<InfoResponse> mListInfoResponse = new ArrayList<>();
    private HashMap<String, InfoResponse> mMapInfo = new HashMap();
    private List<DepthResponse> mListCurDepth;
    private DepthResponse mDepthResponse;
    private String[] mArrOriginTitle;
    private String mGroupTitle = "";


    public static DataInfoController getInstance() {
        if (mInstance == null) {
            mInstance = new DataInfoController();
        }
        return mInstance;
    }

    private DataInfoController() {
    }


    public List<DepthResponse> getListDepthResponse() {
        return mListCurDepth;
    }

    public void setListCurDepth(List<DepthResponse> mListCurDepth) {
        this.mListCurDepth = mListCurDepth;
    }

    public DepthResponse getDepthResponse() {
        return mDepthResponse;
    }

    public void setDepthResponse(DepthResponse mDepthResponse) {
        this.mDepthResponse = mDepthResponse;
    }

    public HashMap<String, InfoResponse> getMapInfo() {
        return mMapInfo;
    }

    public void setMapInfo(HashMap<String, InfoResponse> mMapInfo) {
        this.mMapInfo = mMapInfo;
    }

    public String[] getArrOriginTitle() {
        return mArrOriginTitle;
    }

    public String getGroupTitle() {
        return mGroupTitle;
    }

    public void parserDataInfo(JsonObject objectInfo) {
        if (objectInfo != null) {
            mArrOriginTitle = getStringArray(objectInfo);
            if (mArrOriginTitle != null && mArrOriginTitle.length != 0) {
                JsonElement listInfo = new JsonParser().parse(objectInfo.getAsJsonObject().get("pairs").toString());
                int serverTime = Integer.parseInt("" + objectInfo.getAsJsonObject().get("server_time").toString());
                loadDataInfoFromJson(listInfo, mArrOriginTitle, serverTime);
            }

        }
    }


    public void parserDataTicker(JsonObject objectTicker) {
        if (objectTicker != null) {
            if (mArrOriginTitle != null && mArrOriginTitle.length != 0) {
                for (String value : mArrOriginTitle) {
                    String jsonTicker = objectTicker.getAsJsonObject().get(value).toString();
                    InfoResponse infoResponse = mMapInfo.get(value);
                    if (infoResponse != null) {
                        InfoResponse.Ticker ticker = infoResponse.parserDataTicker(jsonTicker);
                        infoResponse.setTicker(ticker);
                        mMapInfo.put(value, infoResponse);

                    }
                }
            }

            //new JsonParser().parse(objectTicker.getAsJsonObject().get("pairs").toString()).getAsJsonObject().get("btc_usd");
        }
    }

    private void loadDataInfoFromJson(JsonElement listInfo, String[] arrInfo, int serverTime) {
        InfoResponse infoResponse;
        mListInfoResponse.clear();
        Map<String, String> mapFavourite = loadDataFavourite(Application.getApplication());
        for (String value : arrInfo) {
            infoResponse = new InfoResponse();
            String titleConvert = value.replace("_", " / ").toUpperCase();
            String jsonPrice = listInfo.getAsJsonObject().get(value).toString();
            int sortType = getSortTypeInfo(value);
            InfoResponse.Info info = infoResponse.parserDataInfo(serverTime, value, titleConvert, jsonPrice);
            info.setSortType(sortType);
            String favourite = mapFavourite.get(value);
            if (favourite != null && !favourite.isEmpty()) {
                info.setFavourite(true);
            } else {
                info.setFavourite(false);
            }

            infoResponse.setInfo(info);
            mMapInfo.put(value, infoResponse);

        }
    }

    private int getSortTypeInfo(String title) {
        int result = Constant.TAG_995;
        if (title.contains("_btc")) {
            result = Constant.TAG_996;
        } else if (title.contains("_eth")) {
            result = Constant.TAG_997;
        } else if (title.contains("_usd")) {
            result = Constant.TAG_998;
        }
        return result;
    }


    private String[] getStringArray(JsonObject PairsArray) {
        if (PairsArray == null)
            return null;

        JsonObject jsonPairs = PairsArray.get("pairs").getAsJsonObject();
        JSONObject js = null;

        try {
            js = new JSONObject(jsonPairs.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (js == null) {
            return null;
        }

        String[] arr = new String[js.length()];
        mGroupTitle = "";
        for (int i = 0; i < arr.length; i++) {
            try {
                String title = js.names().get(i).toString();
                arr[i] = title;
                if (mGroupTitle.isEmpty()) {
                    mGroupTitle = title;
                } else {
                    mGroupTitle = mGroupTitle + "-" + title;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return arr;
    }

    public void saveDataFavourite(Context context, Map<String, String> inputMap) {
        JSONObject jsonObject = new JSONObject(inputMap);
        String jsonString = jsonObject.toString();
        PreferencesUtil.getInstance(context).putString(Constant.KEY_PREF_SAVE_DATA_FAVOURITE, jsonString);
    }

    public Map<String, String> loadDataFavourite(Context context) {
        Map<String, String> outputMap = new HashMap<>();
        try {
            String jsonString = PreferencesUtil.getInstance(context).getString(Constant.KEY_PREF_SAVE_DATA_FAVOURITE, (new JSONObject()).toString());
            JSONObject jsonObject = new JSONObject(jsonString);
            Iterator<String> keysItr = jsonObject.keys();
            while (keysItr.hasNext()) {
                String key = keysItr.next();
                String value = (String) jsonObject.get(key);
                outputMap.put(key, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputMap;
    }

    private void clearOldData() {

    }


}
