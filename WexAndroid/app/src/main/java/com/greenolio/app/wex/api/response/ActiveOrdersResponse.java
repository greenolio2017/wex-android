package com.greenolio.app.wex.api.response;

public class ActiveOrdersResponse {
    private String pair;
    private String type;
    private String amount;
    private String rate;
    private String timeStamp;
    private String status;
    private String orderID;
    private String isYourOrder;


    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getIsYourOrder() {
        return isYourOrder;
    }

    public void setIsYourOrder(String isYourOrder) {
        this.isYourOrder = isYourOrder;
    }
}
