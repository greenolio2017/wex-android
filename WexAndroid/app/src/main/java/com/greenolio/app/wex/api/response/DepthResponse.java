package com.greenolio.app.wex.api.response;


public class DepthResponse {

    private Ask ask;
    private Bids bids;
    private int posItemClick;

    public Ask getAsk() {
        return ask;
    }

    public void setAsk(Ask ask) {
        this.ask = ask;
    }

    public Bids getBids() {
        return bids;
    }

    public void setBids(Bids bids) {
        this.bids = bids;
    }

    public int getPosItemClick() {
        return posItemClick;
    }

    public void setPosItemClick(int posItemClick) {
        this.posItemClick = posItemClick;
    }

    public class Ask {
        private String askPrice;
        private String askAmount;

        public String getAskPrice() {
            return askPrice;
        }

        public void setAskPrice(String askPrice) {
            this.askPrice = askPrice;
        }

        public String getAskAmount() {
            return askAmount;
        }

        public void setAskAmount(String askAmount) {
            this.askAmount = askAmount;
        }
    }

    public class Bids {
        private String bidPrice;
        private String bidAmount;

        public String getBidPrice() {
            return bidPrice;
        }

        public void setBidPrice(String bidPrice) {
            this.bidPrice = bidPrice;
        }

        public String getBidAmount() {
            return bidAmount;
        }

        public void setBidAmount(String bidAmount) {
            this.bidAmount = bidAmount;
        }
    }
}
