package com.greenolio.app.wex.api.response;


import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InfoResponse {
    private Info info;
    private Ticker ticker;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public Ticker getTicker() {
        return ticker;
    }

    public void setTicker(Ticker ticker) {
        this.ticker = ticker;
    }

    public class Info {
        private String originTitle;
        private String titleConvert;
        private long serverTime;
        private int sortType;
        private boolean isFavourite;
        @SerializedName("decimal_places")
        @Expose
        private String decimalPlaces;
        @SerializedName("min_price")
        @Expose
        private String minPrice;
        @SerializedName("max_price")
        @Expose
        private String maxPrice;
        @SerializedName("min_amount")
        @Expose
        private String minAmout;
        @SerializedName("hidden")
        @Expose
        private int hiden;
        @SerializedName("fee")
        @Expose
        private String fee;

        public String getOriginTitle() {
            return originTitle;
        }

        void setOriginTitle(String originTitle) {
            this.originTitle = originTitle;
        }

        public String getTitleConvert() {
            return titleConvert;
        }

        void setTitleConvert(String titleConvert) {
            this.titleConvert = titleConvert;
        }

        public long getServerTime() {
            return serverTime;
        }

        void setServerTime(long serverTime) {
            this.serverTime = serverTime;
        }

        public String getDecimalPlaces() {
            return decimalPlaces;
        }

        public void setDecimalPlaces(String decimalPlaces) {
            this.decimalPlaces = decimalPlaces;
        }

        public String getMinPrice() {
            return minPrice;
        }

        public void setMinPrice(String minPrice) {
            this.minPrice = minPrice;
        }

        public String getMaxPrice() {
            return maxPrice;
        }

        public void setMaxPrice(String maxPrice) {
            this.maxPrice = maxPrice;
        }

        public String getMinAmout() {
            return minAmout;
        }

        public void setMinAmout(String minAmout) {
            this.minAmout = minAmout;
        }

        public int getHiden() {
            return hiden;
        }

        public void setHiden(int hiden) {
            this.hiden = hiden;
        }

        public String getFee() {
            return fee;
        }

        public void setFee(String fee) {
            this.fee = fee;
        }

        public int getSortType() {
            return sortType;
        }

        public void setSortType(int sortType) {
            this.sortType = sortType;
        }

        public boolean isFavourite() {
            return isFavourite;
        }

        public void setFavourite(boolean favourite) {
            isFavourite = favourite;
        }
    }

    public Info parserDataInfo(int serverTime, String originTitle, String titleConvert, String json) {
        Gson gson = new Gson();
        if (json != null && !json.isEmpty()) {
            Info info = gson.fromJson(json, Info.class);
            if (info != null) {
                info.setOriginTitle(originTitle);
                info.setTitleConvert(titleConvert);
                info.setServerTime(serverTime);
                return info;
            }
        }
        return null;
    }

    public Ticker parserDataTicker(String json) {
        try {
            Gson gson = new Gson();
            if (json != null && !json.isEmpty()) {
                Ticker ticker = gson.fromJson(json, Ticker.class);
                if (ticker != null) {
                    return ticker;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public class Ticker {
        @SerializedName("high")
        @Expose
        private String high;
        @SerializedName("low")
        @Expose
        private String low;
        @SerializedName("avg")
        @Expose
        private String avg;
        @SerializedName("vol")
        @Expose
        private String vol;
        @SerializedName("vol_cur")
        @Expose
        private String volCur;
        @SerializedName("last")
        @Expose
        private String last;
        @SerializedName("buy")
        @Expose
        private String buy;
        @SerializedName("sell")
        @Expose
        private String sell;
        @SerializedName("updated")
        @Expose
        private long updated;

        public String getHigh() {
            return high;
        }

        public void setHigh(String high) {
            this.high = high;
        }

        public String getLow() {
            return low;
        }

        public void setLow(String low) {
            this.low = low;
        }

        public String getAvg() {
            return avg;
        }

        public void setAvg(String avg) {
            this.avg = avg;
        }

        public String getVol() {
            return vol;
        }

        public void setVol(String vol) {
            this.vol = vol;
        }

        public String getVolCur() {
            return volCur;
        }

        public void setVolCur(String volCur) {
            this.volCur = volCur;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }

        public String getBuy() {
            return buy;
        }

        public void setBuy(String buy) {
            this.buy = buy;
        }

        public String getSell() {
            return sell;
        }

        public void setSell(String sell) {
            this.sell = sell;
        }

        public long getUpdated() {
            return updated;
        }

        public void setUpdated(long updated) {
            this.updated = updated;
        }
    }


}
