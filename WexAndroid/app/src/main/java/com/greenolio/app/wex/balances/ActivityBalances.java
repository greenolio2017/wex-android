package com.greenolio.app.wex.balances;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.adapter.ActiveOrdersAdapter;
import com.greenolio.app.wex.adapter.BalancesAdapter;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityBalancesBinding;

import java.util.List;

public class ActivityBalances extends BaseActivity implements View.OnClickListener, BalancesView, BalancesAdapter.ItemClickListener {
    private BalancesPresenter mPresenter;
    private ActivityBalancesBinding mBindingOrders;
    private BalancesAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBindingOrders = DataBindingUtil.setContentView(this, R.layout.activity_balances);
        initView();
        initData();

    }

    private void initView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mBindingOrders.recyclerView.setLayoutManager(layoutManager);
        mBindingOrders.imgBack.setOnClickListener(this);

    }

    private void initData() {
        if (mPresenter == null) {
            BalancesModel mModel = new BalancesModel(this);
            mPresenter = new BalancesPresenter(this, this, mModel);
        }
        mPresenter.loadDataOrders();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_back:
                finish();
                break;

        }

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClickItem(int position, BalancesResponse responses) {

    }

    @Override
    public void loadDataSuccess(final List<BalancesResponse> responseList) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mAdapter == null) {
                    mAdapter = new BalancesAdapter(responseList, ActivityBalances.this);
                    mAdapter.setItemClickListener(ActivityBalances.this);
                    mBindingOrders.recyclerView.setAdapter(mAdapter);
                } else {
                    mAdapter.dataSetChangeList(responseList);
                }
            }
        });
    }
}
