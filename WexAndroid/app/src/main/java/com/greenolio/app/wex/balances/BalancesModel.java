package com.greenolio.app.wex.balances;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.greenolio.app.wex.api.ActiveOrdersAPI;
import com.greenolio.app.wex.api.CancelOrderAPI;
import com.greenolio.app.wex.api.GetInfo;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

class BalancesModel {
    private final Context mContext;
    private BalancesResult mResult;

    public BalancesModel(Context mContext) {
        this.mContext = mContext;
    }

    public void loadDataInfo() {
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, JsonObject> task = new AsyncTask<Void, Void, JsonObject>() {
            @Override
            protected JsonObject doInBackground(Void... voids) {
                JsonObject objectFund = null;
                JsonObject jsonObject = GetInfo.getInfoTradeObj(mContext);
                if (jsonObject != null) {
                    boolean isSuccess = GetInfo.getSuccess(jsonObject);
                    if (isSuccess) {
                        objectFund = GetInfo.getReturn(jsonObject);
                        return objectFund;
                    } else {
                        String error = jsonObject.getAsJsonObject().get("error").toString();
                        mResult.loadInfoFail(error);
                        return null;
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(JsonObject response) {
                super.onPostExecute(response);
                if (response != null) {
                    mResult.loadInfoSuccess(response);
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }


    public void setResult(BalancesResult result) {
        this.mResult = result;
    }
}
