package com.greenolio.app.wex.balances;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.component.ProgressDialog;

import java.util.ArrayList;
import java.util.List;

public class BalancesPresenter implements BalancesResult {
    private final AppCompatActivity mContext;
    private final BalancesView mView;
    private final BalancesModel mModel;
    private ProgressDialog mDialogLoading;
    private List<ActiveOrdersResponse> mResponseList;

    public BalancesPresenter(AppCompatActivity mContext, BalancesView mView, BalancesModel model) {
        this.mContext = mContext;
        this.mView = mView;
        this.mModel = model;
        this.mModel.setResult(this);


    }

    public void loadDataOrders() {
        if (mDialogLoading == null) {
            mDialogLoading = new ProgressDialog(mContext);
        }
        mDialogLoading.show();
        mModel.loadDataInfo();

    }


    @Override
    public void loadInfoSuccess(JsonObject objectFund) {
        if (objectFund != null) {
            String[] lisPair = DataInfoController.getInstance().getArrOriginTitle();
            List<BalancesResponse> responseList = new ArrayList<>();
            if (lisPair != null && lisPair.length != 0) {
                for (String value : lisPair) {
                    String[] arrTitle = value.split("_");
                    if (arrTitle.length == 2) {
                        String firtTitle = arrTitle[0];
                        String secondTitle = arrTitle[1];
                        if (objectFund.getAsJsonObject("funds") != null) {
                            String balanceBuy = objectFund.getAsJsonObject("funds").getAsJsonObject().get(secondTitle).toString();
                            if (balanceBuy != null && !balanceBuy.isEmpty() && !balanceBuy.equals("0")) {
                                if (isCheckExistPair(responseList, secondTitle, balanceBuy)) {
                                    continue;
                                }
                                BalancesResponse response = new BalancesResponse();
                                response.setPair(secondTitle.toUpperCase());
                                response.setValue(balanceBuy);
                                response.setOriginPair(value);
                                responseList.add(response);
                            }
                            String balanceSell = objectFund.getAsJsonObject("funds").getAsJsonObject().get(firtTitle).toString();
                            if (balanceSell != null && !balanceSell.isEmpty() && !balanceSell.equals("0")) {
                                if (isCheckExistPair(responseList, firtTitle, balanceSell)) {
                                    continue;
                                }
                                BalancesResponse response = new BalancesResponse();
                                response.setPair(firtTitle.toUpperCase());
                                response.setValue(balanceSell);
                                response.setOriginPair(value);
                                responseList.add(response);
                            }

                        }
                    }
                }
            }
            mView.loadDataSuccess(responseList);


        }
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
    }

    private boolean isCheckExistPair(List<BalancesResponse> responseList, String curPair, String curValue) {
        if (responseList.size() != 0){
            for (BalancesResponse response: responseList) {
                if (curPair.toUpperCase().equals(response.getPair()) && curValue.equals(response.getValue())) {
                    return true;
                }
            }
        }

        return false;
    }



    @Override
    public void loadInfoFail(final String error) {
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null && !error.isEmpty()) {
                    final DialogHelper dialogHelper = new DialogHelper(mContext);
                    dialogHelper.showAlert("Notify", error, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogHelper.dismiss();
                        }
                    });
                }
            }
        });
    }
}
