package com.greenolio.app.wex.balances;

public class BalancesResponse {
    private String pair;
    private String value;
    private String originPair;

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getValue() {
        return value == null ? "" : this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOriginPair() {
        return originPair;
    }

    public void setOriginPair(String originPair) {
        this.originPair = originPair;
    }
}
