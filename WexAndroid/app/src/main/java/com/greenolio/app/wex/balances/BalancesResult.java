package com.greenolio.app.wex.balances;

import com.google.gson.JsonObject;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;

import java.util.List;

interface BalancesResult {
     void loadInfoSuccess(JsonObject objectFund);
     void loadInfoFail(String error);
}
