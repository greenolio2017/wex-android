package com.greenolio.app.wex.balances;


import com.greenolio.app.wex.api.response.ActiveOrdersResponse;

import java.util.List;

interface BalancesView {
        void loadDataSuccess(List<BalancesResponse> responseList);
}
