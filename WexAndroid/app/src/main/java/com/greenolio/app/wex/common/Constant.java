package com.greenolio.app.wex.common;


public class Constant {

    public static final int PERMISSIONS_CODE = 101;
    public static final String URL_PRIVATE = "https://wex.nz/tapi";

    //key save tag fragment
    //type Fvrt
    public static final int TAG_995 = 995;
    //type BTC
    public static final int TAG_996 = 996;
    //type ETH
    public static final int TAG_997 = 997;
    //type USD
    public static final int TAG_998 = 998;
    //type ALL
    public static final int TAG_999 = 999;
    //type reload data of ALL pages
    public static final int TAG_ALL_HOMEPAGE = 100;

    //type Chart
    public static final int TAG_101 = 101;
    //type Orders
    public static final int TAG_102 = 102;
    //type Trades
    public static final int TAG_103 = 103;
    //type Buy
    public static final int TAG_104 = 104;
    //type Sell
    public static final int TAG_105 = 105;

    public static final int ORDER_TYPE_BUY = 20;
    public static final int ORDER_TYPE_SELL = 21;
    public static final int ORDER_TYPE_BUY_AND_SELL = 22;


    //preferences key
    public static final String KEY_PREF_DISPLAY_INTRODUCE = "key_display_introduce";
    public static final String KEY_PREF_DISPLAY_FIRST_FAVOURITE = "key_display_fist_favourite";
    public static final String KEY_PREF_SAVE_DATA_FAVOURITE = "key_save_data_favourite";

    //key broadcast
    public static final String ACTION_BROADCAST_RELOAD_INFO = "action_broadcast_reload_data_info";
    public static final String EXTRAS_BROADCAST_RELOAD_TYPE = "extras_broadcast_reload_type";

    //key intents
    public static final String INTENT_EXTRAS_PAIR_NAME = "extras_info_pair_name";
    public static final String INTENT_EXTRAS_ORDER_TYPE = "extras_info_order_type";
    public static final String INTENT_EXTRAS_TYPE_DISPLAY_PINCODE = "extras_type_display_pin";
    // key setting
    public static final String KEY_PREF_KEY_SETTING_KEY = "key_setting_key";
    public static final String KEY_PREF_KEY_SETTING_SECRET = "key_setting_secret";
    public static final String KEY_PREF_KEY_SAVE_PINCODE = "key_save_pincode";
    public static final String KEY_PREF_KEY_ACTIVE_PIN = "key_active_pincode";
    public static final String KEY_PREF_KEY_TYPE_DIGIT_PINCODE = "key_type_digit_pincode";

    //key trade complete
    public static final String KEY_EXTRAS_AMOUNT = "intent_extras_amount";
    public static final String KEY_EXTRAS_PRICE = "intent_extras_price";
    public static final String KEY_EXTRAS_TOTAL = "intent_extras_total";

    //key extras intent pincode
    public static final String INTENT_EXTRAS_PINCODE_DEFAULT = "pincode_default_input";
    public static final String INTENT_EXTRAS_PINCODE_NEW = "pincode_new";
    public static final String INTENT_EXTRAS_PINCODE_RESET = "pincode_reset";
    public static final String INTENT_EXTRAS_PINCODE_CHANGE = "pincode_change";

    public static final int TYPE_DEFAULT = 4;
    public static final int TYPE_6_DIGIT = 6;
    public static final int TYPE_CUSTOM_NUMERIC_CODE = 8;
    public static final int TYPE_ALPHA_NUMERIC = 10;


}
