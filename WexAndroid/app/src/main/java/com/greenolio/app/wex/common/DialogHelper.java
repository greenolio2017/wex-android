package com.greenolio.app.wex.common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.greenolio.app.wex.R;


public class DialogHelper {
    private Dialog dialog;
    private Context mContext;

    public DialogHelper(Context context) {
        dialog = new Dialog(context);
        this.mContext = context;
    }

    public void showAlert(String title, String message, OnClickListener OnClickListener) {
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dialog);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        TextView mDialogTitle = dialog.findViewById(R.id.tvTitle);
        TextView mDialogMessage = dialog.findViewById(R.id.txt_message);
        if (mDialogTitle != null) {
            mDialogTitle.setText(title);
        }
        if (mDialogMessage != null) {
            mDialogMessage.setText(message);
        }
        btnOk.setOnClickListener(OnClickListener);
        if (dialog.getWindow() == null) {
            return;
        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
       // dismiss();
        dialog.show();

    }

    public void showConfirmResetPin(OnClickListener OnClickListener) {
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_reset_pin);
        Button btnReset = dialog.findViewById(R.id.btnReset);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(OnClickListener);
        if (dialog.getWindow() == null) {
            return;
        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dismiss();
        dialog.show();
    }

    public void showConfirmCancelOrder(OnClickListener OnClickListener, String content) {
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_reset_pin);
        Button btnReset = dialog.findViewById(R.id.btnReset);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        TextView txtTitle = dialog.findViewById(R.id.tvTitle);
        TextView txtContent = dialog.findViewById(R.id.txt_message);

        txtTitle.setText(this.mContext.getResources().getString(R.string.dialog_title_cancel_order));
        txtContent.setText(content);

        btnReset.setText(this.mContext.getResources().getString(R.string.dialog_text_not_agree));
        btnCancel.setText(this.mContext.getResources().getString(R.string.dialog_text_agree));

        btnReset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(OnClickListener);
        if (dialog.getWindow() == null) {
            return;
        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dismiss();
        dialog.show();
    }


    public void dismiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

}
