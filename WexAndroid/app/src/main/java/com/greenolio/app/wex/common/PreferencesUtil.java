package com.greenolio.app.wex.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;


public class PreferencesUtil {

  private Gson gson;
  private static PreferencesUtil mInstance;
  private static SharedPreferences mPrefs;

  private PreferencesUtil() {
  }

  public static PreferencesUtil getInstance(Context context) {
    if (mInstance == null) {
      mInstance = new PreferencesUtil();
      mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    return mInstance;
  }

  public void clearSharedPreferences(Context mContext) {
    PreferenceManager.getDefaultSharedPreferences(mContext).edit().clear().apply();
  }

  public void putString(String key, String value) {
    mPrefs.edit().putString(key, value).apply();
  }

  public String getString(String key, String defValue) {
    return mPrefs.getString(key, defValue);
  }

  public void putBoolean(String key, boolean value) {
    mPrefs.edit().putBoolean(key, value).apply();
  }

  public boolean getBoolean(String key, boolean defValue) {
    return mPrefs.getBoolean(key, defValue);
  }

  public void putInt(String key, int value) {
    mPrefs.edit().putInt(key, value).apply();
  }

  public int getInt(String key, int defValue) {
    return mPrefs.getInt(key, defValue);
  }

  public void putLong(String key, long value) {
    mPrefs.edit().putLong(key, value).apply();
  }

  public long getLong(String key, long defValue) {
    return mPrefs.getLong(key, defValue);
  }

  public void remove(String key) {
    mPrefs.edit().remove(key).apply();
  }

  public Gson getGson() {
    return gson;
  }

  public void setGson(Gson gson) {
    this.gson = gson;
  }

}
