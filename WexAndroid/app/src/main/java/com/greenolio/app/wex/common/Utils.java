package com.greenolio.app.wex.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.greenolio.app.wex.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class Utils {


    public static boolean checkPermissionRequired(Context activity) {
        return checkWriteExternalPermission(activity) && checkReadExternalPermission(activity)
                && checkInternetPermission(activity);
    }

    public static void requestPermission(Activity activity) {
        List<String> permissionRequest = new ArrayList<>();

        if (!checkWriteExternalPermission(activity))
            permissionRequest.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (!checkReadExternalPermission(activity))
            permissionRequest.add(Manifest.permission.READ_EXTERNAL_STORAGE);

        if (!checkInternetPermission(activity))
            permissionRequest.add(Manifest.permission.ACCESS_NETWORK_STATE);

        if (permissionRequest.size() > 0) {
            ActivityCompat.requestPermissions(activity, permissionRequest.toArray(new String[permissionRequest.size()]),
                    Constant.PERMISSIONS_CODE);

        }
    }

    /**
     * check runtime permission android api >=23
     *
     * @param activity activity
     */
    private static boolean checkWriteExternalPermission(Context activity) {
        int locationPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return locationPermission == PackageManager.PERMISSION_GRANTED;
    }

    private static boolean checkReadExternalPermission(Context activity) {
        int locationPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        return locationPermission == PackageManager.PERMISSION_GRANTED;
    }


    private static boolean checkInternetPermission(Context activity) {
        int locationPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_NETWORK_STATE);
        return locationPermission == PackageManager.PERMISSION_GRANTED;
    }


    private static final SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMdd.HHmmss.sss");


    public static String convertTimeStampToTime(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        return formatter.format(date);
    }

    public static String convertTimeStampToDate(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        return formatter.format(date);
    }

    /**
     * CHECK WHETHER INTERNET CONNECTION IS AVAILABLE OR NOT
     */
    public static boolean isCheckConnection(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            // connected to the internet
            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI)
            // connected to wifi
            {
                return true;
            } else return activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE;

        }
        return false;
    }

    public static long getNonceParam() {
        return System.currentTimeMillis() / 1000;
    }

    public static String getKeyParam(Context context) {
        return PreferencesUtil.getInstance(context).getString(Constant.KEY_PREF_KEY_SETTING_KEY, " ");
    }

    public static String getSecretParam(Context context) {
        return PreferencesUtil.getInstance(context).getString(Constant.KEY_PREF_KEY_SETTING_SECRET, " ");
    }

    //log events firebase
    public static void logEventFireBase(FirebaseAnalytics mFirebaseAnalytics, String eventName, String eventContent) {
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.ITEM_NAME, eventContent);
        if (mFirebaseAnalytics != null)
            mFirebaseAnalytics.logEvent(eventName, params);
    }

    public static boolean isAppInstalledOrNot(String packageName, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return pm.getApplicationInfo(packageName, 0).enabled;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

}
