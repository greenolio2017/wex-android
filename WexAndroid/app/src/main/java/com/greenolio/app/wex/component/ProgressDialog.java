package com.greenolio.app.wex.component;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.ImageView;

import com.greenolio.app.wex.R;

public class ProgressDialog extends Dialog {
    private final int[] arrIcon = {
            R.drawable.loading_01, R.drawable.loading_02, R.drawable.loading_03, R.drawable.loading_04,
            R.drawable.loading_05, R.drawable.loading_06, R.drawable.loading_07, R.drawable.loading_08,
            R.drawable.loading_09, R.drawable.loading_10, R.drawable.loading_11, R.drawable.loading_12,
            R.drawable.loading_13, R.drawable.loading_14, R.drawable.loading_15, R.drawable.loading_16,
            R.drawable.loading_17, R.drawable.loading_18, R.drawable.loading_19, R.drawable.loading_20,
            R.drawable.loading_21, R.drawable.loading_22, R.drawable.loading_23, R.drawable.loading_24,
            R.drawable.loading_25, R.drawable.loading_26, R.drawable.loading_27, R.drawable.loading_28,
            R.drawable.loading_29, R.drawable.loading_30, R.drawable.loading_31, R.drawable.loading_32,
            R.drawable.loading_33
    };
    private Handler mHandler = null;
    private int mCoutLoadIcon = 0;
    private AppCompatActivity mActivity;
    private static final long TIMER_RUNABLE_LOADING = 50;
    private final ImageView iv;

    public ProgressDialog(AppCompatActivity context) {
        super(context, R.style.TransparentProgressDialog);
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mActivity = context;
        setContentView(R.layout.dialog_loading_image);
        iv = findViewById(R.id.img_splash);
        this.setCancelable(false);
        this.setCanceledOnTouchOutside(true);
    }

    public void dissmissDialog() {
        if (this.isShowing()) {
            removeCallback();
            this.dismiss();
        }
    }


    @Override
    public void show() {
        super.show();
        startLoading();
    }

    private final Runnable loadImageRunnable = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public void run() {
            // handle count time update first gps
            if (mHandler == null) return;
            mCoutLoadIcon++;
            if (mCoutLoadIcon >= arrIcon.length) {
                mCoutLoadIcon = 0;
            }
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    iv.setImageResource(0);
                    iv.setImageResource(arrIcon[mCoutLoadIcon]);
                }
            });
            if (mHandler != null)
                mHandler.postDelayed(this, TIMER_RUNABLE_LOADING);

        }
    };

    private void startLoading() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mHandler.post(loadImageRunnable);
        mCoutLoadIcon = 0;
    }

    private void removeCallback() {
        mHandler.removeCallbacks(loadImageRunnable);
        mHandler = null;

    }

}
