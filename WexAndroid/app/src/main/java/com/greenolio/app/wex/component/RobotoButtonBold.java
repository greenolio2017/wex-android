package com.greenolio.app.wex.component;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.greenolio.app.wex.helper.FontHelper;


public class RobotoButtonBold extends AppCompatButton {

    public RobotoButtonBold(Context context) {
        super(context);
        init(context);
    }

    public RobotoButtonBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RobotoButtonBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        Typeface typeface = FontHelper.robotoCondensedBold(context);
        if (typeface != null) {
            setTypeface(typeface);
        }
    }
}
