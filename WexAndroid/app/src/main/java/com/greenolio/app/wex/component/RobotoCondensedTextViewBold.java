package com.greenolio.app.wex.component;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.greenolio.app.wex.helper.FontHelper;


public class RobotoCondensedTextViewBold extends AppCompatTextView {

    public RobotoCondensedTextViewBold(Context context) {
        super(context);
        init(context);
    }

    public RobotoCondensedTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RobotoCondensedTextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        Typeface typeface = FontHelper.robotoCondensedBold(context);
        if (typeface != null) {
            setTypeface(typeface);
        }
    }
}
