package com.greenolio.app.wex.component;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.greenolio.app.wex.helper.FontHelper;


public class RobotoEditText extends android.support.v7.widget.AppCompatEditText {

    public RobotoEditText(Context context) {
        super(context);
        init(context);
    }

    public RobotoEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RobotoEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        Typeface typeface = FontHelper.robotoCondensedRegular(context);
        if (typeface != null) {
            setTypeface(typeface);
        }
    }
}
