package com.greenolio.app.wex.component;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.greenolio.app.wex.helper.FontHelper;


public class RobotoTextView extends AppCompatTextView {

    public RobotoTextView(Context context) {
        super(context);
        init(context);
    }

    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RobotoTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        Typeface typeface = FontHelper.robotoRegular(context);
        if (typeface != null) {
            setTypeface(typeface);
        }
    }
}
