package com.greenolio.app.wex.fragment;


import android.support.v4.app.Fragment;
import com.greenolio.app.wex.activity.BaseActivity;


public class BaseFrag extends Fragment {
    BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

}