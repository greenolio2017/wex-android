package com.greenolio.app.wex.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.adapter.BaseTabHomeAdapter;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.FragmentBaseHomeBinding;
import com.greenolio.app.wex.infoscreens.ActivityInfoScreens;
import com.greenolio.app.wex.main.MainActivity;
import com.greenolio.app.wex.tabhome.TabHomeModel;
import com.greenolio.app.wex.tabhome.TabHomePresenter;
import com.greenolio.app.wex.tabhome.TabHomeView;

import java.util.List;


public class BaseTabHomeFragment extends BaseFrag implements TabHomeView, BaseTabHomeAdapter.ItemClickListener {
    private int mCurTab = 0;
    private FragmentBaseHomeBinding mBaseTabBinding;
    private TabHomePresenter mPresenter;
    private TabHomeModel mModel;
    private BaseTabHomeAdapter mAdapter;
    private boolean mIsInit = false;
    private View mView;
    private FirebaseAnalytics mFirebaseAnalytics;



    public static BaseTabHomeFragment getInstance(int index) {
        BaseTabHomeFragment sInstance = new BaseTabHomeFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("Index", index);
        sInstance.setArguments(arguments);
        return sInstance;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mCurTab = getArguments().getInt("Index", 998);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (!mIsInit) {
            mIsInit = true;
        } else {
            return mView;
        }
        mBaseTabBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_base_home, container, false);
        mView = mBaseTabBinding.getRoot();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        initView();
        initData();

        return mView;
    }

    private void initView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mBaseTabBinding.recyclerView.setLayoutManager(layoutManager);

        String eventName = "FIR_DISPLAY_SCREEN_PAIR_LIST";
        String eventContent = "display_pair_list_screen";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);

    }

    private void initData() {
        if (mPresenter == null) {
            if (mModel == null) {
                mModel = new TabHomeModel(getActivity());
            }
            mPresenter = new TabHomePresenter(getActivity(), mModel, this);
            mPresenter.loadData(mCurTab);
        }

    }

    public void reloadData(int curTab) {
        if (mPresenter != null)
            mPresenter.loadData(curTab);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void loadDataSuccess(List<InfoResponse> infoResponseList) {
        if (mAdapter == null) {
            mAdapter = new BaseTabHomeAdapter(infoResponseList, getActivity(), mCurTab);
            mAdapter.setItemClickListener(this);
            mBaseTabBinding.recyclerView.setAdapter(mAdapter);

        } else {
            mAdapter.dataSetChangeList(infoResponseList);
        }

    }



    @Override
    public void onClickItem(int position, InfoResponse infoResponse) {
        if (infoResponse != null) {
            String title = infoResponse.getInfo().getOriginTitle();
            redirectInfoScreens(title);
        }

    }

    private void redirectInfoScreens(String titlePair) {
        if (getActivity() == null) {
            return;
        }
        Intent intent = new Intent(getActivity(), ActivityInfoScreens.class);
        intent.putExtra(Constant.INTENT_EXTRAS_PAIR_NAME, titlePair);
        getActivity().startActivity(intent);

    }

    @Override
    public void onReloadDataPage(int curTab) {
        if ((getActivity()) != null)
            ((MainActivity) getActivity()).reLoadDataPage(curTab);
    }


}
