package com.greenolio.app.wex.fragment;


import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.databinding.FragmentBuyBinding;
import com.greenolio.app.wex.tradescreens.ActivityTradeScreens;
import com.greenolio.app.wex.tradescreens.TabBuyModel;
import com.greenolio.app.wex.tradescreens.TabBuyPresenter;
import com.greenolio.app.wex.tradescreens.TabBuyView;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class TabBuyFragment extends BaseFrag implements TabBuyView, View.OnClickListener {
    private FragmentBuyBinding mTabChartBinding;
    private TabBuyPresenter mPresenter;
    private TabBuyModel mModel;
    private DepthResponse mDepthResponse;
    private boolean mIsInit = false;
    private View mView;
    private String mBalance = "0";


    public static TabBuyFragment getInstance(int index) {
        TabBuyFragment sInstance = new TabBuyFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("Index", index);
        sInstance.setArguments(arguments);
        return sInstance;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (!mIsInit) {
            mIsInit = true;
        } else {
            return mView;
        }
        mTabChartBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_buy, container, false);
        mView = mTabChartBinding.getRoot();
        initView();
        initData();

        return mView;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {

        mTabChartBinding.edtPricePer.addTextChangedListener(generalTextWatcher);
        mTabChartBinding.edtAmout.addTextChangedListener(generalTextWatcher);

        mTabChartBinding.imgClose.setOnClickListener(this);
        mTabChartBinding.imgClose02.setOnClickListener(this);
        mTabChartBinding.btnBuy.setOnClickListener(this);
        mTabChartBinding.txtBalance.setOnClickListener(this);

        mTabChartBinding.edtAmout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View editText, boolean hasFocus) {
                if (hasFocus) {
                    mTabChartBinding.edtAmout.setSelection(((EditText) editText).getText().length());
                    mTabChartBinding.imgClose02.setVisibility(View.GONE);
                    mTabChartBinding.imgClose.setVisibility(View.VISIBLE);
                }
            }
        });

        mTabChartBinding.edtPricePer.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View editText, boolean hasFocus) {
                if (hasFocus) {
                    mTabChartBinding.edtPricePer.setSelection(((EditText) editText).getText().length());
                    mTabChartBinding.imgClose02.setVisibility(View.VISIBLE);
                    mTabChartBinding.imgClose.setVisibility(View.GONE);
                }
            }
        });

        mTabChartBinding.layoutTab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return true;
            }
        });

    }

    private void hideKeyboard(){
        try {
            InputMethodManager imm = (InputMethodManager) getBaseActivity().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getBaseActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void initData() {
        if (mPresenter == null) {
            if (mModel == null) {
                mModel = new TabBuyModel(getActivity());
            }
            mPresenter = new TabBuyPresenter(getBaseActivity(), mModel, this);
            if (((ActivityTradeScreens) getActivity()) == null) {
                mPresenter.loadDataBuy("");
                return;
            }
            String title = ((ActivityTradeScreens) getActivity()).getCurTitle();
            mPresenter.loadDataBuy(title);

        }
    }

    public void loadDataDepth(DepthResponse depthResponse) {
        mDepthResponse = depthResponse;
    }

    private void loadDataExisted(DepthResponse depthResponse, InfoResponse mInfoResponse) {
        if (depthResponse == null && mInfoResponse != null) {
            InfoResponse.Ticker ticker = mInfoResponse.getTicker();
            if (ticker != null) {
                mTabChartBinding.edtPricePer.setText(ticker.getBuy());
            }
        } else if (depthResponse != null && depthResponse.getBids() != null) {
            String amount = depthResponse.getAsk().getAskAmount();
            String price = depthResponse.getAsk().getAskPrice();
            mTabChartBinding.edtAmout.setText(amount);
            mTabChartBinding.edtPricePer.setText(price);
            mPresenter.loadDataAmoutSelected(depthResponse.getPosItemClick());

        }
    }

    public void updateBalanceBuy(String value) {
        mBalance = value;
        String result = value + " " + secondTitle;
        mTabChartBinding.txtBalance.setText(result);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void loadInfoResponse(InfoResponse infoResponse, DepthResponse depthResponse, String[] arrTitle) {
        setDataView(arrTitle);
        initTitleView();
        InfoResponse.Info info = infoResponse.getInfo();
        if (info != null) {
            fee = info.getFee();
        }

        mTabChartBinding.txtTotal.setText("0 " + secondTitle);
        mTabChartBinding.txtFee.setText(0 + " " + firstTitle);
        if (depthResponse != null) {
            String price = depthResponse.getAsk().getAskPrice() + " " + secondTitle;
            mTabChartBinding.txtLowest.setText(price);
        }

        loadDataExisted(mDepthResponse, infoResponse);


    }

    @Override
    public void setDataChange(String total, String feeAmount) {
        mTabChartBinding.txtTotal.setText(total + " " + secondTitle);
        mTabChartBinding.txtFee.setText(feeAmount + " " + firstTitle);
    }

    @Override
    public void requestRedirectComplete() {
        String amount = mTabChartBinding.edtAmout.getText().toString() + " " + firstTitle;
        String price = mTabChartBinding.edtPricePer.getText().toString() + " " + secondTitle;
        String total = mTabChartBinding.txtTotal.getText().toString();
        mPresenter.redirectComplete(amount, price, total);
    }

    @Override
    public void loadTotalAmoutSelected(String totalAmout) {
        mTabChartBinding.edtAmout.setText(totalAmout);
    }

    @Override
    public void setDataAmount(String amount) {
        mTabChartBinding.edtAmout.setText(amount);
    }

    private String firstTitle = "";
    private String secondTitle = "";
    private String fee = "0";

    private void setDataView(String[] arrTitle) {
        if (arrTitle != null && arrTitle.length > 0) {
            firstTitle = arrTitle[0].toUpperCase();
            secondTitle = arrTitle[1].toUpperCase();
        }
    }

    private void initTitleView() {
        mTabChartBinding.txtTitleAmount.setText(getResources().getString(R.string.txt_buy_sell_amount) + " " + firstTitle);
        mTabChartBinding.txtTitlePrice.setText(getResources().getString(R.string.txt_buy_sell_price_per) + " " + firstTitle);
        mTabChartBinding.txtTitle.setText(secondTitle);
        mTabChartBinding.txtBalance.setText(mBalance + " " + secondTitle);

    }

    private final TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            mPresenter.onChangeAmount(fee, mTabChartBinding);

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {


        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_close:
                mTabChartBinding.edtAmout.setText("");
                break;

            case R.id.img_close_02:
                mTabChartBinding.edtPricePer.setText("");
                break;

            case R.id.btn_buy:
                String tradePrice = mTabChartBinding.edtPricePer.getText().toString().trim();
                String tradeAmount = mTabChartBinding.edtAmout.getText().toString().trim();
                if (((ActivityTradeScreens) getActivity()) == null) {
                    mPresenter.doBuyData(tradePrice, tradeAmount, "");
                    break;
                }
                String titlePair = ((ActivityTradeScreens) getActivity()).getCurTitle();
                mPresenter.doBuyData(tradePrice, tradeAmount, titlePair);
                break;
            case R.id.txt_balance:
                String price = mTabChartBinding.edtPricePer.getText().toString().trim();
                mPresenter.calculateClickBalance(mBalance, price);

                break;
        }
    }
}
