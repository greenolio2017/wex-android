package com.greenolio.app.wex.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.databinding.FragmentChartBinding;
import com.greenolio.app.wex.infoscreens.ActivityInfoScreens;
import com.greenolio.app.wex.infoscreens.TabChartPresenter;
import com.greenolio.app.wex.infoscreens.TabChartView;


public class TabChartFragment extends BaseFrag implements TabChartView {
    private FragmentChartBinding mTabChartBinding;
    private TabChartPresenter mPresenter;
    private boolean mIsInit = false;
    private View mView;


    public static TabChartFragment getInstance(int index) {
        TabChartFragment sInstance = new TabChartFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("Index", index);
        sInstance.setArguments(arguments);
        return sInstance;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (!mIsInit) {
            mIsInit = true;
        } else {
            return mView;
        }
        mTabChartBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chart, container, false);
        mView = mTabChartBinding.getRoot();
        initView();
        initData();

        return mView;
    }

    private void initView() {
        mTabChartBinding.webView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mTabChartBinding.webView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width = mTabChartBinding.webView.getWidth(); //width is ready
                int height = mTabChartBinding.webView.getHeight(); //height is ready
                if ((getActivity()) == null) {
                    mPresenter.loadDataWebView(mTabChartBinding.webView, "", width, height);
                    return;
                }
                InfoResponse infoResponse = ((ActivityInfoScreens) getActivity()).getInfoResponse();
                String titleConvert = infoResponse.getInfo().getTitleConvert();
                mPresenter.loadDataWebView(mTabChartBinding.webView, titleConvert, width, height);
            }
        });


    }

    private void initData() {
        if (mPresenter == null) {
            mPresenter = new TabChartPresenter(getActivity());
        }

    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
