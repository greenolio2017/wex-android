package com.greenolio.app.wex.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.adapter.InfoOrdersAdapter;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.databinding.FragmentOrdersBinding;
import com.greenolio.app.wex.infoscreens.ActivityInfoScreens;
import com.greenolio.app.wex.infoscreens.TabOrdersModel;
import com.greenolio.app.wex.infoscreens.TabOrdersPresenter;
import com.greenolio.app.wex.infoscreens.TabOrdersView;
import java.util.List;


public class TabOrdersFragment extends BaseFrag implements TabOrdersView, InfoOrdersAdapter.ItemClickListener {
    private FragmentOrdersBinding mTabOrdersBinding;
    private TabOrdersPresenter mPresenter;
    private TabOrdersModel mModel;
    private boolean mIsInit = false;
    private View mView;
    private InfoOrdersAdapter mAdapter;


    public static TabOrdersFragment getInstance(int index) {
        TabOrdersFragment sInstance = new TabOrdersFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("Index", index);
        sInstance.setArguments(arguments);
        return sInstance;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (!mIsInit) {
            mIsInit = true;
        } else {
            return mView;
        }
        mTabOrdersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_orders, container, false);
        mView = mTabOrdersBinding.getRoot();
        initView();
        initData();

        return mView;
    }

    private void initView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mTabOrdersBinding.recyclerView.setLayoutManager(layoutManager);

    }

    private void initData() {
        if (mPresenter == null) {
            if (mModel == null) {
                mModel = new TabOrdersModel();
            }
            mPresenter = new TabOrdersPresenter(getActivity(), mModel, this);
            if (((ActivityInfoScreens) getActivity()) == null) {
                mPresenter.initData("");
                return;
            }
            InfoResponse infoResponse = ((ActivityInfoScreens) getActivity()).getInfoResponse();
            String title = infoResponse.getInfo().getOriginTitle();
            mPresenter.initData(title);
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

    }


    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }


    @Override
    public void loadDataSuccess(List<DepthResponse> depthResponseList) {
        if (depthResponseList != null) {
            if (mAdapter == null) {
                mAdapter = new InfoOrdersAdapter(depthResponseList);
                mAdapter.setItemClickListener(this);
                mTabOrdersBinding.recyclerView.setAdapter(mAdapter);
            } else {
                mAdapter.dataSetChangeList(depthResponseList);
            }
        }
    }


    @Override
    public void onClickItem(int position, DepthResponse depthResponse, int orderType) {
        mPresenter.redirectTradeScreen(orderType, depthResponse);
    }

}
