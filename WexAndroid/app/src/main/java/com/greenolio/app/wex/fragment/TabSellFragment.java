package com.greenolio.app.wex.fragment;


import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.databinding.FragmentSellBinding;
import com.greenolio.app.wex.tradescreens.ActivityTradeScreens;
import com.greenolio.app.wex.tradescreens.TabSellModel;
import com.greenolio.app.wex.tradescreens.TabSellPresenter;
import com.greenolio.app.wex.tradescreens.TabSellView;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class TabSellFragment extends BaseFrag implements TabSellView, View.OnClickListener {
    private FragmentSellBinding mTabSellBinding;
    private TabSellPresenter mPresenter;
    private TabSellModel mModel;
    private DepthResponse mDepthResponse;
    private boolean mIsInit = false;
    private View mView;
    private String mBalance = "0";


    public static TabSellFragment getInstance(int index) {
        TabSellFragment sInstance = new TabSellFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("Index", index);
        sInstance.setArguments(arguments);
        return sInstance;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (!mIsInit) {
            mIsInit = true;
        } else {
            return mView;
        }
        mTabSellBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sell, container, false);
        mView = mTabSellBinding.getRoot();
        initView();
        initData();

        return mView;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        mTabSellBinding.edtAmout.addTextChangedListener(generalTextWatcher);
        mTabSellBinding.edtPricePer.addTextChangedListener(generalTextWatcher);

        mTabSellBinding.imgClose.setOnClickListener(this);
        mTabSellBinding.imgClose02.setOnClickListener(this);
        mTabSellBinding.btnSell.setOnClickListener(this);
        mTabSellBinding.txtBalance.setOnClickListener(this);

        mTabSellBinding.edtAmout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View editText, boolean hasFocus) {
                if (hasFocus) {
                    mTabSellBinding.edtAmout.setSelection(((EditText) editText).getText().length());
                    mTabSellBinding.imgClose02.setVisibility(View.GONE);
                    mTabSellBinding.imgClose.setVisibility(View.VISIBLE);
                }
            }
        });

        mTabSellBinding.edtPricePer.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View editText, boolean hasFocus) {
                if (hasFocus) {
                    mTabSellBinding.edtPricePer.setSelection(((EditText) editText).getText().length());
                    mTabSellBinding.imgClose02.setVisibility(View.VISIBLE);
                    mTabSellBinding.imgClose.setVisibility(View.GONE);
                }
            }
        });

        mTabSellBinding.layoutTab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return true;
            }
        });

    }

    private void initData() {
        if (mPresenter == null) {
            if (mModel == null) {
                mModel = new TabSellModel(getActivity());
            }
            mPresenter = new TabSellPresenter(getBaseActivity(), mModel, this);
            if (((ActivityTradeScreens) getActivity()) == null ){
                mPresenter.loadDataBuy("");
                return;
            }
            String title = ((ActivityTradeScreens) getActivity()).getCurTitle();
            mPresenter.loadDataBuy(title);
        }
    }

    private void hideKeyboard(){
        try {
            InputMethodManager imm = (InputMethodManager) getBaseActivity().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getBaseActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updateBalanceSell(String value) {
        mBalance = value;
        String result = value + " " + firstTitle;
        mTabSellBinding.txtBalance.setText(result);
    }

    public void loadDataDepth(DepthResponse depthResponse) {
        this.mDepthResponse = depthResponse;

    }

    private void loadDataExisted(DepthResponse depthResponse, InfoResponse mInfoResponse) {
        if (depthResponse == null && mInfoResponse != null) {
            InfoResponse.Ticker ticker = mInfoResponse.getTicker();
            if (ticker != null) {
                mTabSellBinding.edtPricePer.setText(ticker.getSell());
            }
        } else if (depthResponse != null && depthResponse.getBids() != null) {
            String price = depthResponse.getBids().getBidPrice();
            mTabSellBinding.edtPricePer.setText(price);
            mPresenter.loadDataAmoutSelected(depthResponse.getPosItemClick());

        }
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void loadInfoResponse(InfoResponse infoResponse, DepthResponse depthResponse, String[] arrTitle) {
        setDataView(arrTitle);
        initTitleView();
        InfoResponse.Info info = infoResponse.getInfo();
        if (info != null) {
            fee = info.getFee();
        }

        mTabSellBinding.txtTotal.setText("0 " + secondTitle);
        mTabSellBinding.txtFee.setText(0 + " " + secondTitle);

        if (depthResponse != null) {
            String price = depthResponse.getBids().getBidPrice();
            if (price == null || price.isEmpty())
                price = "0";
            mTabSellBinding.txtHighestBid.setText(price + " " + secondTitle);
        }
        loadDataExisted(mDepthResponse, infoResponse);

    }

    @Override
    public void setDataChange(String total, String feeAmount) {
        mTabSellBinding.txtTotal.setText(total + " " + secondTitle);
        mTabSellBinding.txtFee.setText(feeAmount + " " + secondTitle);
    }

    @Override
    public void requestRedirectComplete() {
        String amount = mTabSellBinding.edtAmout.getText().toString() + " " + firstTitle;
        String price = mTabSellBinding.edtPricePer.getText().toString() + " " + secondTitle;
        String total = mTabSellBinding.txtTotal.getText().toString();
        mPresenter.redirectComplete(amount, price, total);
    }

    @Override
    public void loadTotalAmoutSelected(String totalAmout) {
        mTabSellBinding.edtAmout.setText(totalAmout);
    }

    @Override
    public void setDataAmount(String amount) {
        mTabSellBinding.edtAmout.setText(amount);
    }

    private String firstTitle = "";
    private String secondTitle = "";
    private String fee = "0";

    private void setDataView(String[] arrTitle) {
        if (arrTitle != null && arrTitle.length > 0) {
            firstTitle = arrTitle[0].toUpperCase();
            secondTitle = arrTitle[1].toUpperCase();
        }
    }

    private void initTitleView() {
        mTabSellBinding.txtTitleAmount.setText(getResources().getString(R.string.txt_buy_sell_amount) + " " + firstTitle);
        mTabSellBinding.txtPricePer.setText(getResources().getString(R.string.txt_buy_sell_price_per) + " " + firstTitle);
        mTabSellBinding.txtTitle.setText(secondTitle);
        mTabSellBinding.txtBalance.setText(mBalance + " " + firstTitle);

    }

    private final TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            mPresenter.onChangeAmount(fee, mTabSellBinding);

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {


        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_close:
                mTabSellBinding.edtAmout.setText("");
                break;

            case R.id.img_close_02:
                mTabSellBinding.edtPricePer.setText("");
                break;

            case R.id.btn_sell:
                String tradePrice = mTabSellBinding.edtPricePer.getText().toString().trim();
                String tradeAmount = mTabSellBinding.edtAmout.getText().toString().trim();
                if (((ActivityTradeScreens) getActivity()) == null) {
                    mPresenter.doSellData(tradePrice, tradeAmount, "");
                    break;
                }
                String titlePair = ((ActivityTradeScreens) getActivity()).getCurTitle();
                mPresenter.doSellData(tradePrice, tradeAmount, titlePair);
                break;

            case R.id.txt_balance:
                mPresenter.calculateClickBalance(mBalance);

                break;
        }
    }
}
