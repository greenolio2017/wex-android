package com.greenolio.app.wex.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.adapter.InfoTradesAdapter;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.api.response.TradesResponse;
import com.greenolio.app.wex.databinding.FragmentTradesBinding;
import com.greenolio.app.wex.infoscreens.ActivityInfoScreens;
import com.greenolio.app.wex.infoscreens.TabTradesModel;
import com.greenolio.app.wex.infoscreens.TabTradesPresenter;
import com.greenolio.app.wex.infoscreens.TabTradesView;

import java.util.List;


public class TabTradesFragment extends BaseFrag implements TabTradesView, InfoTradesAdapter.ItemClickListener {
    private FragmentTradesBinding mTabTradesBinding;
    private TabTradesPresenter mPresenter;
    private TabTradesModel mModel;
    private boolean mIsInit = false;
    private View mView;
    private InfoTradesAdapter mAdapter;


    public static TabTradesFragment getInstance(int index) {
        TabTradesFragment sInstance = new TabTradesFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("Index", index);
        sInstance.setArguments(arguments);
        return sInstance;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (!mIsInit) {
            mIsInit = true;
        } else {
            return mView;
        }
        mTabTradesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trades, container, false);
        mView = mTabTradesBinding.getRoot();
        initView();
        initData();

        return mView;
    }

    private void initView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mTabTradesBinding.recyclerView.setLayoutManager(layoutManager);

    }

    private void initData() {
        if (mPresenter == null) {
            if (mModel == null) {
                mModel = new TabTradesModel();
            }
            mPresenter = new TabTradesPresenter(mModel, this);
            if (((ActivityInfoScreens) getActivity()) == null){
                mPresenter.initData("");
                return;
            }
            InfoResponse infoResponse = ((ActivityInfoScreens) getActivity()).getInfoResponse();
            String title = infoResponse.getInfo().getOriginTitle();
            mPresenter.initData(title);
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }


    @Override
    public void onLoadSuccess(List<TradesResponse> tradesResponses) {
        if (tradesResponses != null) {
            if (mAdapter == null) {
                mAdapter = new InfoTradesAdapter(tradesResponses, getActivity());
                mAdapter.setItemClickListener(this);
                mTabTradesBinding.recyclerView.setAdapter(mAdapter);
            } else {
                mAdapter.dataSetChangeList(tradesResponses);
            }
        }
    }

    @Override
    public void onClickItem(int position, TradesResponse TradesResponse) {

    }

}
