package com.greenolio.app.wex.helper;

import android.content.Context;
import android.graphics.Typeface;


public class FontHelper {

    private static final String fontRoboto = "fonts/Roboto-Regular.ttf";
    private static final String fontRobotoBold = "fonts/Roboto-Bold.ttf";
    private static final String fontRobotoCondensed = "fonts/RobotoCondensed-Regular.ttf";
    private static final String fontRobotoCondensedBold = "fonts/RobotoCondensed-Bold.ttf";


    public static Typeface robotoRegular(Context context) {
        Typeface tf ;
        try {
            tf = Typeface.createFromAsset(context.getAssets(), fontRoboto);
        }catch (Exception e){
            return null;
        }
        return tf;
    }

    public static Typeface robotoBold(Context context) {
        Typeface tf ;
        try {
            tf = Typeface.createFromAsset(context.getAssets(), fontRobotoBold);
        }catch (Exception e){
            return null;
        }
        return tf;
    }

    public static Typeface robotoCondensedRegular(Context context) {
        Typeface tf ;
        try {
            tf = Typeface.createFromAsset(context.getAssets(), fontRobotoCondensed);
        }catch (Exception e){
            return null;
        }
        return tf;
    }

    public static Typeface robotoCondensedBold(Context context) {
        Typeface tf ;
        try {
            tf = Typeface.createFromAsset(context.getAssets(), fontRobotoCondensedBold);
        }catch (Exception e){
            return null;
        }
        return tf;
    }
}
