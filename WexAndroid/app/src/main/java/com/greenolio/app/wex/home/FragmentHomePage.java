package com.greenolio.app.wex.home;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.fragment.BaseTabHomeFragment;
import com.greenolio.app.wex.databinding.FragmentHomeBinding;

import java.util.ArrayList;
import java.util.List;


public class FragmentHomePage extends Fragment implements View.OnClickListener {
    private FragmentHomeBinding mBindingHome;
    private ViewPagerAdapter mAdapter;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBindingHome = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        View view = mBindingHome.getRoot();
        initView();
        return view;
    }

    private void initView() {

        setupViewPager(mBindingHome.viewPager);
        TabLayout tabLayout = mBindingHome.tabs;
        tabLayout.setupWithViewPager(mBindingHome.viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        Fragment mFVRT = BaseTabHomeFragment.getInstance(Constant.TAG_995);
        Fragment mBTC = BaseTabHomeFragment.getInstance(Constant.TAG_996);
        Fragment mETH = BaseTabHomeFragment.getInstance(Constant.TAG_997);
        Fragment mUSD = BaseTabHomeFragment.getInstance(Constant.TAG_998);
        Fragment mAll = BaseTabHomeFragment.getInstance(Constant.TAG_999);

        mAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        mAdapter.addFragment(mFVRT, "Fav");
        mAdapter.addFragment(mBTC, "BTC");
        mAdapter.addFragment(mETH, "ETH");
        mAdapter.addFragment(mUSD, "USD");
        mAdapter.addFragment(mAll, "ALL");
        viewPager.setAdapter(mAdapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }



        public void reloadDataHomePage(int curPage) {
            switch (curPage) {
                case Constant.TAG_995:
                    BaseTabHomeFragment baseTabHomeFragment = (BaseTabHomeFragment) mFragmentList.get(0);
                    if (baseTabHomeFragment != null)
                        baseTabHomeFragment.reloadData(curPage);
                    break;
                case Constant.TAG_ALL_HOMEPAGE:
                    BaseTabHomeFragment frFavourite = (BaseTabHomeFragment) mFragmentList.get(0);
                    BaseTabHomeFragment frBTC = (BaseTabHomeFragment) mFragmentList.get(1);
                    BaseTabHomeFragment frETH = (BaseTabHomeFragment) mFragmentList.get(2);
                    BaseTabHomeFragment frUSD = (BaseTabHomeFragment) mFragmentList.get(3);
                    BaseTabHomeFragment frALL = (BaseTabHomeFragment) mFragmentList.get(4);
                    if (frFavourite != null)
                        frFavourite.reloadData(Constant.TAG_995);

                    if (frBTC != null)
                        frBTC.reloadData(Constant.TAG_996);

                    if (frETH != null)
                        frETH.reloadData(Constant.TAG_997);

                    if (frUSD != null)
                        frUSD.reloadData(Constant.TAG_998);

                    if (frALL != null)
                        frALL.reloadData(Constant.TAG_999);
                    break;
                case Constant.TAG_996:
                    BaseTabHomeFragment frBTC996 = (BaseTabHomeFragment) mFragmentList.get(1);
                    if (frBTC996 != null)
                        frBTC996.reloadData(Constant.TAG_996);
                    break;
                case Constant.TAG_997:
                    BaseTabHomeFragment frETH997 = (BaseTabHomeFragment) mFragmentList.get(2);
                    if (frETH997 != null)
                        frETH997.reloadData(Constant.TAG_997);
                    break;

                case Constant.TAG_998:
                    BaseTabHomeFragment frUSD998 = (BaseTabHomeFragment) mFragmentList.get(3);
                    if (frUSD998 != null)
                        frUSD998.reloadData(Constant.TAG_998);
                    break;

                case Constant.TAG_999:
                    BaseTabHomeFragment frALL999 = (BaseTabHomeFragment) mFragmentList.get(4);
                    if (frALL999 != null)
                        frALL999.reloadData(Constant.TAG_999);
                    break;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onClick(View view) {


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void reloadDataPage(int curPage) {
        if (mAdapter != null) {
            mAdapter.reloadDataHomePage(curPage);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


}
