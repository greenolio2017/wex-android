package com.greenolio.app.wex.infoscreens;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityInfoScreensBinding;
import com.greenolio.app.wex.fragment.TabChartFragment;
import com.greenolio.app.wex.fragment.TabOrdersFragment;
import com.greenolio.app.wex.fragment.TabTradesFragment;
import java.util.ArrayList;
import java.util.List;

public class ActivityInfoScreens extends BaseActivity implements View.OnClickListener, InfoScreensView {
    private static final String TAG = ActivityInfoScreens.class.getSimpleName();
    private InfoResponse mInfoResponse;
    private BroadcastReloadData mBroadcastReceive;
    private InfoScreensPresenter mInfoPresenter;
    private ActivityInfoScreensBinding mInfoScreensBinding;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mInfoScreensBinding = DataBindingUtil.setContentView(this, R.layout.activity_info_screens);
            initView();
            initData();
            registerBroadcastReload();
        }

    }


    private void initView() {
        mInfoScreensBinding.imgBack.setOnClickListener(this);
        mInfoScreensBinding.btnBuySell.setOnClickListener(this);

        setupViewPager(mInfoScreensBinding.viewPager);
        TabLayout tabLayout = mInfoScreensBinding.tabs;
        tabLayout.setupWithViewPager(mInfoScreensBinding.viewPager);


    }

    private void setupViewPager(ViewPager viewPager) {
        TabChartFragment  mChart = TabChartFragment.getInstance(Constant.TAG_101);
        TabOrdersFragment mOrders = TabOrdersFragment.getInstance(Constant.TAG_102);
        TabTradesFragment mTrades = TabTradesFragment.getInstance(Constant.TAG_103);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(mChart, "CHART");
        adapter.addFragment(mOrders, "ORDERS");
        adapter.addFragment(mTrades, "TRADES");
        viewPager.setAdapter(adapter);


    }


    private void initData() {
        if (mInfoPresenter == null) {
            mInfoPresenter = new InfoScreensPresenter(this, this);
        }

        mInfoPresenter.initTopData();

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterBroadcast();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_back) {
            finish();
        } else if (view.getId() == R.id.btn_buy_sell) {
            mInfoPresenter.redirectTradeScreen();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onresult...");

    }


    private void registerBroadcastReload() {
        try {
            IntentFilter filter = new IntentFilter(Constant.ACTION_BROADCAST_RELOAD_INFO);
            mBroadcastReceive = new BroadcastReloadData();
            this.registerReceiver(mBroadcastReceive, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unRegisterBroadcast() {
        try {
            if (mBroadcastReceive != null)
                this.unregisterReceiver(mBroadcastReceive);
            Log.e(TAG, " unregisterReceiver");
        } catch (Exception e) {
            // already unregistered
        }
    }

    @Override
    public void setTopView(String headTitle, String last, String low, String high, String vol01, String vol02) {
        mInfoScreensBinding.txtTitleInfo.setText(headTitle);
        mInfoScreensBinding.txtValueLast.setText(last);
        mInfoScreensBinding.txtValueLow.setText(low);
        mInfoScreensBinding.txtValueHigh.setText(high);
        mInfoScreensBinding.txtVol01.setText(vol01);
        mInfoScreensBinding.txtVol02.setText(vol02);

    }

    @Override
    public void resultInfoResponse(InfoResponse infoResponse) {
        this.mInfoResponse = infoResponse;
    }

    public InfoResponse getInfoResponse() {
        return mInfoResponse;
    }

    private class BroadcastReloadData extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                mInfoPresenter.initTopData();
            }
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void showDialogDisconnect() {
        super.showDialogDisconnect();
        mInfoPresenter.dialogDisconnectInternet();
    }

    @Override
    public void dismissDialogDisconnect() {
        super.dismissDialogDisconnect();
        mInfoPresenter.dismissDialogDisconnect();
    }



}

