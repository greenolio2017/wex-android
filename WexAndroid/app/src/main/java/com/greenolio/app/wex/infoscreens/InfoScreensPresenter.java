package com.greenolio.app.wex.infoscreens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.tradescreens.ActivityTradeScreens;

class InfoScreensPresenter {
    private final AppCompatActivity mContext;
    private final InfoScreensView mView;
    private String mCurTitle;
    private FirebaseAnalytics mFirebaseAnalytics;

    public InfoScreensPresenter(AppCompatActivity mContext, InfoScreensView mView) {
        this.mContext = mContext;
        this.mView = mView;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);

        String eventName = "FIR_DISPLAY_SCREEN_DETAIL_INFO";
        String eventContent = "display_detail_info_screen";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    public void initTopData() {
        if (mContext.getIntent().getExtras() != null)
            mCurTitle = mContext.getIntent().getExtras().getString(Constant.INTENT_EXTRAS_PAIR_NAME, "");
        if (mCurTitle != null && !mCurTitle.isEmpty()) {
            InfoResponse mInfoResponse = DataInfoController.getInstance().getMapInfo().get(mCurTitle);
            String titleConvert = mInfoResponse.getInfo().getTitleConvert();
            String[] arrSymbol = titleConvert.split(" / ");
            String firstSym = arrSymbol[0];
            String secondSym = arrSymbol[1];
            String last = mInfoResponse.getTicker().getLast();
            String low = mInfoResponse.getTicker().getLow() + " " + secondSym;
            String high = mInfoResponse.getTicker().getHigh() + " " + secondSym;
            String vol01 = mInfoResponse.getTicker().getVolCur() + " " + firstSym;
            String vol02 = mInfoResponse.getTicker().getVol() + " " + secondSym;
            mView.setTopView(titleConvert, last, low, high, vol01, vol02);
            mView.resultInfoResponse(mInfoResponse);
        }

    }

    public void redirectTradeScreen() {
        Intent intent = new Intent(mContext, ActivityTradeScreens.class);
        intent.putExtra(Constant.INTENT_EXTRAS_PAIR_NAME, mCurTitle);
        intent.putExtra(Constant.INTENT_EXTRAS_ORDER_TYPE, Constant.ORDER_TYPE_BUY_AND_SELL);
        mContext.startActivity(intent);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_BUY_SELL_BUTTON";
        String eventContent = "touched_buy_sell_button";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    DialogHelper dialogHelper;

    public void dialogDisconnectInternet() {
        dialogHelper = new DialogHelper(mContext);
        dialogHelper.showAlert("INFO",
                mContext.getResources().getString(R.string.txt_content_disconnect_internet), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogHelper.dismiss();
                        checkDismissDialog();
                    }
                });
    }

    public void checkDismissDialog() {
        if (!Utils.isCheckConnection(mContext)) {
            dialogDisconnectInternet();
        }
    }

    public void dismissDialogDisconnect() {
        if (dialogHelper != null)
            dialogHelper.dismiss();
    }
}
