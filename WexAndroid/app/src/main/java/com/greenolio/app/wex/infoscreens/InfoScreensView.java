package com.greenolio.app.wex.infoscreens;

import com.greenolio.app.wex.api.response.InfoResponse;

interface InfoScreensView {
     void setTopView(String headTitle, String last, String low, String high, String vol01, String vol02);
     void resultInfoResponse(InfoResponse infoResponse);

}
