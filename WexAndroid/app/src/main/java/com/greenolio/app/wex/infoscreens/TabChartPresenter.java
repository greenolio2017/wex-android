package com.greenolio.app.wex.infoscreens;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.GeolocationPermissions;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.IOException;
import java.io.InputStream;

public class TabChartPresenter implements TabChartResult {
    private static final String DEFAULT_REPLACE = "'symbol': 'WEX:###'";
    private static final String DEFAULT_REPLACE_HEIGHT = "###height###";
    private static final String DEFAULT_REPLACE_HEIGHT_VIEWPORT = "###HEIGHT_VIEWPORT###";
    private final Context mContext;

    public TabChartPresenter(Context mContext) {
        this.mContext = mContext;

    }

    public void loadDataWebView(WebView webView, String titleConvert, int width, int height) {
        try {

            float heightHtml = 500;
            float heightViewPort = 500;
            if (height > 0 && width > 0) {
                heightHtml = (510 * ((float) height / width));
                heightViewPort = heightHtml + 50;
            }

            titleConvert = titleConvert.replace(" / ", "");
            WebSettings webSetting = webView.getSettings();
//            webSetting.setBuiltInZoomControls(true);
//            webSetting.setSupportZoom(true);
            webSetting.setUseWideViewPort(true);
            webSetting.setJavaScriptEnabled(true);
//            webSetting.setJavaScriptCanOpenWindowsAutomatically(true);

            webView.setWebViewClient(new CustomWebclient());
            //webView.loadUrl("file:///android_asset/graph_new.html");

            InputStream is;
            try {
                is = mContext.getAssets().open("graph_new.html");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                String htmlText = new String(buffer);
                String replace = "'symbol': 'WEX:" + titleConvert + "'";
                String outPut = htmlText.replace(DEFAULT_REPLACE, replace)
                        .replace(DEFAULT_REPLACE_HEIGHT, "" + Math.round(heightHtml))
                        .replace(DEFAULT_REPLACE_HEIGHT_VIEWPORT, "" + Math.round(heightViewPort));
                //String outPut = htmlText.replace(DEFAULT_REPLACE, replace);

                webView.loadData(outPut, "text/html", null);
                //webView.loadDataWithBaseURL("file:///android_asset/", outPut, "text/html", "UTF-8", null);

//                webView.clearCache(true);
//                webView.clearHistory();
//                webView.getSettings().setGeolocationEnabled(true);
//                webView.getSettings().setGeolocationDatabasePath(mContext.getApplicationContext().getFilesDir().getPath());
                // HTML5 API flags
//                webView.getSettings().setAppCacheEnabled(true);
//                webView.getSettings().setDatabaseEnabled(true);
                webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//                webView.getSettings().setBuiltInZoomControls(true);
//                webView.getSettings().setDomStorageEnabled(true);

//                // Cookie設定
//                CookieManager manager = CookieManager.getInstance(); //[General][Deprecated API usage]チェック済
//                // Cookie利用開始
//                manager.setAcceptCookie(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private class CustomWebclient extends WebViewClient {
        /**
         * ローディング処理.
         *
         * @param view view
         * @param url  url
         * @return 処理結果
         * @see WebViewClient#shouldOverrideUrlLoading(WebView,
         * String)
         */
        @Override
        public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
            Log.d("nvTien", "nvTien-Webview shouldOverrideUrlLoading");
            return true;
        }

        @SuppressWarnings("JavaDoc")
        @Override
        public void onPageStarted(final WebView view, final String url, final Bitmap favicon) {
            Log.d("nvTien", "nvTien-Webview onPageStarted");
        }

        @SuppressWarnings("JavaDoc")
        @Override
        public void onPageFinished(final WebView view, final String url) {
            Log.d("nvTien", "nvTien-Webview onPageFinished");
//            view.clearCache(true);
//            view.clearHistory();

        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Log.d("nvTien", "nvTien-Webview onReceivedError");
        }

        @Override
        public void onReceivedSslError(WebView view, @NonNull SslErrorHandler handler, SslError error) {
            Log.d("nvTien", "nvTien-Webview onReceivedSslError");
        }
    }
}




