package com.greenolio.app.wex.infoscreens;


import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.greenolio.app.wex.api.ApiClient;
import com.greenolio.app.wex.api.ApiInterface;
import com.greenolio.app.wex.api.CustomCallBack;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.DepthResponse;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class TabOrdersModel {
    private TabOrdersResult mResult;

    public TabOrdersModel() {

    }

    public void loadDataDepth(final String title) {
        String baseUrl = ApiClient.BASE_URL;
        ApiInterface apiInterface = ApiClient.getClient(baseUrl).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getDataDepth(title);
        call.enqueue(new CustomCallBack<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        JsonObject jsonObject = new JsonParser().parse(response.body().string()).getAsJsonObject();
                        JsonObject jsonResponse = new JsonParser().parse(jsonObject.getAsJsonObject().get(title).toString()).getAsJsonObject();
                        List<DepthResponse> depthResponseList = getDataDepthResponse(jsonResponse);
                        DataInfoController.getInstance().setListCurDepth(depthResponseList);
                        mResult.loadDataSuccess(depthResponseList);

                    } catch (Exception e) {
                        mResult.loadDataSuccess(null);
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                super.onFailure(call, t);
                mResult.loadDataSuccess(null);
            }
        });
    }

    private List<DepthResponse> getDataDepthResponse(JsonObject jsonObject) {
        List<DepthResponse> depthResponseList = new ArrayList<>();
        if (jsonObject != null) {
            JsonArray arrAsks = jsonObject.get("asks").getAsJsonArray();
            JsonArray arrBids = jsonObject.get("bids").getAsJsonArray();
            DepthResponse depthResponse ;
            if (arrAsks != null && arrAsks.size() != 0) {
                for (int i = 0; i < arrAsks.size(); i++) {
                    DepthResponse.Ask ask;
                    if (arrAsks.get(i) != null) {
                        depthResponse = new DepthResponse();
                        ask = depthResponse.new Ask();
                        JsonElement price = arrAsks.get(i).getAsJsonArray().get(0);
                        ask.setAskPrice(price == null ? "" : price.getAsString());
                        JsonElement amount = arrAsks.get(i).getAsJsonArray().get(1);
                        ask.setAskAmount(amount == null ? "" : amount.getAsString());
                        depthResponse.setAsk(ask);
                        depthResponseList.add(depthResponse);
                    }
                }
                for (int i = 0; i < arrBids.size(); i++) {
                    DepthResponse.Bids bids ;
                    if (arrBids.get(i) != null) {
                        depthResponse = new DepthResponse();
                        bids = depthResponse.new Bids();
                        JsonElement price = arrBids.get(i).getAsJsonArray().get(0);
                        bids.setBidPrice(price == null ? "" : price.getAsString());
                        JsonElement amount = arrBids.get(i).getAsJsonArray().get(1);
                        bids.setBidAmount(amount == null ? "" : amount.getAsString());
                        depthResponse.setBids(bids);

                        if (i < depthResponseList.size()) {
                            DepthResponse response = depthResponseList.get(i);
                            response.setBids(bids);
                            depthResponseList.set(i, response);

                        } else {
                            depthResponse.setBids(bids);
                            depthResponseList.add(depthResponse);

                        }
                    }

                }
            }

        }

        return depthResponseList;
    }

    public void setResult(TabOrdersResult result) {
        this.mResult = result;
    }
}
