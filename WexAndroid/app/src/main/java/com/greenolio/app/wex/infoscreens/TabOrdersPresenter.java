package com.greenolio.app.wex.infoscreens;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.tradescreens.ActivityTradeScreens;

import java.util.List;

public class TabOrdersPresenter implements TabOrdersResult {
    private final Context mContext;
    private final TabOrdersModel mModel;
    private final TabOrdersView mView;
    private String mTitle = "";
    private Handler mHandler;
    private boolean mIsRuningCallService = true;
    private FirebaseAnalytics mFirebaseAnalytics;

    public TabOrdersPresenter(Context mContext, TabOrdersModel mModel, TabOrdersView mView) {
        this.mContext = mContext;
        this.mModel = mModel;
        this.mView = mView;
        this.mModel.setResult(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
    }

    public void initData(String title) {
        this.mTitle = title;
        mModel.loadDataDepth(title);
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mHandler.postDelayed(reloadDataOrderRunnable, INTERVAL_RELOAD_DATA_INFO);
    }

    public void onDestroy() {
        if (mHandler != null) {
            mHandler.removeCallbacks(reloadDataOrderRunnable);
            mHandler = null;
        }
    }

    @Override
    public void loadDataSuccess(List<DepthResponse> depthResponseList) {
        if (depthResponseList != null) {
            mView.loadDataSuccess(depthResponseList);
            mIsRuningCallService = false;
        }
    }

    public void redirectTradeScreen(int orderType, DepthResponse depthResponse) {
        DataInfoController.getInstance().setDepthResponse(depthResponse);
        Intent intent = new Intent(mContext, ActivityTradeScreens.class);
        intent.putExtra(Constant.INTENT_EXTRAS_ORDER_TYPE, orderType);
        intent.putExtra(Constant.INTENT_EXTRAS_PAIR_NAME, mTitle);
        mContext.startActivity(intent);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_CELL_ON_ORDER_TABLE";
        String eventContent = "touched_cell_on_order_table";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    private final int INTERVAL_RELOAD_DATA_INFO = 5 * 1000;
    private final Runnable reloadDataOrderRunnable = new Runnable() {
        public void run() {
            // handle count time update first gps
            if (mHandler == null) return;
            if (!mIsRuningCallService && mTitle != null && !mTitle.isEmpty()) {
                mIsRuningCallService = true;
                mModel.loadDataDepth(mTitle);
            }
            mHandler.postDelayed(this, INTERVAL_RELOAD_DATA_INFO);

        }
    };
}
