package com.greenolio.app.wex.infoscreens;

import com.greenolio.app.wex.api.response.DepthResponse;

import java.util.List;

interface TabOrdersResult {
     void loadDataSuccess(List<DepthResponse> depthResponseList);
}
