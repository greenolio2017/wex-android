package com.greenolio.app.wex.infoscreens;

import com.greenolio.app.wex.api.response.DepthResponse;

import java.util.List;

public interface TabOrdersView {
     void loadDataSuccess(List<DepthResponse> depthResponseList);
}
