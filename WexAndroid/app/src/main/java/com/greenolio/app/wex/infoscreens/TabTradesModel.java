package com.greenolio.app.wex.infoscreens;



import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.greenolio.app.wex.api.ApiClient;
import com.greenolio.app.wex.api.ApiInterface;
import com.greenolio.app.wex.api.CustomCallBack;
import com.greenolio.app.wex.api.response.TradesResponse;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class TabTradesModel {

    private TabTradesResult mResult;

    public TabTradesModel() {

    }

    public void loadDataTrades(final String title) {
        String baseUrl = ApiClient.BASE_URL;
        ApiInterface apiInterface = ApiClient.getClient(baseUrl).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getDataTrades(title);
        call.enqueue(new CustomCallBack<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        JsonObject jsonObject = new JsonParser().parse(response.body().string()).getAsJsonObject();
                        JsonArray jsonResponse = jsonObject.getAsJsonObject().get(title).getAsJsonArray();
                        List<TradesResponse> tradesResponses = getDataTradesResponse(jsonResponse);
                        mResult.onLoadSuccess(tradesResponses);

                    } catch (Exception e) {
                        mResult.onLoadSuccess(null);
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                super.onFailure(call, t);
                mResult.onLoadSuccess(null);
            }
        });
    }

    private List<TradesResponse> getDataTradesResponse(JsonArray jsonResponse) {
        List<TradesResponse> tradesResponses = new ArrayList<>();
        if (jsonResponse != null) {
            TradesResponse tradesResponse;
            Gson gson = new Gson();
            if (jsonResponse.size() != 0) {
                for (int i = 0; i < jsonResponse.size(); i++) {
                    if (jsonResponse.get(i) != null) {
                        tradesResponse = gson.fromJson(jsonResponse.get(i).toString(), TradesResponse.class);
                        tradesResponses.add(tradesResponse);
                    }
                }
            }

        }

        return tradesResponses;
    }

    public void setResult(TabTradesResult result) {
        this.mResult = result;
    }
}
