package com.greenolio.app.wex.infoscreens;

import android.os.Handler;
import android.util.Log;

import com.greenolio.app.wex.api.response.TradesResponse;

import java.util.List;

public class TabTradesPresenter implements TabTradesResult {

    private final TabTradesModel mModel;
    private final TabTradesView mView;
    private String mTitle = "";
    private Handler mHandler;
    private boolean mIsRuningCallService = true;

    public TabTradesPresenter(TabTradesModel mModel, TabTradesView mView) {
        this.mModel = mModel;
        this.mView = mView;
        this.mModel.setResult(this);
    }

    public void initData(String title) {
        if (title.isEmpty()) {
            return;
        }
        this.mTitle = title;
        mModel.loadDataTrades(title);
        Log.d("nvTien", "nvTien-Loading...");
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mHandler.postDelayed(reloadDataTradesRunnable, INTERVAL_RELOAD_DATA_INFO);
    }

    public void onDestroy() {
        if (mHandler != null) {
            mHandler.removeCallbacks(reloadDataTradesRunnable);
            mHandler = null;
        }
    }

    @Override
    public void onLoadSuccess(List<TradesResponse> tradesResponses) {
        mView.onLoadSuccess(tradesResponses);
        mIsRuningCallService = false;
    }

    private final int INTERVAL_RELOAD_DATA_INFO = 5 * 1000;
    private final Runnable reloadDataTradesRunnable = new Runnable() {
        public void run() {
            // handle count time update first gps
            if (mHandler == null) return;
            if (!mIsRuningCallService && mTitle != null && !mTitle.isEmpty()) {
                mIsRuningCallService = true;
                mModel.loadDataTrades(mTitle);
            }
            mHandler.postDelayed(this, INTERVAL_RELOAD_DATA_INFO);

        }
    };
}
