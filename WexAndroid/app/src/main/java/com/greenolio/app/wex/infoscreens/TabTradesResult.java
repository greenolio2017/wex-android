package com.greenolio.app.wex.infoscreens;

import com.greenolio.app.wex.api.response.TradesResponse;

import java.util.List;

interface TabTradesResult {
     void onLoadSuccess(List<TradesResponse> tradesResponses);

}
