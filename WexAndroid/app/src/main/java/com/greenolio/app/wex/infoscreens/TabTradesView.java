package com.greenolio.app.wex.infoscreens;

import com.greenolio.app.wex.api.response.TradesResponse;

import java.util.List;

public interface TabTradesView {
     void onLoadSuccess(List<TradesResponse> tradesResponses);
}
