package com.greenolio.app.wex.introduce;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.adapter.IntroduceTabsPagerAdapter;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.PreferencesUtil;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityIntroduceBinding;
import com.greenolio.app.wex.splash.ActivitySplash;
import com.greenolio.app.wex.splash.SplashView;

public class ActivityIntroduce extends AppCompatActivity implements View.OnClickListener, SplashView, ViewPager.OnPageChangeListener {
    private ActivityIntroduceBinding mBindingIntroduce;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayout();

    }

    private void initLayout() {
        boolean isDisplayedIntroduce = PreferencesUtil.getInstance(this).getBoolean(Constant.KEY_PREF_DISPLAY_INTRODUCE, false);
        if (isDisplayedIntroduce) {
            redirectToSplash();
        } else {
            mBindingIntroduce = DataBindingUtil.setContentView(this, R.layout.activity_introduce);
            initViewIntroduce();
            initDataIntroduce();

        }
    }

    private void initViewIntroduce() {
        mBindingIntroduce.btnStart.setOnClickListener(this);
    }

    private void initDataIntroduce() {
        IntroduceTabsPagerAdapter mAdapter = new IntroduceTabsPagerAdapter(this);
        mBindingIntroduce.pagerIntroduce.setAdapter(mAdapter);
        mBindingIntroduce.pagerIntroduce.addOnPageChangeListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
                redirectToSplash();
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                mBindingIntroduce.circleIndicator01.setBackgroundResource(R.drawable.circle_indicator_selected);
                mBindingIntroduce.circleIndicator02.setBackgroundResource(R.drawable.circle_indicator);
                mBindingIntroduce.circleIndicator03.setBackgroundResource(R.drawable.circle_indicator);
                break;
            case 1:
                mBindingIntroduce.circleIndicator01.setBackgroundResource(R.drawable.circle_indicator);
                mBindingIntroduce.circleIndicator02.setBackgroundResource(R.drawable.circle_indicator_selected);
                mBindingIntroduce.circleIndicator03.setBackgroundResource(R.drawable.circle_indicator);
                break;
            case 2:
                mBindingIntroduce.circleIndicator01.setBackgroundResource(R.drawable.circle_indicator);
                mBindingIntroduce.circleIndicator02.setBackgroundResource(R.drawable.circle_indicator);
                mBindingIntroduce.circleIndicator03.setBackgroundResource(R.drawable.circle_indicator_selected);
                break;
//            case 3:
//                redirectToSplash();
//                break;

            default:
                break;
        }
    }

    private void redirectToSplash() {
        PreferencesUtil.getInstance(this).putBoolean(Constant.KEY_PREF_DISPLAY_INTRODUCE, true);
        Intent intent = new Intent(this, ActivitySplash.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
