package com.greenolio.app.wex.keysetting;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.PreferencesUtil;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityKeySettingBinding;

public class ActivityKeySetting extends BaseActivity implements View.OnClickListener {
    private ActivityKeySettingBinding mKeySettingBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mKeySettingBinding = DataBindingUtil.setContentView(this, R.layout.activity_key_setting);
            initView();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        mKeySettingBinding.btnSave.setOnClickListener(this);
        mKeySettingBinding.imgClose.setOnClickListener(this);
        mKeySettingBinding.imgClose02.setOnClickListener(this);
        mKeySettingBinding.txtClickHere.setOnClickListener(this);
        mKeySettingBinding.imgBack.setOnClickListener(this);

        mKeySettingBinding.edtKey.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View editText, boolean hasFocus) {
                if (hasFocus) {
                    mKeySettingBinding.edtKey.setSelection(((EditText) editText).getText().length());
                    mKeySettingBinding.imgClose02.setVisibility(View.GONE);
                    mKeySettingBinding.imgClose.setVisibility(View.VISIBLE);
                }
            }
        });

        mKeySettingBinding.edtSecret.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View editText, boolean hasFocus) {
                if (hasFocus) {
                    mKeySettingBinding.edtSecret.setSelection(((EditText) editText).getText().length());
                    mKeySettingBinding.imgClose02.setVisibility(View.VISIBLE);
                    mKeySettingBinding.imgClose.setVisibility(View.GONE);
                }
            }
        });
        String key = PreferencesUtil.getInstance(this).getString(Constant.KEY_PREF_KEY_SETTING_KEY, "");
        String secret = PreferencesUtil.getInstance(this).getString(Constant.KEY_PREF_KEY_SETTING_SECRET, "");
        mKeySettingBinding.edtKey.setText(key);
        mKeySettingBinding.edtSecret.setText(secret);

        mKeySettingBinding.wrapper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return true;
            }
        });

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_close:
                mKeySettingBinding.edtKey.setText("");
                break;
            case R.id.img_close_02:
                mKeySettingBinding.edtSecret.setText("");
                break;

            case R.id.txt_click_here:
                openWebView();
                break;

            case R.id.btn_save:
                saveKey();
                break;
            case R.id.img_back:
                finish();
                break;
        }

    }

    private void saveKey() {
        String key = PreferencesUtil.getInstance(this).getString(Constant.KEY_PREF_KEY_SETTING_KEY, "");
        String secret = PreferencesUtil.getInstance(this).getString(Constant.KEY_PREF_KEY_SETTING_SECRET, "");

        String dataKey = mKeySettingBinding.edtKey.getText().toString().trim();
        String dataSecret = mKeySettingBinding.edtSecret.getText().toString().trim();

        if (dataKey.isEmpty() || dataSecret.isEmpty()) {
            showDialogInfo();
        } else {
            if (dataKey.equalsIgnoreCase(key) && dataSecret.equalsIgnoreCase(secret)) {
                showDialogInfo();
            } else {
                PreferencesUtil.getInstance(this).putString(Constant.KEY_PREF_KEY_SETTING_KEY, dataKey);
                PreferencesUtil.getInstance(this).putString(Constant.KEY_PREF_KEY_SETTING_SECRET, dataSecret);
                finish();
            }
        }


    }

    private void showDialogInfo() {
        final DialogHelper dialogHelper = new DialogHelper(this);
        dialogHelper.showAlert("INFO", getResources().getString(R.string.txt_key_setting_diaglog_content), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogHelper.dismiss();
                finish();
            }
        });
    }

    private void openWebView() {
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://wex.nz/profile#api_keys"));
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No application can handle this request."
                    + " Please install a webbrowser", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

