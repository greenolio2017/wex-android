package com.greenolio.app.wex.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityMainBinding;
import com.greenolio.app.wex.fragment.BaseTabHomeFragment;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseActivity implements View.OnClickListener, MainView {
    private static final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReloadData mBroadcastReceive;
    private DrawerLayout mDrawerLayout;
    private MainPresenter mMainPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            ActivityMainBinding mMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
            initView();
            invalidateOptionsMenu();
            initData();
            registerBroadcastReload();
        }

    }


    private void initView() {
        mDrawerLayout = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, getToolbar(), R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //hideSoftKeyboard();
            }
        };

        TextView txtPinSetting = findViewById(R.id.txt_pin_setting);
        TextView txtKeySetting = findViewById(R.id.txt_api_key_setting);
        TextView txtActiveOrder = findViewById(R.id.txt_active_order);
        TextView txtHistory = findViewById(R.id.txt_trade_history);
        TextView txtBalances = findViewById(R.id.txt_balances);
        TextView txtAboutUs = findViewById(R.id.txt_about_us);

        txtPinSetting.setOnClickListener(this);
        txtKeySetting.setOnClickListener(this);
        txtActiveOrder.setOnClickListener(this);
        txtHistory.setOnClickListener(this);
        txtBalances.setOnClickListener(this);
        txtAboutUs.setOnClickListener(this);


    }

    public Toolbar getToolbar() {
        Toolbar mToolbar;
        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mToolbar.setTitle("");
        if (mToolbar != null) {
            boolean hadContentDescription = TextUtils
                    .isEmpty(mToolbar.getNavigationContentDescription());
            String contentDescription = String.valueOf(
                    !hadContentDescription ? mToolbar.getNavigationContentDescription() : getString(R.string
                            .nav_main));
            mToolbar.setNavigationContentDescription(contentDescription);
            setSupportActionBar(mToolbar);
            setTitleToolbar(mToolbar);
            mToolbar.setNavigationIcon(R.drawable.ic_nav_menu);
        }
        return mToolbar;
    }

    public void setTitleToolbar(Toolbar toolbar) {
        if (toolbar != null) {
            TextView txtTitleToolbar = (TextView) toolbar.findViewById(R.id.title_toolbar);
            txtTitleToolbar.setText(getResources().getString(R.string.nav_main));
        }
    }

    private void initData() {
        if (mMainPresenter == null) {
            mMainPresenter = new MainPresenter(this);
        }
        //set default display menu
        mMainPresenter.displaySelectedScreen(0, mDrawerLayout);
        mMainPresenter.loadData();


    }

    @Override
    public void onDestroy() {
        if (mMainPresenter != null)
            mMainPresenter.onDestroy();
        super.onDestroy();
        unRegisterBroadcast();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }


    @Override
    public void onClick(View view) {
        mMainPresenter.displaySelectedScreen(view.getId(), mDrawerLayout);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onresult...");
        if (mMainPresenter != null && mMainPresenter.getHomePage() != null) {
            mMainPresenter.getHomePage().onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void reLoadDataPage(int curPage) {
        if (mMainPresenter.getHomePage() != null)
            mMainPresenter.getHomePage().reloadDataPage(curPage);
    }


    private void registerBroadcastReload() {
        try {
            IntentFilter filter = new IntentFilter(Constant.ACTION_BROADCAST_RELOAD_INFO);
            mBroadcastReceive = new BroadcastReloadData();
            this.registerReceiver(mBroadcastReceive, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unRegisterBroadcast() {
        try {
            if (mBroadcastReceive != null)
                this.unregisterReceiver(mBroadcastReceive);
            Log.e(TAG, " unregisterReceiver");
        } catch (Exception e) {
            // already unregistered
        }
    }

    private class BroadcastReloadData extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                int curTab = extras.getInt(Constant.EXTRAS_BROADCAST_RELOAD_TYPE);
                reLoadDataPage(curTab);

            }
        }
    }

    @Override
    public void showDialogDisconnect() {
        super.showDialogDisconnect();
        mMainPresenter.dialogDisconnectInternet();
    }

    @Override
    public void dismissDialogDisconnect() {
        super.dismissDialogDisconnect();
        mMainPresenter.dismissDialogDisconnect();
    }


}
