package com.greenolio.app.wex.main;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.aboutus.ActivityAboutUs;
import com.greenolio.app.wex.activeorders.ActivityActiveOrders;
import com.greenolio.app.wex.balances.ActivityBalances;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.home.FragmentHomePage;
import com.greenolio.app.wex.keysetting.ActivityKeySetting;
import com.greenolio.app.wex.pincode.PinSettingActivity;
import com.greenolio.app.wex.service.ServiceLoadDataInfo;
import com.greenolio.app.wex.splash.SplashPresenter;
import com.greenolio.app.wex.tradehistory.ActivityTradeHistory;


class MainPresenter {
    private Intent mIntentService;
    private final AppCompatActivity mContext;
    private FragmentHomePage mFragmentHome;
    private FirebaseAnalytics mFirebaseAnalytics;


    public MainPresenter(AppCompatActivity mContext) {
        this.mContext = mContext;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);


    }

    public void loadData() {
        onStartServiceInfo();
    }

    private void onStartServiceInfo() {
        mIntentService = new Intent(mContext, ServiceLoadDataInfo.class);
        mContext.startService(mIntentService);

    }

    private void onStopServiceInfo() {
        if (mIntentService != null) {
            mContext.stopService(mIntentService);
        }
    }


    public void onDestroy() {
        onStopServiceInfo();

    }

    public void displaySelectedScreen(int viewID, DrawerLayout drawer) {
//        if (mCurrentID != -1 && viewID == mCurrentID) {
//            drawer.closeDrawer(GravityCompat.START);
//            return;
//        }
        if (viewID == 0) {
            if (mFragmentHome == null) {
                mFragmentHome = new FragmentHomePage();

            }
            FragmentTransaction fragmentTransaction = mContext.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_container, mFragmentHome);
            fragmentTransaction.commit();
            return;
        }

        switch (viewID) {
            case R.id.txt_pin_setting:
                redirectActivityPinSetting();
                break;
            case R.id.txt_api_key_setting:
                redirectActivityKeySetting();
                break;
            case R.id.txt_active_order:
                redirectActivityActiveOrders();
                break;
            case R.id.txt_trade_history:
                redirectActivityTradeHistory();
                break;
            case R.id.txt_balances:
                redirectActivityBalances();
                break;
            case R.id.txt_about_us:
                redirectActivityAboutUs();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);

    }

    private void redirectActivityKeySetting() {
        Intent intent = new Intent(mContext, ActivityKeySetting.class);
        mContext.startActivity(intent);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_API_KEY_SETTING";
        String eventContent = "touched_api_key_setting";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    private void redirectActivityPinSetting() {
        Intent intent = new Intent(mContext, PinSettingActivity.class);
        mContext.startActivity(intent);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_PIN_SETTING";
        String eventContent = "touched_pin_setting";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    private void redirectActivityActiveOrders() {
        Intent intent = new Intent(mContext, ActivityActiveOrders.class);
        mContext.startActivity(intent);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_ACTIVE_ORDER";
        String eventContent = "touched_active_order_menu";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    private void redirectActivityTradeHistory() {
        Intent intent = new Intent(mContext, ActivityTradeHistory.class);
        mContext.startActivity(intent);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_TRADE_HISTORY";
        String eventContent = "touched_trade_history_menu";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    private void redirectActivityBalances() {
        Intent intent = new Intent(mContext, ActivityBalances.class);
        mContext.startActivity(intent);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_BALANCE";
        String eventContent = "touched_balances_menu";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    private void redirectActivityAboutUs() {
        Intent intent = new Intent(mContext, ActivityAboutUs.class);
        mContext.startActivity(intent);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_ABOUT_US";
        String eventContent = "touched_about_us_menu";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    DialogHelper dialogHelper;

    public void dialogDisconnectInternet() {
        if (dialogHelper == null)
            dialogHelper = new DialogHelper(mContext);

        dialogHelper.showAlert("INFO",
                mContext.getResources().getString(R.string.txt_content_disconnect_internet), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogHelper.dismiss();
                        checkDismissDialog();
                    }
                });
    }

    public void checkDismissDialog() {
        if (!Utils.isCheckConnection(mContext)) {
            dialogDisconnectInternet();
        }
    }

    public void dismissDialogDisconnect() {
        if (dialogHelper != null)
            dialogHelper.dismiss();
    }

    public FragmentHomePage getHomePage() {
        return mFragmentHome;
    }


}
