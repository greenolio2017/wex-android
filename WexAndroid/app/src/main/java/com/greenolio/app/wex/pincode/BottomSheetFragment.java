package com.greenolio.app.wex.pincode;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.PreferencesUtil;
import com.greenolio.app.wex.databinding.FragmentBottomSheetDialogBinding;
import com.greenolio.app.wex.R;

public class BottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private FragmentBottomSheetDialogBinding binding;
    private ListenerOptionPin mListener;

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bottom_sheet_dialog, container, false);
        View rootView = binding.getRoot();
        initView();
        initData();
        return rootView;

    }

    private void initView() {
        binding.layout6Digit.setOnClickListener(this);
        binding.layout4Digit.setOnClickListener(this);
        binding.layoutAlphaNumeric.setOnClickListener(this);
        binding.layoutNumericCode.setOnClickListener(this);
        binding.layoutCancel.setOnClickListener(this);
    }

    private void initData() {
        int typeDigit = PreferencesUtil.getInstance(getActivity()).getInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, Constant.TYPE_DEFAULT);
        if (typeDigit == Constant.TYPE_6_DIGIT) {
            binding.layout4Digit.setVisibility(View.VISIBLE);
            binding.layout6Digit.setVisibility(View.GONE);
        } else {
            binding.layout4Digit.setVisibility(View.GONE);
            binding.layout6Digit.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_6_digit:
                PreferencesUtil.getInstance(getActivity()).putInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, Constant.TYPE_6_DIGIT);
                mListener.onClick6Digit();
                this.dismiss();
                break;
            case R.id.layout_4_digit:
                PreferencesUtil.getInstance(getActivity()).putInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, Constant.TYPE_DEFAULT);
                mListener.onClick4Digit();
                this.dismiss();
                break;
            case R.id.layout_numeric_code:
                PreferencesUtil.getInstance(getActivity()).putInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, Constant.TYPE_CUSTOM_NUMERIC_CODE);
                mListener.onClickNumericCode();
                this.dismiss();
                break;
            case R.id.layout_alpha_numeric:
                PreferencesUtil.getInstance(getActivity()).putInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, Constant.TYPE_ALPHA_NUMERIC);
                mListener.onClickAlphaNumeric();
                this.dismiss();
                break;
            case R.id.layout_cancel:
                this.dismiss();
                break;

        }
    }

    public void setListener(ListenerOptionPin listener) {
        this.mListener = listener;
    }

    public interface ListenerOptionPin {
         void onClick6Digit();
         void onClick4Digit();
         void onClickNumericCode();
         void onClickAlphaNumeric();
    }
}
