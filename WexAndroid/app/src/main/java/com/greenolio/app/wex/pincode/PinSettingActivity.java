package com.greenolio.app.wex.pincode;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityPinSettingBinding;

public class PinSettingActivity extends BaseActivity implements View.OnClickListener, PinsettingView {
    private PinsettingPresenter mPresenter;
    private ActivityPinSettingBinding mPincodeBinding;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mPincodeBinding = DataBindingUtil.setContentView(this, R.layout.activity_pin_setting);
            initView();
            initData();
        }
    }


    private void initView() {
        mPincodeBinding.btnCreatePin.setOnClickListener(this);
        mPincodeBinding.btnChangePin.setOnClickListener(this);
        mPincodeBinding.btnResetPin.setOnClickListener(this);
        mPincodeBinding.imgBack.setOnClickListener(this);

    }


    private void initData() {
        if (mPresenter == null) {
            mPresenter = new PinsettingPresenter(this, this);

        }
        mPresenter.initData();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_create_pin:
                mPresenter.onClickCreatePin();
                break;
            case R.id.btn_change_pin:
                mPresenter.onClickChangePin();
                break;
            case R.id.btn_reset_pin:
                mPresenter.onClickResetPin();
                break;
            case R.id.img_back:
                finish();
                break;
        }

    }


    @Override
    public void isExistedPincode(boolean isExisted) {
        if (isExisted) {
            mPincodeBinding.btnCreatePin.setBackgroundResource(R.drawable.bg_create_pin_gray);
            mPincodeBinding.btnCreatePin.setTextColor(getResources().getColor(R.color.gray));
            mPincodeBinding.btnCreatePin.setEnabled(false);
            mPincodeBinding.btnResetPin.setVisibility(View.VISIBLE);
            mPincodeBinding.btnChangePin.setVisibility(View.VISIBLE);
        } else {
            mPincodeBinding.btnCreatePin.setBackgroundResource(R.drawable.bg_default_button);
            mPincodeBinding.btnCreatePin.setTextColor(getResources().getColor(R.color.white));
            mPincodeBinding.btnCreatePin.setEnabled(true);
            mPincodeBinding.btnResetPin.setVisibility(View.GONE);
            mPincodeBinding.btnChangePin.setVisibility(View.GONE);
        }
    }


}
