package com.greenolio.app.wex.pincode;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityPincodeBinding;

public class PincodeActivity extends BaseActivity implements View.OnClickListener, PincodeView {
    private PincodePresenter mPresenter;
    private ActivityPincodeBinding mPincodeBinding;
    private boolean mIsHideBack;
    private boolean mIsFinishBack;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mPincodeBinding = DataBindingUtil.setContentView(this, R.layout.activity_pincode);
            initView();
            initData();

        }

    }


    private void initView() {
        mPincodeBinding.numericKeyboard.btn0.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btn1.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btn2.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btn3.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btn4.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btn5.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btn6.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btn7.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btn8.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btn9.setOnClickListener(this);
        mPincodeBinding.numericKeyboard.btnDel.setOnClickListener(this);
        mPincodeBinding.txtPinOption.setOnClickListener(this);
        mPincodeBinding.imgBack.setOnClickListener(this);
        mPincodeBinding.txtNext.setOnClickListener(this);
        mPincodeBinding.edtCustomInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mPresenter.addTextChangeCustomInput(charSequence.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    private void initData() {
        if (mPresenter == null) {
            mPresenter = new PincodePresenter(this, this);
            mPresenter.initDataTypePin(mPincodeBinding);
            mPresenter.initImageView();
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
       mPresenter.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.onPause();
    }




    @Override
    public void onBackPressed() {
        if (this.mIsHideBack) {
            return;
        }
        mPresenter.onBackPress();

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        return super.dispatchKeyEvent(event);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn0:
                mPresenter.addPin(0);
                break;
            case R.id.btn1:
                mPresenter.addPin(1);
                break;
            case R.id.btn2:
                mPresenter.addPin(2);
                break;
            case R.id.btn3:
                mPresenter.addPin(3);
                break;
            case R.id.btn4:
                mPresenter.addPin(4);
                break;
            case R.id.btn5:
                mPresenter.addPin(5);
                break;
            case R.id.btn6:
                mPresenter.addPin(6);
                break;
            case R.id.btn7:
                mPresenter.addPin(7);
                break;
            case R.id.btn8:
                mPresenter.addPin(8);
                break;
            case R.id.btn9:
                mPresenter.addPin(9);
                break;
            case R.id.btn_del:
                mPresenter.deletePin();
                break;
            case R.id.txt_pin_option:
                mPresenter.onClickOption();
                break;
            case R.id.img_back:
                if (mIsFinishBack) {
                    finish();
                    return;
                }
                mPresenter.onBackPress();
                break;
            case R.id.txt_next:
                mPresenter.validateNext();
                break;
        }

    }


    @Override
    public void setTitlePin(String titlePin, boolean isShowOption) {
        mPincodeBinding.txtTitlePin.setText(titlePin);
        if (isShowOption) {
            mPincodeBinding.txtPinOption.setVisibility(View.VISIBLE);
        } else {
            mPincodeBinding.txtPinOption.setVisibility(View.GONE);
        }
    }

    @Override
    public void setHideButtonBack(boolean isHideBack) {
        this.mIsHideBack = isHideBack;
        mPincodeBinding.imgBack.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setFinishBack(boolean isFinishBack) {
        mIsFinishBack = isFinishBack;
    }

}
