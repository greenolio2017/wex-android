package com.greenolio.app.wex.pincode;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.PreferencesUtil;
import com.greenolio.app.wex.databinding.ActivityPincodeBinding;

import java.util.ArrayList;
import java.util.List;

public class PincodePresenter implements PincodeResult, BottomSheetFragment.ListenerOptionPin {

    private final AppCompatActivity mContext;
    private final PincodeView mView;
    private int mTypeDigitPin = Constant.TYPE_DEFAULT;
    private String mDisplayPin;
    private int mCurView = 0;
    private String mCustomValue;
    private final List<ImageView> mListOrigin = new ArrayList<>();
    private final List<ImageView> mListViewPin = new ArrayList<>();
    private final List<String> mListValue = new ArrayList<>();
    private ActivityPincodeBinding mPincodeBinding;


    public PincodePresenter(AppCompatActivity mContext, PincodeView mView) {
        this.mContext = mContext;
        this.mView = mView;

    }

    public void initDataTypePin(ActivityPincodeBinding pincodeBinding) {
        this.mPincodeBinding = pincodeBinding;
        if (mContext.getIntent().getExtras() != null)
            mDisplayPin = mContext.getIntent().getExtras().getString(Constant.INTENT_EXTRAS_TYPE_DISPLAY_PINCODE, Constant.INTENT_EXTRAS_PINCODE_DEFAULT);
        mTypeDigitPin = PreferencesUtil.getInstance(mContext).getInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, Constant.TYPE_DEFAULT);
        if (mDisplayPin.equalsIgnoreCase(Constant.INTENT_EXTRAS_PINCODE_RESET)) {
            mDisplayPin = Constant.INTENT_EXTRAS_PINCODE_NEW;
        }

        String title;
        if (mDisplayPin.equalsIgnoreCase(Constant.INTENT_EXTRAS_PINCODE_NEW)) {
            title = mContext.getResources().getString(R.string.pincode_text_enter_new_pin);
            mView.setTitlePin(title, true);
        } else if (mDisplayPin.equalsIgnoreCase(Constant.INTENT_EXTRAS_PINCODE_CHANGE)) {
            title = mContext.getResources().getString(R.string.pincode_text_enter_pin);
            mView.setTitlePin(title, false);
        } else {
            title = mContext.getResources().getString(R.string.pincode_text_enter_default_pin);
            mView.setTitlePin(title, false);
            mView.setHideButtonBack(true);
        }
    }


    public void initImageView() {
        mListOrigin.clear();
        mPincodeBinding.layoutPincode.setVisibility(View.VISIBLE);
        mPincodeBinding.layoutPinDefault.setVisibility(View.VISIBLE);
        mPincodeBinding.layoutPinCustom.setVisibility(View.GONE);
        if (mTypeDigitPin == Constant.TYPE_DEFAULT) {
            mPincodeBinding.layoutPin0.setVisibility(View.GONE);
            mPincodeBinding.layoutPin5.setVisibility(View.GONE);
            mPincodeBinding.txtNext.setVisibility(View.GONE);

            mListOrigin.add(mPincodeBinding.pinBox1);
            mListOrigin.add(mPincodeBinding.pinBox2);
            mListOrigin.add(mPincodeBinding.pinBox3);
            mListOrigin.add(mPincodeBinding.pinBox4);
        } else if (mTypeDigitPin == Constant.TYPE_6_DIGIT) {
            mPincodeBinding.layoutPin0.setVisibility(View.VISIBLE);
            mPincodeBinding.layoutPin5.setVisibility(View.VISIBLE);
            mPincodeBinding.txtNext.setVisibility(View.GONE);

            mListOrigin.add(mPincodeBinding.pinBox0);
            mListOrigin.add(mPincodeBinding.pinBox1);
            mListOrigin.add(mPincodeBinding.pinBox2);
            mListOrigin.add(mPincodeBinding.pinBox3);
            mListOrigin.add(mPincodeBinding.pinBox4);
            mListOrigin.add(mPincodeBinding.pinBox5);
        } else if (mTypeDigitPin == Constant.TYPE_CUSTOM_NUMERIC_CODE) {
            mPincodeBinding.layoutPinDefault.setVisibility(View.GONE);
            mPincodeBinding.layoutPinCustom.setVisibility(View.VISIBLE);
            mPincodeBinding.txtNext.setVisibility(View.VISIBLE);
            mPincodeBinding.edtCustomInput.setEnabled(false);

        } else if (mTypeDigitPin == Constant.TYPE_ALPHA_NUMERIC) {
            mPincodeBinding.layoutPinDefault.setVisibility(View.GONE);
            mPincodeBinding.layoutPinCustom.setVisibility(View.VISIBLE);
            mPincodeBinding.txtNext.setVisibility(View.VISIBLE);
            mPincodeBinding.edtCustomInput.setEnabled(true);
            mPincodeBinding.layoutPincode.setVisibility(View.INVISIBLE);
            showKeyboard();
        }

    }

    private void showKeyboard() {
        mPincodeBinding.edtCustomInput.requestFocus();
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }

    public void addPin(int value) {
        switch (value) {
            case 0:
                addPinToList("0", mListOrigin);
                break;
            case 1:
                addPinToList("1", mListOrigin);
                break;
            case 2:
                addPinToList("2", mListOrigin);
                break;
            case 3:
                addPinToList("3", mListOrigin);
                break;
            case 4:
                addPinToList("4", mListOrigin);
                break;
            case 5:
                addPinToList("5", mListOrigin);
                break;
            case 6:
                addPinToList("6", mListOrigin);
                break;
            case 7:
                addPinToList("7", mListOrigin);
                break;
            case 8:
                addPinToList("8", mListOrigin);
                break;
            case 9:
                addPinToList("9", mListOrigin);
                break;
        }
    }

    public void validateNext() {
        if (mTypeDigitPin == Constant.TYPE_CUSTOM_NUMERIC_CODE || mTypeDigitPin == Constant.TYPE_ALPHA_NUMERIC) {
            if (mCustomValue == null || mCustomValue.isEmpty()) {

            } else {
                if (mDisplayPin.equalsIgnoreCase(Constant.INTENT_EXTRAS_PINCODE_NEW)) {
                    if (mNewPin.isEmpty()) {
                        String title = mContext.getResources().getString(R.string.pincode_text_enter_confirm_new_pin);
                        mView.setTitlePin(title, false);
                        mNewPin = mCustomValue;
                        clearPincode();
                        return;
                    }
                    if (mCustomValue.equals(mNewPin)) {
                        finishCustomPin(mCustomValue);
                        mContext.finish();
                    } else {
                        clearPincode();
                    }

                } else if (mDisplayPin.equalsIgnoreCase(Constant.INTENT_EXTRAS_PINCODE_CHANGE)) {
                    String pincode = PreferencesUtil.getInstance(mContext).getString(Constant.KEY_PREF_KEY_SAVE_PINCODE, "");
                    if (mCustomValue.equals(pincode)) {
                        String title = mContext.getResources().getString(R.string.pincode_text_enter_new_pin);
                        mView.setTitlePin(title, true);
                        mDisplayPin = Constant.INTENT_EXTRAS_PINCODE_NEW;
                        mNewPin = "";
                        clearPincode();
                    } else {
                        clearPincode();
                    }

                } else {
                    String pincode = PreferencesUtil.getInstance(mContext).getString(Constant.KEY_PREF_KEY_SAVE_PINCODE, "");
                    if (mCustomValue.equals(pincode)) {
                        mContext.finish();
                    } else {
                        clearPincode();
                    }
                }
            }
        }

    }

    private void finishCustomPin(String value) {
        PreferencesUtil.getInstance(mContext).putString(Constant.KEY_PREF_KEY_SAVE_PINCODE, value);
        PreferencesUtil.getInstance(mContext).putInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, mTypeDigitPin);
        PreferencesUtil.getInstance(mContext).putBoolean(Constant.KEY_PREF_KEY_ACTIVE_PIN, true);
    }

    public void addTextChangeCustomInput(String value) {
        if (mCustomValue != null && value != null && mCustomValue.equals(value)) {
            return;
        }
        if (mTypeDigitPin == Constant.TYPE_CUSTOM_NUMERIC_CODE || mTypeDigitPin == Constant.TYPE_ALPHA_NUMERIC) {
            mCustomValue = value;
            mPincodeBinding.edtCustomInput.setText(mCustomValue);
            mPincodeBinding.edtCustomInput.setSelection(mCustomValue.length());
            return;
        }
    }

    private void addPinToList(String value, List<ImageView> listOrigin) {
        if (mTypeDigitPin == Constant.TYPE_CUSTOM_NUMERIC_CODE || mTypeDigitPin == Constant.TYPE_ALPHA_NUMERIC) {
            mListValue.add(value);
            mCustomValue = getValueText();
            mPincodeBinding.edtCustomInput.setText(mCustomValue);
            return;
        }

        if (mCurView < 0)
            mCurView = 0;

        if (mCurView < listOrigin.size()) {
            ImageView imageView = listOrigin.get(mCurView);
            if (mListViewPin.size() < mTypeDigitPin) {
                mListViewPin.add(imageView);
                mListValue.add(value);
                imageView.setBackgroundResource(R.drawable.icon_circle_gray);
                mCurView++;
                if (mListViewPin.size() == mTypeDigitPin) {
                    //handle full input pincode
                    validatePinFull();

                }
            }
        }
    }

    private String mNewPin = "";

    private void validatePinFull() {
        if (mDisplayPin.equalsIgnoreCase(Constant.INTENT_EXTRAS_PINCODE_NEW)) {
            if (mNewPin.isEmpty()) {
                String title = mContext.getResources().getString(R.string.pincode_text_enter_confirm_new_pin);
                mView.setTitlePin(title, false);
                mNewPin = getInputPin();
                clearPincode();
                return;
            }
            if (isPincodeMatch(mNewPin)) {
                finishPincode(mNewPin);
            } else {
                clearPincode();
            }

        } else if (mDisplayPin.equalsIgnoreCase(Constant.INTENT_EXTRAS_PINCODE_CHANGE)) {
            String pincode = PreferencesUtil.getInstance(mContext).getString(Constant.KEY_PREF_KEY_SAVE_PINCODE, "");
            if (isPincodeMatch(pincode)) {
                String title = mContext.getResources().getString(R.string.pincode_text_enter_new_pin);
                mView.setTitlePin(title, true);
                mDisplayPin = Constant.INTENT_EXTRAS_PINCODE_NEW;
                mNewPin = "";
                clearPincode();
            } else {
                clearPincode();
            }

        } else {
            String pincode = PreferencesUtil.getInstance(mContext).getString(Constant.KEY_PREF_KEY_SAVE_PINCODE, "");
            if (isPincodeMatch(pincode)) {
                mContext.finish();
            } else {
                clearPincode();
            }
        }
    }

    private void clearPincode() {
        mListViewPin.clear();
        mListValue.clear();
        mPincodeBinding.edtCustomInput.setText("");
        mCurView = 0;
        for (int i = 0; i < mListOrigin.size(); i++) {
            ImageView imageView = mListOrigin.get(i);
            imageView.setBackgroundResource(R.drawable.icon_circle_blue);
        }
    }

    private String getInputPin() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < mListValue.size(); i++) {
            if (i == 0) {
                result = new StringBuilder(mListValue.get(i));
            } else {
                result.append("-").append(mListValue.get(i));
            }
        }
        return result.toString();
    }

    private boolean isPincodeMatch(String savePin) {
        if (savePin != null && !savePin.isEmpty()) {
            String inputPin = "";
            for (int i = 0; i < mListValue.size(); i++) {
                if (i == 0) {
                    inputPin = mListValue.get(i);
                } else {
                    inputPin = inputPin + "-" + mListValue.get(i);
                }
            }
            return inputPin.equalsIgnoreCase(savePin);
        }
        return false;
    }

    public void deletePin() {
        if (mTypeDigitPin == Constant.TYPE_CUSTOM_NUMERIC_CODE || mTypeDigitPin == Constant.TYPE_ALPHA_NUMERIC) {
            if (mListValue.size() != 0)
                mListValue.remove(mListValue.size() - 1);

            String value = getValueText();
            mPincodeBinding.edtCustomInput.setText(value);
            return;
        }

        int del = mListViewPin.size() - 1;
        if (mListViewPin.size() != 0) {
            ImageView curView = mListViewPin.get(del);
            curView.setBackgroundResource(R.drawable.icon_circle_blue);
            mListViewPin.remove(del);
            mListValue.remove(del);
            mCurView--;
        }
    }

    private String getValueText() {
        if (mListValue.size() != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (String value : mListValue) {
                stringBuilder.append(value);
            }
            return stringBuilder.toString();
        }
        return "";
    }

    public void onClickOption() {
        BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
        bottomSheetFragment.setCancelable(false);
        bottomSheetFragment.show(mContext.getSupportFragmentManager(), bottomSheetFragment.getTag());
        bottomSheetFragment.setListener(this);
    }

    private void finishPincode(String savePincode) {
        PreferencesUtil.getInstance(mContext).putString(Constant.KEY_PREF_KEY_SAVE_PINCODE, savePincode);
        PreferencesUtil.getInstance(mContext).putBoolean(Constant.KEY_PREF_KEY_ACTIVE_PIN, true);
        if (mTypeDigitPin == Constant.TYPE_6_DIGIT) {
            PreferencesUtil.getInstance(mContext).putInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, Constant.TYPE_6_DIGIT);
        } else {
            PreferencesUtil.getInstance(mContext).putInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, Constant.TYPE_DEFAULT);
        }
        mContext.finish();

    }

    public void onBackPress() {
        if (mNewPin.isEmpty()) {
            mContext.finish();
            redirectActivityPinSetting();
        } else {
            mNewPin = "";
            clearPincode();
            String title = mContext.getResources().getString(R.string.pincode_text_enter_new_pin);
            mView.setTitlePin(title, true);
        }

    }

    private void redirectActivityPinSetting() {
        Intent intent = new Intent(mContext, PinSettingActivity.class);
        mContext.startActivity(intent);
    }

    @Override
    public void onClick6Digit() {
        mTypeDigitPin = Constant.TYPE_6_DIGIT;
        initImageView();
        clearPincode();
    }

    @Override
    public void onClick4Digit() {
        mTypeDigitPin = Constant.TYPE_DEFAULT;
        initImageView();
        clearPincode();
    }

    @Override
    public void onClickNumericCode() {
        mTypeDigitPin = Constant.TYPE_CUSTOM_NUMERIC_CODE;
        initImageView();
        clearPincode();

    }

    @Override
    public void onClickAlphaNumeric() {
        mTypeDigitPin = Constant.TYPE_ALPHA_NUMERIC;
        initImageView();
        clearPincode();
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(mContext.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mContext.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void onDestroy() {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            hideKeyboard();
        }

    }

    public void onPause() {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            hideKeyboard();
        }
    }

    public void onResume() {
        if (mTypeDigitPin == Constant.TYPE_ALPHA_NUMERIC) {
            showKeyboard();
        }
    }


}
