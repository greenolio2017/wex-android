package com.greenolio.app.wex.pincode;

interface PincodeView {
     void setTitlePin(String titlePin, boolean isShowOption);
     void setHideButtonBack(boolean isHideBack);
     void setFinishBack(boolean isFinishBack);
}
