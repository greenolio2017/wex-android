package com.greenolio.app.wex.pincode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.PreferencesUtil;

class PinsettingPresenter {
    private final AppCompatActivity mContext;
    private final PinsettingView mView;

    public PinsettingPresenter(AppCompatActivity mContext, PinsettingView mView) {
        this.mContext = mContext;
        this.mView = mView;
    }

    public void initData() {
        String pinCode = PreferencesUtil.getInstance(mContext).getString(Constant.KEY_PREF_KEY_SAVE_PINCODE, "");
        if (pinCode != null && !pinCode.isEmpty()) {
            mView.isExistedPincode(true);
        } else {
            mView.isExistedPincode(false);
        }
    }

    public void onClickCreatePin() {
        Intent intent = new Intent(mContext, PincodeActivity.class);
        intent.putExtra(Constant.INTENT_EXTRAS_TYPE_DISPLAY_PINCODE, Constant.INTENT_EXTRAS_PINCODE_NEW);
        mContext.startActivity(intent);
        mContext.finish();
    }

    public void onClickChangePin() {
        Intent intent = new Intent(mContext, PincodeActivity.class);
        intent.putExtra(Constant.INTENT_EXTRAS_TYPE_DISPLAY_PINCODE, Constant.INTENT_EXTRAS_PINCODE_CHANGE);
        mContext.startActivity(intent);
        mContext.finish();
    }

    public void onClickResetPin() {
        final DialogHelper dialogHelper = new DialogHelper(mContext);
        dialogHelper.showConfirmResetPin(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferencesUtil.getInstance(mContext).putInt(Constant.KEY_PREF_KEY_TYPE_DIGIT_PINCODE, Constant.TYPE_DEFAULT);
                PreferencesUtil.getInstance(mContext).putString(Constant.KEY_PREF_KEY_SAVE_PINCODE, "");
                PreferencesUtil.getInstance(mContext).putBoolean(Constant.KEY_PREF_KEY_ACTIVE_PIN, false);
                dialogHelper.dismiss();
                mContext.finish();

            }
        });
    }

}
