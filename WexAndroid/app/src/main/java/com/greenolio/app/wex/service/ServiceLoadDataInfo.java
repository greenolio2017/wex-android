package com.greenolio.app.wex.service;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;


public class ServiceLoadDataInfo extends Service {
    private static final String TAG = ServiceLoadDataInfo.class.getName();
    private ServiceLoadDataInfoPresenter mPresenter;
    private ServiceLoadDataInfoModel mModel;

    @Override
    public void onCreate() {
        Log.d(TAG, "Service onCreate");
        initData();
        super.onCreate();
    }

    private void initData() {
        if (mPresenter == null) {
            if (mModel == null) {
                mModel = new ServiceLoadDataInfoModel();
            }
            mPresenter = new ServiceLoadDataInfoPresenter(this, mModel);
        }
        mPresenter.startLoadData();
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        Log.e(TAG, "Service onBind");
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        return super.onUnbind(intent);
    }


}
