package com.greenolio.app.wex.service;


import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.greenolio.app.wex.api.ApiClient;
import com.greenolio.app.wex.api.ApiInterface;
import com.greenolio.app.wex.api.CustomCallBack;
import com.greenolio.app.wex.api.DataInfoController;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

class ServiceLoadDataInfoModel {
    private ServiceLoadDataInfoResult mResult;

    public ServiceLoadDataInfoModel() {

    }

    public void reloadDataInfo() {
        String[] originTitle = DataInfoController.getInstance().getArrOriginTitle();
        if (originTitle == null || originTitle.length == 0) {
            mResult.reloadDataInfoSuccess(false);
            return;
        }
        StringBuilder groupTilte = new StringBuilder(DataInfoController.getInstance().getGroupTitle());
        if (groupTilte.length() == 0) {
            for (int i = 0; i < originTitle.length; i++) {
                if (i == 0) {
                    groupTilte = new StringBuilder(originTitle[0]);
                } else {
                    groupTilte.append("-").append(originTitle[0]);
                }
            }
        }
        String baseUrl = ApiClient.BASE_URL;
        ApiInterface apiInterface = ApiClient.getClient(baseUrl).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getDataTicker(groupTilte.toString());
        call.enqueue(new CustomCallBack<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        JsonObject jsonObject = new JsonParser().parse(response.body().string()).getAsJsonObject();
                        DataInfoController.getInstance().parserDataTicker(jsonObject);

                    } catch (Exception e) {
                        e.printStackTrace();
                        mResult.reloadDataInfoSuccess(false);
                        return;
                    }

                }
                mResult.reloadDataInfoSuccess(true);

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                super.onFailure(call, t);
                mResult.reloadDataInfoSuccess(false);
            }
        });
    }

    public void setResult(ServiceLoadDataInfoResult result) {
        this.mResult = result;
    }

}
