package com.greenolio.app.wex.service;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.greenolio.app.wex.common.Constant;


public class ServiceLoadDataInfoPresenter implements ServiceLoadDataInfoResult {
    private static final String TAG = ServiceLoadDataInfoPresenter.class.getSimpleName();
    private final Context mContext;
    private final ServiceLoadDataInfoModel mModel;
    private Handler mHandler;
    private boolean mIsRuningCallService = true;

    public ServiceLoadDataInfoPresenter(Context mContext, ServiceLoadDataInfoModel mModel) {
        this.mContext = mContext;
        this.mModel = mModel;
        this.mModel.setResult(this);
    }

    public void startLoadData() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mHandler.postDelayed(reloadDataInfoRunnable, INTERVAL_RELOAD_DATA_INFO);
        reloadDataInfo();
    }

    private void reloadDataInfo() {
        mModel.reloadDataInfo();
        Log.d(TAG, "reloadDataInfo() ");
    }

    public void onDestroy() {
        if (mHandler != null) {
            mHandler.removeCallbacks(reloadDataInfoRunnable);
            mHandler = null;
            Log.d(TAG, "onDestroy() ");
        }
    }

    private final int INTERVAL_RELOAD_DATA_INFO = 5 * 1000;
    private final Runnable reloadDataInfoRunnable = new Runnable() {
        public void run() {
            // handle count time update first gps
            if (mHandler == null) return;
            if (!mIsRuningCallService) {
                mIsRuningCallService = true;
                reloadDataInfo();
            }
            mHandler.postDelayed(this, INTERVAL_RELOAD_DATA_INFO);

        }
    };

    @Override
    public void reloadDataInfoSuccess(boolean mIsSuccess) {
        mIsRuningCallService = false;
        Log.d(TAG, "reloadDataInfoSuccess: " + mIsSuccess);
        if (mIsSuccess)
            sendBroadcastReloadData();
    }

    private void sendBroadcastReloadData() {
        Intent intent = new Intent(Constant.ACTION_BROADCAST_RELOAD_INFO);
        intent.putExtra(Constant.EXTRAS_BROADCAST_RELOAD_TYPE, Constant.TAG_ALL_HOMEPAGE);
        mContext.sendBroadcast(intent);
        Log.d(TAG, "sendBroadcastReloadData: ");
    }
}
