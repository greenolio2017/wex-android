package com.greenolio.app.wex.service;

interface ServiceLoadDataInfoResult {

     void reloadDataInfoSuccess(boolean mIsSuccess);
}
