package com.greenolio.app.wex.splash;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivitySplashBinding;

public class ActivitySplash extends BaseActivity implements View.OnClickListener, SplashView {
    private SplashPresenter mPresenter;
    private SplashModel mModel;
    private ActivitySplashBinding mBindingSplash;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayout();

    }

    private void initLayout() {
        mBindingSplash = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        initData();

    }


    private void initData() {
        if (mPresenter == null) {
            if (mModel == null) {
                mModel = new SplashModel();
            }
            mPresenter = new SplashPresenter(this, mModel, mBindingSplash);
            mPresenter.loadData();
        }
    }


    @Override
    public void onClick(View view) {


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }

}
