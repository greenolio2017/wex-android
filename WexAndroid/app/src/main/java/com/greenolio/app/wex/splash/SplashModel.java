package com.greenolio.app.wex.splash;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.greenolio.app.wex.api.ApiClient;

import com.greenolio.app.wex.api.ApiInterface;
import com.greenolio.app.wex.api.CustomCallBack;
import com.greenolio.app.wex.api.DataInfoController;

import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;


class SplashModel {
    private static final String TAG = SplashPresenter.class.getSimpleName();
    private SplashResult mResult;

    public SplashModel() {

    }

    public void serviceGetDataInfo() {

        String baseUrl = ApiClient.BASE_URL;
        ApiInterface apiInterface = ApiClient.getClient(baseUrl).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getDataInfo();
        call.enqueue(new CustomCallBack<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        JsonObject jsonObject = new JsonParser().parse(response.body().string()).getAsJsonObject();
                        DataInfoController.getInstance().parserDataInfo(jsonObject);
                        serviceGetDataTicker();
                        Log.d(TAG, "GetDataInfo success...");
                    } catch (Exception e) {
                        mResult.getDataInfoFail("");
                        e.printStackTrace();
                    }
                }
                Log.d(TAG, "abcde");

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                super.onFailure(call, t);
                mResult.getDataInfoFail("Check conect Internet!");
                Log.d(TAG, "GetDataInfo fail...");
            }
        });
    }

    private void serviceGetDataTicker() {
        String[] originTitle = DataInfoController.getInstance().getArrOriginTitle();
        if (originTitle == null || originTitle.length == 0) {
            mResult.getDataInfoFail("");
            return;
        }
        StringBuilder groupTilte = new StringBuilder(DataInfoController.getInstance().getGroupTitle());
        if (groupTilte.length() == 0) {
            for (int i = 0; i < originTitle.length; i++) {
                if (i == 0) {
                    groupTilte = new StringBuilder(originTitle[0]);
                } else {
                    groupTilte.append("-").append(originTitle[0]);
                }
            }
        }
        String baseUrl = ApiClient.BASE_URL;
        ApiInterface apiInterface = ApiClient.getClient(baseUrl).create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getDataTicker(groupTilte.toString());
        call.enqueue(new CustomCallBack<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                super.onResponse(call, response);
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        JsonObject jsonObject = new JsonParser().parse(response.body().string()).getAsJsonObject();
                        DataInfoController.getInstance().parserDataTicker(jsonObject);

                        Log.d(TAG, "GetDataInfo success...");
                    } catch (Exception e) {
                        e.printStackTrace();
                        mResult.getDataInfoFail("");
                        return;
                    }

                }
                mResult.getDataInfoSuccess();
                Log.d(TAG, "abcde");

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                super.onFailure(call, t);
                mResult.getDataInfoFail("Check conect Internet!");
                Log.d(TAG, "GetDataInfo fail...");
            }
        });
    }



    public void setResult(SplashResult result) {
        this.mResult = result;
    }
}
