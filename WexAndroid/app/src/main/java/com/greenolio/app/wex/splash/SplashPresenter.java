package com.greenolio.app.wex.splash;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivitySplashBinding;
import com.greenolio.app.wex.main.MainActivity;
import com.greenolio.app.wex.common.DialogHelper;


public class SplashPresenter implements SplashResult {
    private static final String TAG = SplashPresenter.class.getSimpleName();
    private static final long TIMER_RUNABLE_LOADING = 50;
    private final AppCompatActivity mActivity;
    private final SplashModel mModel;
    private final ActivitySplashBinding mBindingSplash;
    private ConnectivityReceiver mReceiverInternet;
    private Handler mHandler = null;
    private int mCoutLoadIcon = 0;
    private boolean mIsDisconnectInternet;
    private boolean mIsRegisterBroadcast;

    private final int[] arrIcon = {
            R.drawable.loading_01, R.drawable.loading_02, R.drawable.loading_03, R.drawable.loading_04,
            R.drawable.loading_05, R.drawable.loading_06, R.drawable.loading_07, R.drawable.loading_08,
            R.drawable.loading_09, R.drawable.loading_10, R.drawable.loading_11, R.drawable.loading_12,
            R.drawable.loading_13, R.drawable.loading_14, R.drawable.loading_15, R.drawable.loading_16,
            R.drawable.loading_17, R.drawable.loading_18, R.drawable.loading_19, R.drawable.loading_20,
            R.drawable.loading_21, R.drawable.loading_22, R.drawable.loading_23, R.drawable.loading_24,
            R.drawable.loading_25, R.drawable.loading_26, R.drawable.loading_27, R.drawable.loading_28,
            R.drawable.loading_29, R.drawable.loading_30, R.drawable.loading_31, R.drawable.loading_32,
            R.drawable.loading_33
    };


    public SplashPresenter(AppCompatActivity mActivity, SplashModel mModel, ActivitySplashBinding mBindingSplash) {
        this.mActivity = mActivity;
        this.mModel = mModel;
        this.mModel.setResult(this);
        this.mBindingSplash = mBindingSplash;
    }

    public void loadData() {
        if (Utils.isCheckConnection(mActivity)) {
            this.mModel.serviceGetDataInfo();
            mIsDisconnectInternet = false;
        } else {
            registerBroadcast();
            dialogDisconnectInternet();
            mIsDisconnectInternet = true;
        }
        startLoading();
    }

    DialogHelper dialogHelper;

    private void dialogDisconnectInternet() {
        if (dialogHelper == null)
            dialogHelper = new DialogHelper(mActivity);
        dialogHelper.showAlert("INFO",
                mActivity.getResources().getString(R.string.txt_content_disconnect_internet), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogHelper.dismiss();
                        checkDismissDialog();
                    }
                });
    }

    private void checkDismissDialog() {
        if (!Utils.isCheckConnection(mActivity)) {
            dialogDisconnectInternet();
        }
    }

    private void startLoading() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mHandler.post(loadImageRunnable);
        mCoutLoadIcon = 0;
    }

    private void removeCallback() {
        mHandler.removeCallbacks(loadImageRunnable);
        mHandler = null;

    }

    private final Runnable loadImageRunnable = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public void run() {
            // handle count time update first gps
            if (mHandler == null) return;
            mCoutLoadIcon++;
            if (mCoutLoadIcon >= arrIcon.length) {
                mCoutLoadIcon = 0;
            }
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBindingSplash.imgSplash.setImageResource(0);
                    mBindingSplash.imgSplash.setImageResource(arrIcon[mCoutLoadIcon]);
                }
            });

            mHandler.postDelayed(this, TIMER_RUNABLE_LOADING);

        }
    };

    private void doActivityMain() {
        Intent intent = new Intent(mActivity, MainActivity.class);
        mActivity.startActivity(intent);
        mActivity.finish();
    }

    public void onDestroy() {
        unRegisterBroadcast();
        removeCallback();
    }

    @Override
    public void getDataInfoSuccess() {
        mIsDisconnectInternet = false;
        doActivityMain();
    }

    @Override
    public void getDataInfoFail(String error) {
        if (!Utils.isCheckConnection(mActivity)) {
            dialogDisconnectInternet();
            mIsDisconnectInternet = true;
            registerBroadcast();

        }
    }

    private void registerBroadcast() {
        if (!mIsRegisterBroadcast) {
            if (mReceiverInternet == null) {
                mReceiverInternet = new ConnectivityReceiver();
                mIsRegisterBroadcast = true;
            }
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            mActivity.registerReceiver(mReceiverInternet, intentFilter);
        }
    }

    private void unRegisterBroadcast() {
        if (mReceiverInternet != null) {
            mActivity.unregisterReceiver(mReceiverInternet);
            if (dialogHelper != null)
                dialogHelper.dismiss();
        }
    }

    public class ConnectivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                //NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo == null) {
                    mIsDisconnectInternet = true;
                    dialogDisconnectInternet();
                    return;
                }
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                    if (mIsDisconnectInternet) {
                        if (dialogHelper != null)
                            dialogHelper.dismiss();
                        Log.d(TAG, "nvTien - Internet connected");
                        mModel.serviceGetDataInfo();
                        mIsDisconnectInternet = false;
                    }

                } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                    mIsDisconnectInternet = true;
                    Log.d(TAG, "nvTien - Internet DisConnected");
                }
            }
        }

    }
}
