package com.greenolio.app.wex.splash;


interface SplashResult {
     void getDataInfoSuccess();
     void getDataInfoFail(String error);


}
