package com.greenolio.app.wex.tabhome;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.PreferencesUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TabHomeModel {
    private final Context mContext;
    private TabHomeResult mResult;

    public TabHomeModel(Context mContext) {
        this.mContext = mContext;
    }

    public void loadDataInfo(final int curType) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, List<InfoResponse>> task = new AsyncTask<Void, Void, List<InfoResponse>>() {
            @Override
            protected List<InfoResponse> doInBackground(Void... voids) {
                List<InfoResponse> infoResponseList = new ArrayList<>();
                Map<String, InfoResponse> infoResponseMap = DataInfoController.getInstance().getMapInfo();
                String[] arrTitle = DataInfoController.getInstance().getArrOriginTitle();
                if (arrTitle != null && arrTitle.length != 0) {
                    for (int i = arrTitle.length - 1; i >= 0; i--) {
                        String title = arrTitle[i];
                        InfoResponse infoResponse = infoResponseMap.get(title);

                        if (infoResponse != null) {
                            int type = infoResponse.getInfo().getSortType();
                            if (curType == Constant.TAG_999) {
                                infoResponseList.add(infoResponse);
                            } else if (curType == type) {
                                infoResponseList.add(infoResponse);
                            } else {
                            }
                        }
                    }
                }

                return infoResponseList;
            }

            @Override
            protected void onPostExecute(List<InfoResponse> response) {
                super.onPostExecute(response);
                mResult.loadDataSuccess(response);


            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public void loadDataInfoFavourite() {
        final boolean isFistDisplay = PreferencesUtil.getInstance(mContext).getBoolean(Constant.KEY_PREF_DISPLAY_FIRST_FAVOURITE, false);
        if (!isFistDisplay) {
            Map<String, String> defaultMap = DataInfoController.getInstance().loadDataFavourite(mContext);
            defaultMap.put("btc_usd", "btc_usd");
            defaultMap.put("eth_usd", "eth_usd");
            defaultMap.put("ltc_usd", "ltc_usd");
            defaultMap.put("bch_usd", "bch_usd");
            DataInfoController.getInstance().saveDataFavourite(mContext, defaultMap);
            PreferencesUtil.getInstance(mContext).putBoolean(Constant.KEY_PREF_DISPLAY_FIRST_FAVOURITE, true);

        }
        final Map<String, String> mapFavourite = DataInfoController.getInstance().loadDataFavourite(mContext);

        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, List<InfoResponse>> task = new AsyncTask<Void, Void, List<InfoResponse>>() {
            @Override
            protected List<InfoResponse> doInBackground(Void... voids) {
                List<InfoResponse> infoResponseList = new ArrayList<>();
                Map<String, InfoResponse> infoResponseMap = DataInfoController.getInstance().getMapInfo();
                String[] arrTitle = DataInfoController.getInstance().getArrOriginTitle();
                if (arrTitle != null && arrTitle.length != 0) {
                    for (int i = arrTitle.length - 1; i >= 0; i--) {
                        String title = arrTitle[i];
                        InfoResponse infoResponse = infoResponseMap.get(title);

                        if (infoResponse != null) {
                            String favourite = mapFavourite.get(title);
                            if (favourite != null && !favourite.isEmpty()) {
                                infoResponseList.add(infoResponse);
                            }

                        }
                    }
                }

                return infoResponseList;
            }

            @Override
            protected void onPostExecute(List<InfoResponse> response) {
                super.onPostExecute(response);
                mResult.loadDataSuccess(response);


            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void setResult(TabHomeResult result) {
        this.mResult = result;
    }
}
