package com.greenolio.app.wex.tabhome;

import android.content.Context;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.PreferencesUtil;
import com.greenolio.app.wex.common.Utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TabHomePresenter implements TabHomeResult {
    private final Context mContext;
    private final TabHomeModel mModel;
    private final TabHomeView mView;


    public TabHomePresenter(Context mContext, TabHomeModel mModel, TabHomeView mView) {
        this.mContext = mContext;
        this.mModel = mModel;
        this.mView = mView;
        this.mModel.setResult(this);

    }

    public void loadData(int curTab) {
        if (curTab == Constant.TAG_995) {
            mModel.loadDataInfoFavourite();
        } else {
            mModel.loadDataInfo(curTab);
        }
    }

    public void onDestroy() {
        final boolean isFistDisplay = PreferencesUtil.getInstance(mContext).getBoolean(Constant.KEY_PREF_DISPLAY_FIRST_FAVOURITE, false);
        if (!isFistDisplay)
            PreferencesUtil.getInstance(mContext).putBoolean(Constant.KEY_PREF_DISPLAY_FIRST_FAVOURITE, true);
    }


    @Override
    public void loadDataSuccess(List<InfoResponse> infoResponseList) {
        if (infoResponseList != null) {
            Collections.sort(infoResponseList, new Comparator<InfoResponse>() {
                public int compare(InfoResponse one, InfoResponse other) {
                    return one.getInfo().getTitleConvert().compareTo(other.getInfo().getTitleConvert());
                }
            });
            mView.loadDataSuccess(infoResponseList);

        }

    }
}
