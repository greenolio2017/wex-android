package com.greenolio.app.wex.tabhome;

import com.greenolio.app.wex.api.response.InfoResponse;

import java.util.List;

interface TabHomeResult {
     void loadDataSuccess(List<InfoResponse> infoResponseList);
}
