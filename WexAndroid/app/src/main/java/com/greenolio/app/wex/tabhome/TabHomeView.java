package com.greenolio.app.wex.tabhome;

import com.greenolio.app.wex.api.response.InfoResponse;

import java.util.List;

public interface TabHomeView {
     void loadDataSuccess(List<InfoResponse> infoResponseList);

}
