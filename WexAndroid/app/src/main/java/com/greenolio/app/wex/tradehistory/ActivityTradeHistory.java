package com.greenolio.app.wex.tradehistory;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.adapter.TradeHistoryAdapter;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityTradeHistoryBinding;

import java.util.List;

public class ActivityTradeHistory extends BaseActivity implements View.OnClickListener, ActivityTradeHistoryView, TradeHistoryAdapter.ItemClickListener {
    private ActivityTradeHistoryPresenter mPresenter;
    private ActivityTradeHistoryModel mModel;
    private ActivityTradeHistoryBinding mBinding;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayout();

    }

    private void initLayout() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_trade_history);
        initView();
        initData();

    }

    private void initView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(layoutManager);
        mBinding.imgBack.setOnClickListener(this);
    }


    private void initData() {
        if (mPresenter == null) {
            if (mModel == null) {
                mModel = new ActivityTradeHistoryModel(this);
            }
            mPresenter = new ActivityTradeHistoryPresenter(this, this, mModel);
        }
        mPresenter.loadDataOrders();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_back:
                finish();
                break;

        }

    }

    @Override
    public void loadDataSuccess(List<ActiveOrdersResponse> responseList) {
        TradeHistoryAdapter mAdapter = new TradeHistoryAdapter(responseList, this);
        mAdapter.setItemClickListener(this);
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void showTextNoData(boolean isNodata) {
        if (isNodata) {
        mBinding.txtNoData.setVisibility(View.VISIBLE);
            mBinding.recyclerView.setVisibility(View.INVISIBLE);
        } else {
            mBinding.txtNoData.setVisibility(View.GONE);
            mBinding.recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClickItem(int position, ActiveOrdersResponse responses, int orderType) {

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
