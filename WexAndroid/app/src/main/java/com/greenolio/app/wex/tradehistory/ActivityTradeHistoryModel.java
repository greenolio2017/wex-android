package com.greenolio.app.wex.tradehistory;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.greenolio.app.wex.api.ActiveOrdersAPI;
import com.greenolio.app.wex.api.GetInfo;
import com.greenolio.app.wex.api.TradeHistoryAPI;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

class ActivityTradeHistoryModel {
    private final Context mContext;
    private ActivityTradeHistoryResult mResult;

    public ActivityTradeHistoryModel(Context mContext) {
        this.mContext = mContext;
    }

    public void loadDataTradesHistory(final String pair) {
        final List<ActiveOrdersResponse> responseList = new ArrayList<>();
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, List<ActiveOrdersResponse>> task = new AsyncTask<Void, Void, List<ActiveOrdersResponse>>() {
            @Override
            protected List<ActiveOrdersResponse> doInBackground(Void... voids) {
                JsonObject jsonObject = TradeHistoryAPI.getTradeHistoryObj(mContext, pair);
                if (jsonObject != null) {
                    boolean isSuccess = TradeHistoryAPI.getSuccess(jsonObject);
                    if (isSuccess) {
                        JsonObject ordersReturn = TradeHistoryAPI.getReturn(jsonObject);
                        try {
                            JSONObject jo = new JSONObject(ordersReturn.toString());
                            final int size = jo.length();
                            for (int i = 0; i < size; i++) {
                                String key = jo.names().get(i).toString();
                                JsonElement jelement = new JsonParser().parse(jo.get(key).toString());
                                JsonObject getKeyElement = jelement.getAsJsonObject();

                                ActiveOrdersResponse response = new ActiveOrdersResponse();
                                response.setPair(TradeHistoryAPI.getPair(getKeyElement));
                                response.setType(TradeHistoryAPI.getType(getKeyElement));
                                response.setAmount(TradeHistoryAPI.getAmount(getKeyElement));
                                response.setRate(TradeHistoryAPI.getRate(getKeyElement));
                                response.setTimeStamp(TradeHistoryAPI.getTimestamp(getKeyElement));
                                response.setIsYourOrder(TradeHistoryAPI.getYourOrder(getKeyElement));
                                response.setOrderID(key);
                                responseList.add(response);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        String error = jsonObject.getAsJsonObject().get("error").toString();
                        mResult.loadOrdersFail(error);
                    }

                }
                return responseList;

            }

            @Override
            protected void onPostExecute(List<ActiveOrdersResponse> response) {
                super.onPostExecute(response);
                if (response.size() != 0) {
                    mResult.loadOrdersSuccess(response);
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }



    public void setResult(ActivityTradeHistoryResult result) {
        this.mResult = result;
    }
}
