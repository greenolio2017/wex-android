package com.greenolio.app.wex.tradehistory;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.gson.JsonObject;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.ActiveOrdersResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.component.ProgressDialog;

import java.util.List;

public class ActivityTradeHistoryPresenter implements ActivityTradeHistoryResult {
    private final AppCompatActivity mContext;
    private final ActivityTradeHistoryView mView;
    private final ActivityTradeHistoryModel mModel;
    private String mCurTitle = "";
    private ProgressDialog mDialogLoading;

    public ActivityTradeHistoryPresenter(AppCompatActivity mContext, ActivityTradeHistoryView mView, ActivityTradeHistoryModel model) {
        this.mContext = mContext;
        this.mView = mView;
        this.mModel = model;
        this.mModel.setResult(this);
        if (mContext.getIntent().getExtras() != null)
            mCurTitle = mContext.getIntent().getExtras().getString(Constant.INTENT_EXTRAS_PAIR_NAME, "");

    }


    public void loadDataOrders() {
        if (mDialogLoading == null) {
            mDialogLoading = new ProgressDialog(mContext);
        }
        mDialogLoading.show();
        mModel.loadDataTradesHistory("");
    }


    @Override
    public void loadOrdersSuccess(List<ActiveOrdersResponse> responseList) {
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        if (responseList.size() != 0) {
            mView.loadDataSuccess(responseList);
            mView.showTextNoData(false);
        } else {
            mView.showTextNoData(true);
        }
    }

    @Override
    public void loadOrdersFail(final String error) {
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        if (error == null || error.isEmpty()) {
            return;
        }
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null && !error.isEmpty()) {
                    final DialogHelper dialogHelper = new DialogHelper(mContext);
                    dialogHelper.showAlert("Notify", error, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogHelper.dismiss();
                        }
                    });
                }
                mView.showTextNoData(true);
            }
        });

    }
}
