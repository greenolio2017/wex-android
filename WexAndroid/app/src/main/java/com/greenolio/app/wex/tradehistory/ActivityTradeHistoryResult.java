package com.greenolio.app.wex.tradehistory;

import com.greenolio.app.wex.api.response.ActiveOrdersResponse;

import java.util.List;

interface ActivityTradeHistoryResult {
     void loadOrdersSuccess(List<ActiveOrdersResponse> responseList);
     void loadOrdersFail(String error);
}
