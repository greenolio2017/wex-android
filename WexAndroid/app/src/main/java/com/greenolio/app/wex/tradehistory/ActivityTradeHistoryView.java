package com.greenolio.app.wex.tradehistory;


import com.greenolio.app.wex.api.response.ActiveOrdersResponse;

import java.util.List;

interface ActivityTradeHistoryView {
    void loadDataSuccess(List<ActiveOrdersResponse> responseList);
    void showTextNoData(boolean isNodata);
}
