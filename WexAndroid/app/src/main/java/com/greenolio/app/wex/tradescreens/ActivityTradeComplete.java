package com.greenolio.app.wex.tradescreens;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityConfirmTradeBinding;

public class ActivityTradeComplete extends BaseActivity implements View.OnClickListener, ConfirmTradeView {
    private ActivityConfirmTradeBinding mBindingTrade;
    private ConfirmTradePresenter mPresenter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayout();

    }

    private void initLayout() {
        mBindingTrade = DataBindingUtil.setContentView(this, R.layout.activity_confirm_trade);
        initView();
        initData();

    }

    private void initView() {
        mBindingTrade.imgBack.setOnClickListener(this);
    }

    private void initData() {
        if (mPresenter == null) {
            mPresenter = new ConfirmTradePresenter(this, this);
        }
        mPresenter.initData();


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void setDataComplete(String amount, String price, String total) {
        mBindingTrade.txtAmount.setText(amount);
        mBindingTrade.txtPrice.setText(price);
        mBindingTrade.txtTotal.setText(total);
    }
}
