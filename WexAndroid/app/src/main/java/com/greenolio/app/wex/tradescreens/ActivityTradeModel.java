package com.greenolio.app.wex.tradescreens;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.JsonObject;
import com.greenolio.app.wex.api.GetInfo;

class ActivityTradeModel {
    private final Context mContext;
    private ActivityTradeResult mResult;

    public ActivityTradeModel(Context mContext) {
        this.mContext = mContext;
    }

    public void loadDataTrades() {
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, JsonObject> task = new AsyncTask<Void, Void, JsonObject>() {
            @Override
            protected JsonObject doInBackground(Void... voids) {
                JsonObject objectFund = null;
                JsonObject jsonObject = GetInfo.getInfoTradeObj(mContext);
                if (jsonObject != null) {
                    boolean isSuccess = GetInfo.getSuccess(jsonObject);
                    if (isSuccess) {
                        objectFund = GetInfo.getReturn(jsonObject);
                        return objectFund;
                    } else {
                        String error = jsonObject.getAsJsonObject().get("error").toString();
                        mResult.loadInfoFail(error);
                        return null;
                    }
                    //JsonObject objectFund =   GetInfo.getReturn(jsonObject).getAsJsonObject("funds").getAsJsonObject().get("usd")

                }
                return null;
            }

            @Override
            protected void onPostExecute(JsonObject response) {
                super.onPostExecute(response);
                if (response != null) {
                    mResult.loadInfoSuccess(response);
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    public void setResult(ActivityTradeResult result) {
        this.mResult = result;
    }
}
