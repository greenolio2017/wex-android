package com.greenolio.app.wex.tradescreens;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.component.ProgressDialog;

public class ActivityTradePresenter implements ActivityTradeResult {
    private final AppCompatActivity mContext;
    private final ActivityTradeView mView;
    private final ActivityTradeModel mModel;
    private String mCurTitle;
    private ProgressDialog mDialogLoading;
    private FirebaseAnalytics mFirebaseAnalytics;

    public ActivityTradePresenter(AppCompatActivity mContext, ActivityTradeView mView, ActivityTradeModel model) {
        this.mContext = mContext;
        this.mView = mView;
        this.mModel = model;
        this.mModel.setResult(this);
        if (mContext.getIntent().getExtras() != null)
            mCurTitle = mContext.getIntent().getExtras().getString(Constant.INTENT_EXTRAS_PAIR_NAME, "");

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);

        String eventName = "FIR_DISPLAY_SCREEN_BUY_SELL_INFO";
        String eventContent = "display_buy_sell_screen";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);

    }

    public void loadDataInfoTrade() {
        if (mDialogLoading == null)
            mDialogLoading = new ProgressDialog(mContext);
        mDialogLoading.show();
        mModel.loadDataTrades();
    }

    public String getCurTitle() {
        return mCurTitle;
    }


    public void setTypeInfo() {
        if (mContext.getIntent().getExtras() != null) {
            int typeInfo = mContext.getIntent().getExtras().getInt(Constant.INTENT_EXTRAS_ORDER_TYPE, Constant.ORDER_TYPE_BUY_AND_SELL);
            switch (typeInfo) {
                case Constant.ORDER_TYPE_BUY_AND_SELL:
                    mView.updateDefaultTab(0, null);
                    break;
                case Constant.ORDER_TYPE_BUY:
                    DepthResponse depthResponse = DataInfoController.getInstance().getDepthResponse();
                    mView.updateDefaultTab(0, depthResponse);
                    break;
                case Constant.ORDER_TYPE_SELL:
                    DepthResponse depthResponseSell = DataInfoController.getInstance().getDepthResponse();
                    mView.updateDefaultTab(1, depthResponseSell);
                    break;
            }
        }

    }

    @Override
    public void loadInfoSuccess(JsonObject objectFund) {
        if (objectFund != null) {
            String[] arrTitle = getCurTitle().split("_");
            if (arrTitle.length == 2) {
                String firtTitle = arrTitle[0];
                String secondTitle = arrTitle[1];
                if (objectFund.getAsJsonObject("funds") != null) {
                    String balanceBuy = objectFund.getAsJsonObject("funds").getAsJsonObject().get(secondTitle).toString();
                    String balanceSell = objectFund.getAsJsonObject("funds").getAsJsonObject().get(firtTitle).toString();
                    mView.updateBalance(balanceBuy, balanceSell);
                }
            }
        }
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }

    }

    @Override
    public void loadInfoFail(final String error) {
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null && !error.isEmpty()) {
                    if (dialogHelper == null)
                        dialogHelper = new DialogHelper(mContext);
                    dialogHelper.showAlert("Notify", error, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogHelper.dismiss();
                        }
                    });
                }
            }
        });

    }

    DialogHelper dialogHelper;

    public void dialogDisconnectInternet() {
        if (dialogHelper == null)
            dialogHelper = new DialogHelper(mContext);
        dialogHelper.showAlert("INFO",
                mContext.getResources().getString(R.string.txt_content_disconnect_internet), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogHelper.dismiss();
                        checkDismissDialog();
                    }
                });
    }

    public void checkDismissDialog() {
        if (!Utils.isCheckConnection(mContext)) {
            dialogDisconnectInternet();
        }
    }

    public void dismissDialogDisconnect() {
        if (dialogHelper != null)
            dialogHelper.dismiss();
    }
}
