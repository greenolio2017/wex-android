package com.greenolio.app.wex.tradescreens;

import com.google.gson.JsonObject;

interface ActivityTradeResult {
     void loadInfoSuccess(JsonObject objectFund);
     void loadInfoFail(String error);
}
