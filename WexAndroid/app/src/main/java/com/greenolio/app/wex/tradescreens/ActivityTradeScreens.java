package com.greenolio.app.wex.tradescreens;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.greenolio.app.wex.R;
import com.greenolio.app.wex.activity.BaseActivity;
import com.greenolio.app.wex.adapter.TradeTabsPagerAdapter;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.databinding.ActivityTradeBinding;
import com.greenolio.app.wex.fragment.TabBuyFragment;
import com.greenolio.app.wex.fragment.TabSellFragment;

import java.util.ArrayList;
import java.util.List;

public class ActivityTradeScreens extends BaseActivity implements View.OnClickListener, ActivityTradeView, ViewPager.OnPageChangeListener {
    private static final String TAG = ActivityTradeScreens.class.getSimpleName();
    private ActivityTradeBinding mBindingTrade;
    private final List<Fragment> listFrag = new ArrayList<>();
    private TabBuyFragment mFrBuy;
    private TabSellFragment mFrSell;
    private ActivityTradePresenter mPresenter;
    private ActivityTradeModel mModel;
    private int mCurPage = -1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayout();

    }

    private void initLayout() {
        mBindingTrade = DataBindingUtil.setContentView(this, R.layout.activity_trade);
        initView();
        initData();

    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        mBindingTrade.btnBuy.setOnClickListener(this);
        mBindingTrade.btnSell.setOnClickListener(this);
        mBindingTrade.imgBack.setOnClickListener(this);
        mBindingTrade.pagerBuySell.addOnPageChangeListener(this);

        mFrBuy = TabBuyFragment.getInstance(Constant.TAG_104);
        mFrSell = TabSellFragment.getInstance(Constant.TAG_105);

        listFrag.add(mFrBuy);
        listFrag.add(mFrSell);

        mBindingTrade.wrapper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return true;
            }
        });
    }

    public String getCurTitle() {
        return mPresenter.getCurTitle();
    }

    private void initData() {
        if (mPresenter == null) {
            if (mModel == null) {
                mModel = new ActivityTradeModel(this);
            }
            mPresenter = new ActivityTradePresenter(this, this, mModel);
            mPresenter.loadDataInfoTrade();
        }

        TradeTabsPagerAdapter mAdapterPage = new TradeTabsPagerAdapter(this.getSupportFragmentManager(), listFrag);
        mBindingTrade.pagerBuySell.setAdapter(mAdapterPage);
        mAdapterPage.notifyDataSetChanged();
        mPresenter.setTypeInfo();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_buy:
                setOnclickButton(0);
                mBindingTrade.pagerBuySell.setCurrentItem(0);
                break;
            case R.id.btn_sell:
                setOnclickButton(1);
                mBindingTrade.pagerBuySell.setCurrentItem(1);
                break;
            case R.id.img_back:
                finish();
                break;

        }

    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setOnclickButton(int curBtn) {
        if (curBtn == mCurPage) {
            return;
        }
        if (curBtn == 0) {
            mBindingTrade.btnBuy.setBackgroundResource(R.drawable.bg_btn_buy);
            mBindingTrade.txtBuy.setTextColor(Color.WHITE);

            mBindingTrade.btnSell.setBackgroundResource(R.color.transparent);
            mBindingTrade.txtSell.setTextColor(Color.parseColor("#06A5D1"));

            mCurPage = 0;
        } else {
            mBindingTrade.btnBuy.setBackgroundResource(R.color.transparent);
            mBindingTrade.txtBuy.setTextColor(Color.parseColor("#06A5D1"));

            mBindingTrade.btnSell.setBackgroundResource(R.drawable.bg_btn_sell);
            mBindingTrade.txtSell.setTextColor(Color.WHITE);

            mCurPage = 1;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        Log.d(TAG, "onPageScrolled: " + position);
    }

    @Override
    public void onPageSelected(int position) {
        Log.d(TAG, "onPageSelected: " + position);
        switch (position) {
            case 0:
                setOnclickButton(0);
                break;
            case 1:
                setOnclickButton(1);
                break;

        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d(TAG, "onPageScrollStateChanged: " + state);
    }


    @Override
    public void updateBalance(String balanceBuy, String balanceSell) {
        if (mFrBuy != null) {
            mFrBuy.updateBalanceBuy(balanceBuy);
        }
        if (mFrSell != null) {
            mFrSell.updateBalanceSell(balanceSell);
        }
    }

    @Override
    public void updateDefaultTab(int type, DepthResponse depthResponse) {

        if (depthResponse != null) {
            if (type == 0) {
                setOnclickButton(0);
                mBindingTrade.pagerBuySell.setCurrentItem(0);
                mFrBuy.loadDataDepth(depthResponse);
            } else if (type == 1) {
                setOnclickButton(1);
                mBindingTrade.pagerBuySell.setCurrentItem(1);
                mFrSell.loadDataDepth(depthResponse);
            }
        } else {
            setOnclickButton(0);
            mBindingTrade.pagerBuySell.setCurrentItem(0);
        }
    }


    @Override
    public void showDialogDisconnect() {
        super.showDialogDisconnect();
        mPresenter.dialogDisconnectInternet();
    }

    @Override
    public void dismissDialogDisconnect() {
        super.dismissDialogDisconnect();
        mPresenter.dismissDialogDisconnect();
    }
}
