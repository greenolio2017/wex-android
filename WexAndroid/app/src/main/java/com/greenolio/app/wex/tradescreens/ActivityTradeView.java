package com.greenolio.app.wex.tradescreens;

import com.greenolio.app.wex.api.response.DepthResponse;

interface ActivityTradeView {
         void updateBalance(String balanceBuy, String balanceSell);
         void updateDefaultTab(int type, DepthResponse response);
}
