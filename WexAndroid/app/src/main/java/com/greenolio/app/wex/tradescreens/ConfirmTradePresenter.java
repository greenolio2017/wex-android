package com.greenolio.app.wex.tradescreens;

import android.support.v7.app.AppCompatActivity;

import com.greenolio.app.wex.common.Constant;

class ConfirmTradePresenter {
    private final AppCompatActivity mContext;
    private final ConfirmTradeView mView;

    public ConfirmTradePresenter(AppCompatActivity mContext, ConfirmTradeView mView) {
        this.mContext = mContext;
        this.mView = mView;
    }

    public void initData() {
        if (mContext.getIntent().getExtras() == null) {
            return;
        }
        String amount = mContext.getIntent().getExtras().getString(Constant.KEY_EXTRAS_AMOUNT, "0");
        String price = mContext.getIntent().getExtras().getString(Constant.KEY_EXTRAS_PRICE, "0");
        String total = mContext.getIntent().getExtras().getString(Constant.KEY_EXTRAS_TOTAL, "0");
        mView.setDataComplete(amount, price, total);
    }

}
