package com.greenolio.app.wex.tradescreens;

interface ConfirmTradeView {
     void setDataComplete(String amount, String price, String total);
}
