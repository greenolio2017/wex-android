package com.greenolio.app.wex.tradescreens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.component.ProgressDialog;
import com.greenolio.app.wex.databinding.FragmentBuyBinding;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class TabBuyPresenter implements TabBuyResult {
    private final AppCompatActivity mContext;
    private final TabBuyModel mModel;
    private final TabBuyView mView;
    private ProgressDialog mDialogLoading;
    private List<DepthResponse> mListDepthResponse;
    private FirebaseAnalytics mFirebaseAnalytics;

    public TabBuyPresenter(AppCompatActivity mContext, TabBuyModel mModel, TabBuyView mView) {
        this.mContext = mContext;
        this.mModel = mModel;
        this.mView = mView;
        this.mModel.setResult(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
    }

    public void loadDataBuy(String title) {
        if (title != null && !title.isEmpty()) {
            InfoResponse infoResponse = DataInfoController.getInstance().getMapInfo().get(title);
            mListDepthResponse = DataInfoController.getInstance().getListDepthResponse();
            DepthResponse depthResponse = null;
            if (mListDepthResponse != null && mListDepthResponse.size() != 0) {
                depthResponse = mListDepthResponse.get(0);
            }
            String[] arrTitle = title.split("_");

            if (infoResponse != null) {
                mView.loadInfoResponse(infoResponse, depthResponse, arrTitle);
            }
        }
    }

    public void onChangeAmount(String fee, FragmentBuyBinding mTabChartBinding) {
        try {
            if (mTabChartBinding.edtAmout.getText().toString().isEmpty() || mTabChartBinding.edtPricePer.getText().toString().isEmpty()) {
                mView.setDataChange("0", "0");
                return;
            }
            Double amout = Double.parseDouble(mTabChartBinding.edtAmout.getText().toString());
            Double price = Double.parseDouble(mTabChartBinding.edtPricePer.getText().toString());
            Double feeValue = Double.parseDouble(fee);


            NumberFormat formatter = new DecimalFormat("#.########");
            double total = calculateTotalValue(amout, price);
            double feeAmount = amout * feeValue * 0.01;
            mView.setDataChange(formatter.format(total), formatter.format(feeAmount));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private double calculateTotalValue(double amountBuy, double priceBuy) {
        double totalBuy = 0;
        if (mListDepthResponse != null && mListDepthResponse.size() != 0) {
            for (int i = 0; i < mListDepthResponse.size(); i++) {
                DepthResponse response = mListDepthResponse.get(i);
                if (response.getAsk() != null) {
                    double curAmout = Double.parseDouble(response.getAsk().getAskAmount());
                    double curPrice = Double.parseDouble(response.getAsk().getAskPrice());

                    if (priceBuy > curPrice) {
                        if (amountBuy >= curAmout) {
                            totalBuy = totalBuy + curAmout * curPrice;
                            amountBuy = amountBuy - curAmout;
                        } else {
                            totalBuy = totalBuy + amountBuy * curPrice;
                            amountBuy = 0;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (amountBuy > 0) {
                totalBuy = totalBuy + amountBuy * priceBuy;
            }

        } else {
            totalBuy = amountBuy * priceBuy;
        }

        return totalBuy;
    }

    public void loadDataAmoutSelected(int posItem) {
        String result ;
        double totalAmout = 0;
        if (mListDepthResponse != null && mListDepthResponse.size() != 0) {
            for (int i = 0; i < mListDepthResponse.size(); i++) {
                if (i > posItem) {
                    break;
                } else {
                    DepthResponse depthResponse = mListDepthResponse.get(i);
                    if (depthResponse != null && depthResponse.getAsk() != null) {
                        double curAmout = Double.parseDouble(depthResponse.getAsk().getAskAmount());
                        totalAmout = totalAmout + curAmout;
                    }
                }
            }
            NumberFormat formatter = new DecimalFormat("#.########");
            result = formatter.format(totalAmout).replace(",",".");
            mView.loadTotalAmoutSelected(result);
        }

    }


    public void doBuyData(String tradePrice, String tradeAmount, String title) {
        if (mDialogLoading == null)
            mDialogLoading = new ProgressDialog(mContext);
        mDialogLoading.show();
        mModel.doBuyData(title, tradePrice, tradeAmount);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_BUY_BUTTON";
        String eventContent = "touched_buy_button";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    public void calculateClickBalance(String balance, String price) {
        if (price == null || price.isEmpty() || price.equals("0")) {
            return;
        }
        double curBalance = Double.parseDouble(balance);
        double curPrice = Double.parseDouble(price);
        double amount =  (curBalance/curPrice) - 0.0000001;
        if (amount < 0) {
            amount = 0;
        }
        NumberFormat formatter = new DecimalFormat("#.########");
        String result = formatter.format(amount).replace(",",".");
        mView.setDataAmount(result);

    }

    @Override
    public void buyDataSuccess(JsonObject objectData) {
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        if (objectData != null) {
            mView.requestRedirectComplete();
        }

    }

    public void redirectComplete(String amount, String price, String total) {
        Intent intent = new Intent(mContext, ActivityTradeComplete.class);
        intent.putExtra(Constant.KEY_EXTRAS_AMOUNT, amount);
        intent.putExtra(Constant.KEY_EXTRAS_PRICE, price);
        intent.putExtra(Constant.KEY_EXTRAS_TOTAL, total);
        mContext.startActivity(intent);
        mContext.finish();

    }

    @Override
    public void buyDataFail(final String error) {
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        if (error == null || error.isEmpty()) {
            return;
        }
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null && !error.isEmpty()) {
                    final DialogHelper dialogHelper = new DialogHelper(mContext);
                    dialogHelper.showAlert("Notify", error, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogHelper.dismiss();
                        }
                    });
                }
            }
        });
    }
}
