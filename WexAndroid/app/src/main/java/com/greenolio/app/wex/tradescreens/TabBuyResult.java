package com.greenolio.app.wex.tradescreens;

import com.google.gson.JsonObject;

interface TabBuyResult {
     void buyDataSuccess(JsonObject objectData);
     void buyDataFail(String error);
}
