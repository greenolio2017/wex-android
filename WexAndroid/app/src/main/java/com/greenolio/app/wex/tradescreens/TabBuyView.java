package com.greenolio.app.wex.tradescreens;

import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.api.response.InfoResponse;

public interface TabBuyView {
     void loadInfoResponse(InfoResponse infoResponse, DepthResponse depthResponse, String[] arrTitle);
     void setDataChange(String total, String fee);
     void requestRedirectComplete();
     void loadTotalAmoutSelected(String totalAmout);
     void setDataAmount(String amount);
}
