package com.greenolio.app.wex.tradescreens;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.JsonObject;
import com.greenolio.app.wex.R;
import com.greenolio.app.wex.api.DoTrade;

public class TabSellModel {
    private final Context mContext;
    private TabSellResult mResult;
    private static final String SELL_TYPE = "sell";

    public TabSellModel(Context mContext) {
        this.mContext = mContext;
    }

    public void doSellData(final String pair, final String tradePrice, final String tradeAmount) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, JsonObject> task = new AsyncTask<Void, Void, JsonObject>() {
            @Override
            protected JsonObject doInBackground(Void... voids) {
                JsonObject objectReturn;
                JsonObject jsonObject = DoTrade.doTrade(mContext, pair, SELL_TYPE, tradePrice, tradeAmount);
                if (jsonObject == null) {
                    return null;
                }
                final boolean success = DoTrade.getSuccess(jsonObject);
                if (success) {
                    objectReturn = DoTrade.getReturn(jsonObject);
                } else {
                    String error = jsonObject.getAsJsonObject().get("error").toString();
                    mResult.sellDataFail(error);
                    return null;
                }
                return objectReturn;
            }

            @Override
            protected void onPostExecute(JsonObject response) {
                super.onPostExecute(response);
                if (response != null) {
                    mResult.sellDataSuccess(response);
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void setResult(TabSellResult result) {
        this.mResult = result;
    }
}
