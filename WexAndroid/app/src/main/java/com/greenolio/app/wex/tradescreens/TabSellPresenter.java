package com.greenolio.app.wex.tradescreens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.greenolio.app.wex.api.DataInfoController;
import com.greenolio.app.wex.api.response.DepthResponse;
import com.greenolio.app.wex.api.response.InfoResponse;
import com.greenolio.app.wex.common.Constant;
import com.greenolio.app.wex.common.DialogHelper;
import com.greenolio.app.wex.common.Utils;
import com.greenolio.app.wex.component.ProgressDialog;
import com.greenolio.app.wex.databinding.FragmentSellBinding;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class TabSellPresenter implements TabSellResult {
    private final AppCompatActivity mContext;
    private final TabSellModel mModel;
    private final TabSellView mView;
    private ProgressDialog mDialogLoading;
    private List<DepthResponse> mListDepthResponse;
    private FirebaseAnalytics mFirebaseAnalytics;

    public TabSellPresenter(AppCompatActivity mContext, TabSellModel mModel, TabSellView mView) {
        this.mContext = mContext;
        this.mModel = mModel;
        this.mView = mView;
        this.mModel.setResult(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
    }

    public void loadDataBuy(String title) {
        InfoResponse infoResponse = DataInfoController.getInstance().getMapInfo().get(title);
        mListDepthResponse = DataInfoController.getInstance().getListDepthResponse();
        DepthResponse depthResponse = null;
        if (mListDepthResponse != null && mListDepthResponse.size() != 0) {
            depthResponse = mListDepthResponse.get(0);
        }
        String[] arrTitle = title.split("_");
        if (infoResponse != null) {
            mView.loadInfoResponse(infoResponse, depthResponse, arrTitle);
        }

    }

    public void onChangeAmount(String fee, FragmentSellBinding mTabSellBinding) {
        try {
            if (mTabSellBinding.edtAmout.getText().toString().isEmpty() || mTabSellBinding.edtPricePer.getText().toString().isEmpty()) {
                mView.setDataChange("0", "0");
                return;
            }

            Double amout = Double.parseDouble(mTabSellBinding.edtAmout.getText().toString());
            Double price = Double.parseDouble(mTabSellBinding.edtPricePer.getText().toString());
            Double feeValue = Double.parseDouble(fee);

            NumberFormat formatter = new DecimalFormat("#.########");
            double total = calculateTotalValue(amout, price);
            Double feeAmount = total * feeValue * 0.01;
            mView.setDataChange(formatter.format(total), formatter.format(feeAmount));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private double calculateTotalValue(double amountSell, double priceSell) {
        // String result = "";
        double totalSell = 0;
        if (mListDepthResponse != null && mListDepthResponse.size() != 0) {
            for (int i = 0; i < mListDepthResponse.size(); i++) {
                DepthResponse response = mListDepthResponse.get(i);
                if (response.getBids() != null) {
                    double curAmout = Double.parseDouble(response.getBids().getBidAmount());
                    double curPrice = Double.parseDouble(response.getBids().getBidPrice());

                    if (priceSell < curPrice) {
                        if (amountSell >= curAmout) {
                            totalSell = totalSell + curAmout * curPrice;
                            amountSell = amountSell - curAmout;
                        } else {
                            totalSell = totalSell + amountSell * curPrice;
                            amountSell = 0;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (amountSell > 0) {
                totalSell = totalSell + amountSell * priceSell;
            }

        } else {
            totalSell = amountSell * priceSell;
        }

        //result =  formatter.format(totalSell);
        return totalSell;
    }

    public void loadDataAmoutSelected(int posItem) {
        String result;
        double totalAmout = 0;
        if (mListDepthResponse != null && mListDepthResponse.size() != 0) {
            for (int i = 0; i < mListDepthResponse.size(); i++) {
                if (i > posItem) {
                    break;
                } else {
                    DepthResponse depthResponse = mListDepthResponse.get(i);
                    if (depthResponse != null && depthResponse.getBids() != null) {
                        double curAmout = Double.parseDouble(depthResponse.getBids().getBidAmount());
                        totalAmout = totalAmout + curAmout;
                    }
                }
            }
            NumberFormat formatter = new DecimalFormat("#.########");
            result = formatter.format(totalAmout).replace(",", ".");
            mView.loadTotalAmoutSelected(result);
        }

    }

    public void doSellData(String tradePrice, String tradeAmount, String title) {
        if (mDialogLoading == null)
            mDialogLoading = new ProgressDialog(mContext);
        mDialogLoading.show();
        mModel.doSellData(title, tradePrice, tradeAmount);

        String eventName = "FIR_ACTION_TOUCH_EVENT_ON_SELL_BUTTON";
        String eventContent = "touched_sell_button";
        Utils.logEventFireBase(mFirebaseAnalytics, eventName, eventContent);
    }

    public void redirectComplete(String amount, String price, String total) {
        Intent intent = new Intent(mContext, ActivityTradeComplete.class);
        intent.putExtra(Constant.KEY_EXTRAS_AMOUNT, amount);
        intent.putExtra(Constant.KEY_EXTRAS_PRICE, price);
        intent.putExtra(Constant.KEY_EXTRAS_TOTAL, total);
        mContext.startActivity(intent);
        mContext.finish();

    }

    public void calculateClickBalance(String balance) {
        if (balance == null) {
            return;
        }
        mView.setDataAmount(balance);

    }

    @Override
    public void sellDataSuccess(JsonObject objectData) {
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        if (objectData != null) {
            mView.requestRedirectComplete();
        }
    }

    @Override
    public void sellDataFail(final String error) {
        if (mDialogLoading != null) {
            mDialogLoading.dissmissDialog();
        }
        if (error == null || error.isEmpty()) {
            return;
        }
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null && !error.isEmpty()) {
                    final DialogHelper dialogHelper = new DialogHelper(mContext);
                    dialogHelper.showAlert("Notify", error, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogHelper.dismiss();
                        }
                    });
                }
            }
        });
    }
}
