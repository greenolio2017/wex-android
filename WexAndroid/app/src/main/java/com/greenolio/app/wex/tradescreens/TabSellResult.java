package com.greenolio.app.wex.tradescreens;

import com.google.gson.JsonObject;

interface TabSellResult {
     void sellDataSuccess(JsonObject objectData);
     void sellDataFail(String error);
}
